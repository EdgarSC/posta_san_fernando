package com.app.factory.peru.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.app.factory.peru.tipoUsuario.entity.TipoUsuarioEntity;

public interface ITipoUsuarioNewDao extends JpaRepository<TipoUsuarioEntity, Long> {
	
	List<TipoUsuarioEntity> findByestado(int estado);
}
