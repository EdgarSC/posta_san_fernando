package com.app.factory.peru.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.app.factory.peru.receta.entity.RecetaEntity;


public interface IRecetaDao extends JpaRepository<RecetaEntity, Long> {
	
	@Query("FROM RecetaEntity WHERE consulta.id_consulta = :id_consulta")
	List<RecetaEntity> listarMedicamentos(long id_consulta);
	
}
