package com.app.factory.peru.tipoUsuario.repository;

import java.util.List;

import com.app.factory.peru.generic.repository.IGenericRepo;
import com.app.factory.peru.tipoUsuario.entity.TipoUsuarioEntity;

public interface ITipoUsuariosRepository extends IGenericRepo<TipoUsuarioEntity, Integer>{
	
	List<TipoUsuarioEntity> findByestado(int estado);

}
