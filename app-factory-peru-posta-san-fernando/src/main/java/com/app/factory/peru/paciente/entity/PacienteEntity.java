package com.app.factory.peru.paciente.entity;

import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.app.factory.peru.region.entity.RegionEntity;


@Entity
@Table(name = "PACIENTE")
public class PacienteEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="PACIENTEID")
	private Long id_paciente;

	@OneToOne
	@JoinColumn(name = "REGIONID")
	private RegionEntity region;
	
	@Column(name="NOMBRE")
	private String nombre;
	
	@Column(name="APELLIDOPATERNO")
	private String apellido_paterno;
	
	@Column(name="APELLIDOMATERNO")
	private String apellido_materno;
	
	@Column(name="NUMERODOCUMENTO")
	private String numero_documento;
	
	@Transient
	private int edad;
	
	@Column(name="SEXO")
	private int sexo;
	
	@Column(name="ESTADOCIVIL")
	private int estado_civil;
	
	@Column(name="LUGARNACIMIENTO")
	private String lugar_nacimiento;
	
	@Column(name="FECHANACIMIENTO")
	private LocalDate fecha_nacimiento;
	
	@Column(name="PESO")
	private double peso;
	
	@Column(name="TALLA")
	private double talla;
	
	@Column(name="RAZA")
	private String raza;
	
	@Column(name="RELIGION")
	private String religion;
	
	@Column(name="NACIONALIDAD")
	private String nacionalidad;
	
	@Column(name="GRADOINSTRUCCION")
	private int grado_instruccion;
	
	@Column(name="OCUPACION")
	private String ocupacion;
	
	@Column(name="DIRECCIONACTUAL")
	private String direccion_actual;
	
	@Column(name="TELEFONO")
	private String telefono;
	
	@Column(name="CELULAR")
	private String celular;
	
	@Column(name="CORREO")
	private String correo;
	
	@Column(name="NOMBRECONTACTO")
	private String nombre_contacto;
	
	@Column(name="TELEFONOCONTACTO")
	private String telefono_contacto;
	
	@Column(name="CORREOCONTACTO")
	private String correo_contacto;
	
	@Column(name="ESTADO")
	private int estado;
	
	@Column(name="CREATOR")
	private String creator;
	
	@Column(name="CREATED")
	private LocalDateTime created;
	
	@Column(name="CHANGER")
	private String changer;
	
	@Column(name="CHANGED")
	private LocalDateTime changed;
	
	@Transient
	private RegionEntity provincia;
	
	@Transient
	private String nombre_distrito;
	
	@Transient
	private String nombre_provincia;
	
	@Transient
	private String nombre_departamento;

	public PacienteEntity() {}
	
	public PacienteEntity(Long id_paciente) {
		this.id_paciente = id_paciente;
	}
	
	public Long getId_paciente() {
		return id_paciente;
	}

	public void setId_paciente(Long id_paciente) {
		this.id_paciente = id_paciente;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido_paterno() {
		return apellido_paterno;
	}

	public void setApellido_paterno(String apellido_paterno) {
		this.apellido_paterno = apellido_paterno;
	}

	public String getApellido_materno() {
		return apellido_materno;
	}

	public void setApellido_materno(String apellido_materno) {
		this.apellido_materno = apellido_materno;
	}
	
	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public String getNumero_documento() {
		return numero_documento;
	}

	public void setNumero_documento(String numero_documento) {
		this.numero_documento = numero_documento;
	}

	public int getSexo() {
		return sexo;
	}

	public void setSexo(int sexo) {
		this.sexo = sexo;
	}

	public int getEstado_civil() {
		return estado_civil;
	}

	public void setEstado_civil(int estado_civil) {
		this.estado_civil = estado_civil;
	}

	public String getLugar_nacimiento() {
		return lugar_nacimiento;
	}

	public void setLugar_nacimiento(String lugar_nacimiento) {
		this.lugar_nacimiento = lugar_nacimiento;
	}

	public LocalDate getFecha_nacimiento() {
		return fecha_nacimiento;
	}

	public void setFecha_nacimiento(LocalDate fecha_nacimiento) {
		this.fecha_nacimiento = fecha_nacimiento;
	}

	public double getPeso() {
		return peso;
	}

	public void setPeso(double peso) {
		this.peso = peso;
	}

	public double getTalla() {
		return talla;
	}

	public void setTalla(double talla) {
		this.talla = talla;
	}

	public String getRaza() {
		return raza;
	}

	public void setRaza(String raza) {
		this.raza = raza;
	}

	public String getReligion() {
		return religion;
	}

	public void setReligion(String religion) {
		this.religion = religion;
	}

	public String getNacionalidad() {
		return nacionalidad;
	}

	public void setNacionalidad(String nacionalidad) {
		this.nacionalidad = nacionalidad;
	}

	public int getGrado_instruccion() {
		return grado_instruccion;
	}

	public void setGrado_instruccion(int grado_instruccion) {
		this.grado_instruccion = grado_instruccion;
	}

	public String getOcupacion() {
		return ocupacion;
	}

	public void setOcupacion(String ocupacion) {
		this.ocupacion = ocupacion;
	}

	public String getDireccion_actual() {
		return direccion_actual;
	}

	public void setDireccion_actual(String direccion_actual) {
		this.direccion_actual = direccion_actual;
	}

	public RegionEntity getRegion() {
		return region;
	}

	public void setRegion(RegionEntity region) {
		this.region = region;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getNombre_contacto() {
		return nombre_contacto;
	}

	public void setNombre_contacto(String nombre_contacto) {
		this.nombre_contacto = nombre_contacto;
	}

	public String getTelefono_contacto() {
		return telefono_contacto;
	}

	public void setTelefono_contacto(String telefono_contacto) {
		this.telefono_contacto = telefono_contacto;
	}

	public String getCorreo_contacto() {
		return correo_contacto;
	}

	public void setCorreo_contacto(String correo_contacto) {
		this.correo_contacto = correo_contacto;
	}

	public int getEstado() {
		return estado;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public LocalDateTime getCreated() {
		return created;
	}

	public void setCreated(LocalDateTime created) {
		this.created = created;
	}

	public String getChanger() {
		return changer;
	}

	public void setChanger(String changer) {
		this.changer = changer;
	}

	public LocalDateTime getChanged() {
		return changed;
	}

	public void setChanged(LocalDateTime changed) {
		this.changed = changed;
	}
	
	public RegionEntity getProvincia() {
		return provincia;
	}
	
	public void setProvincia(RegionEntity provincia) {
		this.provincia = provincia;
	}

	public String getNombre_distrito() {
		return nombre_distrito;
	}

	public void setNombre_distrito(String nombre_distrito) {
		this.nombre_distrito = nombre_distrito;
	}

	public String getNombre_provincia() {
		return nombre_provincia;
	}

	public void setNombre_provincia(String nombre_provincia) {
		this.nombre_provincia = nombre_provincia;
	}

	public String getNombre_departamento() {
		return nombre_departamento;
	}

	public void setNombre_departamento(String nombre_departamento) {
		this.nombre_departamento = nombre_departamento;
	}
	
	
}
