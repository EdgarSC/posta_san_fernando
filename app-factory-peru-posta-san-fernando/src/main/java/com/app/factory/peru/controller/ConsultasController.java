package com.app.factory.peru.controller;

import java.security.Principal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.app.factory.peru.consultas.entity.ConsultaEntity;
import com.app.factory.peru.dao.IConsultaDao;
import com.app.factory.peru.dao.IDiagnosticoDao;
import com.app.factory.peru.dao.IPacienteDao;
import com.app.factory.peru.dao.IRecetaDao;
import com.app.factory.peru.dao.IRegionDao;
import com.app.factory.peru.diagnostico.entity.DiagnosticoEntity;
import com.app.factory.peru.paciente.entity.PacienteEntity;
import com.app.factory.peru.receta.entity.RecetaEntity;
import com.app.factory.peru.region.entity.RegionEntity;

@RestController
public class ConsultasController {
	
	private static final Logger LOGGER = LogManager.getLogger(ConsultasController.class);
	
	@Autowired
	private IConsultaDao daoConsulta;
	
	@Autowired
	private IPacienteDao daoPaciente;
	
	@Autowired
	private IRecetaDao daoReceta;
	
	@Autowired
	private IDiagnosticoDao daoDiagnostico;
	
	@Autowired
	private IRegionDao daoRegion;
	
	@GetMapping("consultas/listar_consultas_paciente")
	@ResponseBody
	public List<ConsultaEntity> listarConsultas(String dni_paciente) {
		List<ConsultaEntity> consultas = new ArrayList<>();
		try {
			if(dni_paciente.equals("##vacio##"))
				consultas = daoConsulta.listarTodas();
			else
				consultas = daoConsulta.listarPorNumeroDNI(dni_paciente);
		}catch (Exception e) {
			LOGGER.error("######################################################Error al listar consultas - paciente asociado. Detalle: " + e.getMessage());
		}
		return consultas;
	}
	
	@GetMapping("consultas/obtener_paciente_dni")
	@ResponseBody
	public PacienteEntity obtenerPacienteDNI(String dni_paciente) {
		PacienteEntity paciente = null;
		try {
			paciente = daoPaciente.obtenerPorDNI(dni_paciente);
			if(paciente != null) {
				int edad = (paciente.getFecha_nacimiento() == null ? -1 : (int) ChronoUnit.YEARS.between(paciente.getFecha_nacimiento(), LocalDate.now()));
				paciente.setEdad(edad);
				RegionEntity distrito = paciente.getRegion();
				RegionEntity provincia = daoRegion.obtenerRegion(distrito.getProvincia() == null ? "-1" : distrito.getProvincia());
				RegionEntity departamento = daoRegion.obtenerRegion(provincia == null ? "-" : provincia.getDepartamento());
				paciente.setNombre_distrito(distrito == null ? "-" : distrito.getNombre());
				paciente.setNombre_provincia(provincia == null ? "-" : provincia.getNombre());
				paciente.setNombre_departamento(departamento == null ? "-" : departamento.getNombre());
			}
		}catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("######################################################Error al obtener paciente - paciente asociado. Detalle: " + e.getMessage());
		}
		return paciente;
	}
	
	private ConsultaEntity guardarConsulta(PacienteEntity paciente, String ids_servicios, long id_pac, double talla_pac, double peso_pac, double imc_pac, String pres_art_pac, String frec_card_pac, String temp_pac, String frec_resp_pac, 
			String satur_pac, String id_servicio, String motivo_consulta_pac, String plan_trabajo_pac, String menarquia_pac, String gestaciones_pac, String partos_pac, String reg_catam_pac, String met_anticon_pac, String fecha_ult_reg_pac, 
			String orient_sex_pac, String nro_parejas_sex_pac, String id_agente_gestionador) {
		try {
			DateTimeFormatter fmt = DateTimeFormatter.ofPattern("yyyy-MM-dd");
			ConsultaEntity consulta = new ConsultaEntity();
			
			/*Segundo panel: PERSONALES GINECOLÓGICOS*/
			boolean mujer = paciente.getSexo() == 1 ? true : false;
			consulta.setMenarquia(mujer ? menarquia_pac : "-");
			consulta.setGestaciones(mujer ? gestaciones_pac : "-");
			consulta.setPartos(mujer ? partos_pac : "-");
			consulta.setRegimen_catamenial(mujer ? reg_catam_pac : "-");
			consulta.setMetconceptivo(mujer ? met_anticon_pac : "-");
			if(!fecha_ult_reg_pac.equals(""))
				consulta.setFur(mujer ? LocalDate.parse(fecha_ult_reg_pac, fmt) : null);
			consulta.setOrientacion_sexual(mujer ? orient_sex_pac : "-");
			consulta.setNro_parejas_sexuales(mujer ? nro_parejas_sex_pac : "-");
			
			/*Tercer panel: CONSULTA*/
			consulta.setPaciente(paciente);
			consulta.setServicio(ids_servicios.trim().length() == 0 ? "Sin Interconsulta" : ids_servicios);
			consulta.setInter_consulta(ids_servicios.trim().length() == 0 ? "Sin Interconsulta" : ids_servicios);
			consulta.setTalla(talla_pac);
			consulta.setPeso(peso_pac);
			consulta.setImc(imc_pac);
			consulta.setPresion_arterial(pres_art_pac);
			consulta.setFrecuencia_cardiaca(frec_card_pac);
			consulta.setTemperatura(temp_pac);
			consulta.setFrecuencia_respiratoria(frec_resp_pac);
			consulta.setSaturacion(satur_pac);
			consulta.setMotivo_consulta(motivo_consulta_pac);
			consulta.setPlan_trabajo(plan_trabajo_pac);
			consulta.setCreator(id_agente_gestionador);
			consulta.setFecha(LocalDate.now());
			daoConsulta.save(consulta);
			return consulta;
		}catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("No se pudo concretar registro consulta. Detalle: " + e.getMessage());
			return null;
		}
	}
	
	private boolean guardarRecetas(String js_recetas, ConsultaEntity consulta, String fecha_expiracion_receta) {
		try {
			DateTimeFormatter fmt = DateTimeFormatter.ofPattern("yyyy-MM-dd");
			
			List<RecetaEntity> recetas = new ArrayList<>();
			JSONArray json_recetas = new JSONArray(js_recetas);
			for (int i = 0; i < json_recetas.length(); i++) {
				JSONObject json_receta = json_recetas.getJSONObject(i);
				
				String str_medicamento = json_receta.getString("medicamento");
				String str_concentracion = json_receta.getString("concentraci");
				String str_ff = json_receta.getString("ff");
				String str_cantidad = json_receta.getString("cantidad");
				String str_dosis = json_receta.getString("dosis");
				String str_via = json_receta.getString("via");
				String str_frecuencia = json_receta.getString("frecuencia");
				String str_duracion = json_receta.getString("duracion");
				
				RecetaEntity receta = new RecetaEntity();
				receta.setMedicamento(str_medicamento);
				receta.setConcentracion(str_concentracion);
				receta.setFf(str_ff);
				receta.setCantidad(str_cantidad);
				receta.setDosis(str_dosis);
				receta.setVia(str_via);
				receta.setFrecuencia(str_frecuencia);
				receta.setDuracion(str_duracion);
				receta.setConsulta(consulta);
				receta.setFecha_expiracion(LocalDate.parse(fecha_expiracion_receta, fmt));
				recetas.add(receta);
			}
			daoReceta.saveAll(recetas);
			return true;
		}catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("No se pudo concretar registro receta. Detalle: " + e.getMessage());
			return false;
		}
	}
	
	private boolean guardarDiagnostico(String js_diagnosticos, ConsultaEntity consulta) {
		try {
			List<DiagnosticoEntity> diagnosticos = new ArrayList<>();
			JSONArray json_diagnosticos = new JSONArray(js_diagnosticos);
			for (int i = 0; i < json_diagnosticos.length(); i++) {
				JSONObject json_diagnostico = json_diagnosticos.getJSONObject(i);
				String str_diagnostico = json_diagnostico.getString("diag_consulta");
				String str_cie = json_diagnostico.getString("cie_consulta");
				
				DiagnosticoEntity diagnostico = new DiagnosticoEntity();
				diagnostico.setDiagnostico(str_diagnostico);
				diagnostico.setCie10(str_cie);
				diagnostico.setConsulta(consulta);
				diagnostico.setFecha_registro(LocalDateTime.now());
				diagnosticos.add(diagnostico);
			}
			daoDiagnostico.saveAll(diagnosticos);
			return true;
		}catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("No se pudo concretar registro diagnostico. Detalle: " + e.getMessage());
			return false;
		}
	}
	
	@PostMapping("consultas/grabar_consulta")
	@ResponseBody
	public String grabarConsultaPaciente(Principal p, String js_diagnosticos, String js_recetas, long id_pac, double talla_pac, double peso_pac, double imc_pac, String pres_art_pac, String frec_card_pac, String temp_pac, String frec_resp_pac, 
			String satur_pac, String ids_servicios, String motivo_consulta_pac, String plan_trabajo_pac, String menarquia_pac, String gestaciones_pac, String partos_pac, String reg_catam_pac, String met_anticon_pac, String fecha_ult_reg_pac, 
			String orient_sex_pac, String nro_parejas_sex_pac, String fecha_expiracion_receta) {
		try {
			PacienteEntity paciente = daoPaciente.getOne(id_pac);
			if(paciente == null) return "(a): Error al obtener paciente.";
			
			ConsultaEntity consulta = guardarConsulta(paciente, ids_servicios, id_pac, talla_pac, peso_pac, imc_pac, pres_art_pac, frec_card_pac, temp_pac, frec_resp_pac, satur_pac, ids_servicios, motivo_consulta_pac, plan_trabajo_pac, menarquia_pac, gestaciones_pac, 
					partos_pac, reg_catam_pac, met_anticon_pac, fecha_ult_reg_pac, orient_sex_pac, nro_parejas_sex_pac, p.getName());
			
			if(consulta == null) return "(a): No se pudo concretar el registro de la consulta.";
			
			if(!guardarRecetas(js_recetas, consulta, fecha_expiracion_receta)) 
				return "(a): Se guardó la consulta, sin embargo no pudo concretarse el registro de las recetas de la consulta.";
			
			if(!guardarDiagnostico(js_diagnosticos, consulta)) 
				return "(a): Se guardó la consulta, sin embargo no pudo concretarse el registro de los diagnósticos de la consulta.";
		}catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("######################################################Error al guardar consulta paciente. Detalle: " + e.getMessage());
			return "(a): Ocurrió un error al concretar registro.";
		}
		return "Consulta guardada correctamente.";
	}
	
	
	
	
	
	
	
	
	
	@GetMapping("consultas/obtener_ultima_consulta")
	@ResponseBody
	public ConsultaEntity obtenerUltimaConsulta(String dni_paciente) {
		ConsultaEntity consulta = null;
		try {
			List<ConsultaEntity> consultas = daoConsulta.listarUltimaConsulta(dni_paciente);
			
			if(consultas.size() > 0)
				consulta = consultas.get(0);
		}catch (Exception e) {
			e.printStackTrace();
		}
		return consulta;
	}
	
	
	@GetMapping("consultas/listar_diagnosticos")
	@ResponseBody
	public List<DiagnosticoEntity> listarDiagnosticos(int id_consulta) {
		List<DiagnosticoEntity> diagnosticos = new ArrayList<>();
		try {
			
			diagnosticos = daoDiagnostico.listarDiagnosticos(id_consulta);
		}catch (Exception e) {
			e.printStackTrace();
		}
		return diagnosticos;
	}
	
	@GetMapping("consultas/listar_medicamentos")
	@ResponseBody
	public List<RecetaEntity> listarMedicamentos(int id_consulta) {
		List<RecetaEntity> medicamentos = new ArrayList<>();
		try {
			medicamentos = daoReceta.listarMedicamentos(id_consulta);
			for (RecetaEntity med : medicamentos) {
				PacienteEntity paciente = med.getConsulta().getPaciente();
				
				int edad = (paciente.getFecha_nacimiento() == null ? -1 : (int) ChronoUnit.YEARS.between(paciente.getFecha_nacimiento(), LocalDate.now()));
				paciente.setEdad(edad);
				med.getConsulta().setPaciente(paciente);
				
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return medicamentos;
	}
}
