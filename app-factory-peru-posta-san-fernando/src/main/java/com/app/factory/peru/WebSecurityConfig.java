package com.app.factory.peru;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter{
	 
	String[] resources = new String[]{
	            "/include/**","/admin_static/bower_components/**","/admin_static/dist/css/**","/admin_static/template/**","/admin_static/plugins/**","/js/**","/admin_static/**","/Login/**"
	    };
	
	 @Override
	    protected void configure(HttpSecurity http) throws Exception {
//hasAnyRole(rol1,rol2)
	    	http
	        .authorizeRequests()
	        .antMatchers(resources).permitAll()  
	        .antMatchers("/","/index","/login").permitAll()
	            .anyRequest().authenticated()
	            .and()
	        .formLogin()
	            .loginPage("/login")
	            .permitAll()
	            .defaultSuccessUrl("/admin",true)
	            .failureUrl("/login?error=true")
	            .usernameParameter("username")
	            .passwordParameter("password")
	            .and()
	            .csrf().disable()
	        .logout()
	            .permitAll()
	            .logoutSuccessUrl("/login")
	            .deleteCookies("JSESSIONID")
	           .and().rememberMe();
	    }
	    
	    BCryptPasswordEncoder bCryptPasswordEncoder;

	    @Bean
	    public BCryptPasswordEncoder passwordEncoder() {
			bCryptPasswordEncoder = new BCryptPasswordEncoder(4);
	        return bCryptPasswordEncoder;
	    }
	    
	    @Autowired
	    UserDetailsService userDetailsService;
	    
	    @Autowired
	    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception { 
	    	//Especificar el encargado del login y encriptacion del password
	        auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
	    }
}
