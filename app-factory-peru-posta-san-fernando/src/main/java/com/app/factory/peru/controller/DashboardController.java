package com.app.factory.peru.controller;

import java.security.Principal;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.app.factory.peru.dao.ICitaDao;
import com.app.factory.peru.dao.IConsultaDao;
import com.app.factory.peru.dao.IPacienteDao;
import com.app.factory.peru.dao.IUsuariosRolesDao;

@RestController
public class DashboardController {
		
	@Autowired
	private IUsuariosRolesDao userRolDao;
	
	@Autowired
	private IPacienteDao pacienteDao;
	
	@Autowired
	private IConsultaDao consultaDao;
	
	@Autowired
	private ICitaDao citaDao;
	
	@GetMapping("/cantidad_total_usuarios")
	@ResponseBody
	public Map<String, Object> cantidadTotalUsuarios(Principal p){
		if(p == null) return null;
		Map<String, Object> resultado = new HashMap<>();
		Object[][] total_users = userRolDao.cantUsuariosXRol();
		long total_pacientes = pacienteDao.count();
		long total_consultas = consultaDao.count();
		long total_citas = citaDao.contarCitasAtendidas();
		resultado.put("usuarios_totales", total_users);
		resultado.put("pacientes_totales", total_pacientes);
		resultado.put("consultas_totales", total_consultas);
		resultado.put("citas_totales", total_citas);
		return resultado;
	}
}
