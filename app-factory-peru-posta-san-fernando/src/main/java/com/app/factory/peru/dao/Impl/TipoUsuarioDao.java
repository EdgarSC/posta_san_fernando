package com.app.factory.peru.dao.Impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.app.factory.peru.converter.TipoUsuarioConverter;
import com.app.factory.peru.dao.ITipoUsuarioDao;
import com.app.factory.peru.dto.TipoUsuarioDTO;
import com.app.factory.peru.service.ITipoUsuarioService;
import com.app.factory.peru.tipoUsuario.entity.TipoUsuarioEntity;

@Component("tipoUsuarioDao")
public class TipoUsuarioDao implements ITipoUsuarioDao{
	
	@Autowired
	@Qualifier("tipoUsuarioConverter")
	private TipoUsuarioConverter tipoUsuarioConverter;
	
//	@Autowired
//	private UsuarioService lUsuarioService;

	@Autowired
	private ITipoUsuarioService tipoUsuarioService;
	
	@Override
	public List<TipoUsuarioDTO> getAllTipoUsuario() throws Exception {
		List<TipoUsuarioDTO> lTipoUsuarioDTOCollection = new ArrayList<>();
		this.tipoUsuarioService.getAllTipUsuario().stream().forEach(c -> {
			lTipoUsuarioDTOCollection.add(this.tipoUsuarioConverter.entityToDTO(c));
		});
		return lTipoUsuarioDTOCollection;
	}

	@Override
	public TipoUsuarioDTO saveTipoUsuario(TipoUsuarioDTO prTipoUsuarioDTO) throws Exception {
		TipoUsuarioDTO lTipoUsuarioDTO = null;
		TipoUsuarioEntity tipoUsuarioEntity = tipoUsuarioConverter.dtoToEntity(prTipoUsuarioDTO);
//		UsuarioEntity lUsuarioEntity = lUsuarioService.getLoggedUser();
		if(tipoUsuarioEntity.getCreator() == null || tipoUsuarioEntity.getCreator().isEmpty()) {
//			tipoUsuarioEntity.setCreator(lUsuarioEntity.getUsuario());
//			tipoUsuarioEntity.setCreated(new Date());
		}

		lTipoUsuarioDTO = tipoUsuarioConverter.entityToDTO(tipoUsuarioService.add(tipoUsuarioEntity));

		return lTipoUsuarioDTO;
	}

	@Override
	public List<TipoUsuarioDTO> getByFilter(TipoUsuarioDTO prTipoUsuarioDTO) throws Exception {
		List<TipoUsuarioDTO> lTipoUsuarioDTOCollection = new ArrayList<>();
		
		TipoUsuarioEntity tipoUsuarioEntity = tipoUsuarioConverter.dtoToEntity(prTipoUsuarioDTO);
		this.tipoUsuarioService.getByFilter(tipoUsuarioEntity).stream().forEach(c -> {
			lTipoUsuarioDTOCollection.add(this.tipoUsuarioConverter.entityToDTO(c));
		});
		return lTipoUsuarioDTOCollection;
	}

	@Override
	public TipoUsuarioDTO activateOrDesactivateTipoUsuario(String prTipoUsuarioId) throws Exception {
		java.util.Optional<TipoUsuarioEntity> optionalTipoUsuarioEntity = tipoUsuarioService.getByTipoUsuarioId(prTipoUsuarioId);
		TipoUsuarioDTO lTipoUsuarioDTO = null;
		if (optionalTipoUsuarioEntity.isPresent()) {
			TipoUsuarioEntity tipoUsuarioEntity = optionalTipoUsuarioEntity.get();
			tipoUsuarioEntity.setEstado((tipoUsuarioEntity.getEstado() == 0) ? 1 : 0);
//			tipoUsuarioEntity.setChanged(new Date());
//			UsuarioEntity lUsuarioEntity = lUsuarioService.getLoggedUser();
//			tipoUsuarioEntity.setChanger(lUsuarioEntity.getUsuario());
			lTipoUsuarioDTO = tipoUsuarioConverter.entityToDTO(tipoUsuarioService.add(tipoUsuarioEntity));
		}

		return lTipoUsuarioDTO;
	}

	@Override
	public List<TipoUsuarioDTO> findbyNombreTipoUsuario(String keyword) throws Exception {
		List<TipoUsuarioDTO> lTipoUsuarioDTOCollection = new ArrayList<>();
		this.tipoUsuarioService.findbyNombreTipoUsuario(keyword).stream().forEach(c -> {
			lTipoUsuarioDTOCollection.add(this.tipoUsuarioConverter.entityToDTO(c));
		});
		return lTipoUsuarioDTOCollection;
	}

}
