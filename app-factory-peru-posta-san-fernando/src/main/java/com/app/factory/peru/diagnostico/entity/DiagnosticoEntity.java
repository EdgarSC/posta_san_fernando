package com.app.factory.peru.diagnostico.entity;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.app.factory.peru.consultas.entity.ConsultaEntity;

@Entity
@Table(name = "DIAGNOSTICO")
public class DiagnosticoEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="DIAGNOSTICOID")
	private Long id_diagnostico;
	
	@OneToOne
	@JoinColumn(name = "CONSULTAID")
	private ConsultaEntity consulta;
	
	@Column(name="DIAGNOSTICO")
	private String diagnostico;
	
	@Column(name="CIE10")
	private String cie10;
	
	@Column(name="FECHA")
	private LocalDateTime fecha_registro;

	public Long getId_diagnostico() {
		return id_diagnostico;
	}

	public void setId_diagnostico(Long id_diagnostico) {
		this.id_diagnostico = id_diagnostico;
	}

	public String getDiagnostico() {
		return diagnostico;
	}

	public void setDiagnostico(String diagnostico) {
		this.diagnostico = diagnostico;
	}

	public String getCie10() {
		return cie10;
	}

	public void setCie10(String cie10) {
		this.cie10 = cie10;
	}

	public LocalDateTime getFecha_registro() {
		return fecha_registro;
	}

	public void setFecha_registro(LocalDateTime fecha_registro) {
		this.fecha_registro = fecha_registro;
	}

	public ConsultaEntity getConsulta() {
		return consulta;
	}

	public void setConsulta(ConsultaEntity consulta) {
		this.consulta = consulta;
	}
	
}