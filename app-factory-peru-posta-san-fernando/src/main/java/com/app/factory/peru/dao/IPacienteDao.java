package com.app.factory.peru.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.app.factory.peru.paciente.entity.PacienteEntity;

public interface IPacienteDao extends JpaRepository<PacienteEntity, Long>{
	
	@Query("FROM PacienteEntity WHERE numero_documento = :numero_documento")
	PacienteEntity obtenerPorDNI(String numero_documento); 
}
