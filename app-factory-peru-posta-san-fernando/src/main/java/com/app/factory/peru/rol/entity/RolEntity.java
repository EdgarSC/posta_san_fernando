package com.app.factory.peru.rol.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "ROL")
public class RolEntity implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ROLID")
	private Long rolId;
	@Column(name = "NAME")
	private String name;
	@Column(name = "DESCRIPCION")
	private String descripcion;
	@Column(name = "ESTADO")
	private int estado;
	@Column(name = "CREATOR")
	private String creator;
	@JsonFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	@Column(name = "CREATED")
	private LocalDateTime created;
	@Column(name = "CHANGER")
	private String changer;
	@JsonFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	@Column(name = "CHANGED")
	private LocalDateTime changed;
	
//	@ManyToMany(fetch = FetchType.LAZY)
//	@JoinTable(name = "ROLPRIVILEGIO",
//			joinColumns=@JoinColumn(name="ROLID"),
//			inverseJoinColumns=@JoinColumn(name="PRIVILEGIOID"))
//	private Set<PrivilegioEntity> privilegios;

	public RolEntity() {
		super();
	}

	public RolEntity(Long rolId, String name, String descripcion, int estado, String creator, LocalDateTime created,
			String changer, LocalDateTime changed) {
		super();
		this.rolId = rolId;
		this.name = name;
		this.descripcion = descripcion;
		this.estado = estado;
		this.creator = creator;
		this.created = created;
		this.changer = changer;
		this.changed = changed;
	}

	public Long getRolId() {
		return rolId;
	}

	public void setRolId(Long rolId) {
		this.rolId = rolId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public int getEstado() {
		return estado;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public LocalDateTime getCreated() {
		return created;
	}

	public void setCreated(LocalDateTime created) {
		this.created = created;
	}

	public LocalDateTime getChanged() {
		return changed;
	}

	public void setChanged(LocalDateTime changed) {
		this.changed = changed;
	}

	public String getChanger() {
		return changer;
	}

	public void setChanger(String changer) {
		this.changer = changer;
	}
	
//	public Set<PrivilegioEntity> getPrivilegios() {
//		return privilegios;
//	}
//
//	public void setPrivilegios(Set<PrivilegioEntity> privilegios) {
//		this.privilegios = privilegios;
//	}

	@Override
	public String toString() {
		return "RolEntity [rolId=" + rolId + ", name=" + name + ", descripcion=" + descripcion + ", estado=" + estado
				+ ", creator=" + creator + ", created=" + created + ", changer=" + changer + ", changed=" + changed
				+ "]";
	}

	private static final long serialVersionUID = 4149692960235315643L;

}
