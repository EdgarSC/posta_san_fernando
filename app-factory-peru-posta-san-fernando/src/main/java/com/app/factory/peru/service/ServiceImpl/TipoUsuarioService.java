package com.app.factory.peru.service.ServiceImpl;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.app.factory.peru.repository.ITipoUsuarioRepository;
import com.app.factory.peru.service.ITipoUsuarioService;
import com.app.factory.peru.tipoUsuario.entity.TipoUsuarioEntity;

@Service
public class TipoUsuarioService implements ITipoUsuarioService {

	@Autowired
	private ITipoUsuarioRepository lITipoUsuarioRepository;

	@Override
	public List<TipoUsuarioEntity> getAllTipUsuario() throws Exception {
		return (List<TipoUsuarioEntity>) lITipoUsuarioRepository.findAll();
	}

	@Override
	public TipoUsuarioEntity add(TipoUsuarioEntity prTipoUsuarioEntity) throws Exception {
		return lITipoUsuarioRepository.save(prTipoUsuarioEntity);
	}

	@Override
	public List<TipoUsuarioEntity> getByFilter(TipoUsuarioEntity prTipoUsuarioEntity) throws Exception {

		return lITipoUsuarioRepository.findbyFilter(
				(prTipoUsuarioEntity.getNombre() == null || prTipoUsuarioEntity.getNombre().isEmpty()) ? null : prTipoUsuarioEntity.getNombre(),
				(prTipoUsuarioEntity.getDescripcion() == null || prTipoUsuarioEntity.getDescripcion().isEmpty()) ? null : prTipoUsuarioEntity.getDescripcion(),
				(prTipoUsuarioEntity.getEstado() > -1) ? String.valueOf(prTipoUsuarioEntity.getEstado()) : null);
	}

	@Override
	public Optional<TipoUsuarioEntity> getByTipoUsuarioId(String prTipoUsuarioId) throws Exception {
		return lITipoUsuarioRepository.findById(Long.parseLong(prTipoUsuarioId));
	}

	@Override
	public List<TipoUsuarioEntity> findbyNombreTipoUsuario(String keyword) throws Exception {
		return lITipoUsuarioRepository.findbyNombreTipoUsuario(keyword);
	}

}
