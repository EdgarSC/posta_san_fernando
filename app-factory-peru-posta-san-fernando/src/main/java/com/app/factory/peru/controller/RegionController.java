package com.app.factory.peru.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.app.factory.peru.dao.IRegionDao;
import com.app.factory.peru.region.entity.RegionEntity;

@RestController
public class RegionController {
	private static final Logger LOGGER = LogManager.getLogger(ServiciosController.class);
	
	@Autowired
	private IRegionDao regionDao;
	
	@GetMapping("regiones/listar_departamentos")
	@ResponseBody
	public ResponseEntity<?> listarDepartamentos(){
		try {
			List<RegionEntity> regiones = new ArrayList<>();
			regiones = regionDao.listarDepartamentos();
			if(regiones.size() > 0) {
				return new ResponseEntity<List<RegionEntity>>(regiones,HttpStatus.OK);
			}else {
				return new ResponseEntity<String>("No se encontraron resultados.",HttpStatus.BAD_REQUEST);
			}
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("Error getRolList: detalle: " + e.getMessage());
			return new ResponseEntity<String>("Se tuvo un error al obtener la lista de departamentos, favor de contactar al administrador.",HttpStatus.BAD_REQUEST);
		}
	}
	
	@GetMapping("regiones/listar_provincias")
	@ResponseBody
	public ResponseEntity<?> listarProvincia(String departamento){
		try {
			List<RegionEntity> regiones = new ArrayList<>();
			regiones = regionDao.listarProvincias(departamento);
			if(regiones.size() > 0) {
				return new ResponseEntity<List<RegionEntity>>(regiones,HttpStatus.OK);
			}else {
				return new ResponseEntity<String>("No se encontraron resultados.",HttpStatus.BAD_REQUEST);
			}
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("Error getRolList: detalle: " + e.getMessage());
			return new ResponseEntity<String>("Se tuvo un error al obtener la lista de provincias, favor de contactar al administrador.",HttpStatus.BAD_REQUEST);
		}
	}
	
	@GetMapping("regiones/listar_distritos")
	@ResponseBody
	public ResponseEntity<?> listarDistritos(String provincia){
		try {
			List<RegionEntity> regiones = new ArrayList<>();
			regiones = regionDao.listarDistritos(provincia);
			if(regiones.size() > 0) {
				return new ResponseEntity<List<RegionEntity>>(regiones,HttpStatus.OK);
			}else {
				return new ResponseEntity<String>("No se encontraron resultados.",HttpStatus.BAD_REQUEST);
			}
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("Error getRolList: detalle: " + e.getMessage());
			return new ResponseEntity<String>("Se tuvo un error al obtener la lista de distritos, favor de contactar al administrador.",HttpStatus.BAD_REQUEST);
		}
	}
	
}
