package com.app.factory.peru.usuarioRol.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.app.factory.peru.rol.entity.RolEntity;
import com.app.factory.peru.usuario.entity.UsuarioEntity;

@Entity
@Table(name ="USUARIOSROLES")
public class UsuarioRolesEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "USUARIOROLESID")
	private Long userRolId;
	@OneToOne
	@JoinColumn(name ="USUARIOID")
	private UsuarioEntity usuario;
	@OneToOne
	@JoinColumn(name ="ROLID")
	private RolEntity rol;
	@Column(name ="ESTADO")
	private int estado;
	
	public static final int ADMINISTRADOR = 1;
	public static final int SUPERVISOR = 2;
	public static final int CONSULTOR = 3;
	
	public Long getUserRolId() {
		return userRolId;
	}
	public void setUserRolId(Long userRolId) {
		this.userRolId = userRolId;
	}	
	public UsuarioEntity getUsuario() {
		return usuario;
	}
	public void setUsuario(UsuarioEntity usuario) {
		this.usuario = usuario;
	}
	public RolEntity getRol() {
		return rol;
	}
	public void setRol(RolEntity rol) {
		this.rol = rol;
	}
	public int getEstado() {
		return estado;
	}
	public void setEstado(int estado) {
		this.estado = estado;
	}
	@Override
	public String toString() {
		return "UsuarioRolesEntity [userRolId=" + userRolId + ", usuario=" + usuario + ", rol=" + rol + ", estado="
				+ estado + "]";
	}	
	
}
