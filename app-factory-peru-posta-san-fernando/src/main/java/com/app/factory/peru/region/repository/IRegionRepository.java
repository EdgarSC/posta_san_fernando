package com.app.factory.peru.region.repository;
import com.app.factory.peru.generic.repository.IGenericRepo;
import com.app.factory.peru.region.entity.RegionEntity;
import java.util.Collection;
import java.util.List;
import org.springframework.data.jpa.repository.Query;

public interface IRegionRepository extends IGenericRepo<RegionEntity, Integer> {
	
	@Query("FROM RegionEntity WHERE departamento is null and provincia is null")
	List<RegionEntity> listarDepartamentos();
	
	@Query("FROM RegionEntity WHERE departamento = :departamento and provincia is null")
	List<RegionEntity> listarProvincias(String departamento);
	
	@Query("FROM RegionEntity WHERE departamento is null and provincia = :provincia")
	List<RegionEntity> listarDistritos(String provincia);
	
	@Query("FROM RegionEntity WHERE regionumero in (select provincia FROM RegionEntity where regionid in (:id_regiones))")
	List<RegionEntity> listarProvinciasXDistritos(Collection<Long> id_regiones);
	
	@Query("FROM RegionEntity WHERE REGIONUMERO = :region_numero")
	RegionEntity obtenerRegion(String region_numero);
	
	
//	Querys Nativos (Ejemplos)
//	@Query(value = "SELECT * FROM REGION WHERE depOrtamento is null and provincia = ?1 and asdasdas = ?2", nativeQuery = true)
//	List<RegionEntity> asdasdasdasd(String param_1, String param_2);	
}
