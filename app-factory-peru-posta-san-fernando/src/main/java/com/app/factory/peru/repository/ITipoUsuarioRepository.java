package com.app.factory.peru.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.app.factory.peru.tipoUsuario.entity.TipoUsuarioEntity;

@Repository
public interface ITipoUsuarioRepository extends CrudRepository<TipoUsuarioEntity, Long>{
	@Query("FROM TipoUsuarioEntity WHERE nombre = IFNULL(?1,nombre) AND descripcion = IFNULL(?2,descripcion) AND estado = IFNULL(?3,estado)")
	List<TipoUsuarioEntity> findbyFilter(String nombre,String descripcion, String estado);
	
	@Query("FROM TipoUsuarioEntity WHERE nombre like %:keyword% AND estado = 1")
	List<TipoUsuarioEntity> findbyNombreTipoUsuario(@Param("keyword") String keyword);
	
}
