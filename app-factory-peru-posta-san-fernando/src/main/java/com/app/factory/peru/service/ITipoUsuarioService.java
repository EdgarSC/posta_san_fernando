package com.app.factory.peru.service;

import java.util.List;
import java.util.Optional;

import com.app.factory.peru.tipoUsuario.entity.TipoUsuarioEntity;

public interface ITipoUsuarioService {
	
	public List<TipoUsuarioEntity> getAllTipUsuario() throws Exception;
	public TipoUsuarioEntity add(TipoUsuarioEntity prTipoUsuarioEntity) throws Exception;
	public List<TipoUsuarioEntity> getByFilter(TipoUsuarioEntity prTipoUsuarioEntity) throws Exception;
	public Optional<TipoUsuarioEntity> getByTipoUsuarioId(String prTipoUsuarioId) throws Exception;
	public List<TipoUsuarioEntity> findbyNombreTipoUsuario(String keyword) throws Exception;
	
}
