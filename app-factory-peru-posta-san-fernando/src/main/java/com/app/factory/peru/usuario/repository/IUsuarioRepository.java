package com.app.factory.peru.usuario.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;

import com.app.factory.peru.generic.repository.IGenericRepo;
import com.app.factory.peru.usuario.entity.UsuarioEntity;

public interface IUsuarioRepository extends IGenericRepo<UsuarioEntity, Integer>{

	UsuarioEntity findBynumeroDocumento(String dni);
	Optional<UsuarioEntity> findByUsuarioAndEstado(String username, int estado); 
	List<UsuarioEntity> findByestado(int estado);
	@Query("FROM UsuarioEntity WHERE usuario = ?1 and estado = ?2")
	UsuarioEntity obtenerXusuarioYEstado(String username, int estado);
	
}
