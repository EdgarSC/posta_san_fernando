package com.app.factory.peru.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.app.factory.peru.servicios.entity.ServicioEntity;


public interface IServicioDao extends JpaRepository<ServicioEntity, Long>{
	
	List<ServicioEntity> findByestado(int estado);
}
