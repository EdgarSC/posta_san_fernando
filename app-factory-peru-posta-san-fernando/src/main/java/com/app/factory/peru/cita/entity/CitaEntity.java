package com.app.factory.peru.cita.entity;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.app.factory.peru.paciente.entity.PacienteEntity;
import com.app.factory.peru.servicios.entity.ServicioEntity;

@Entity
@Table(name = "CITAS")
public class CitaEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="CITAID")
	private Long id_cita;
	
	@Column(name = "FECHA_CITA")
	private LocalDate fecha_cita;
	
	@Column(name = "HORA_CITA")
	private LocalTime hora_cita;
	
	@Column(name = "INTERCONSULTA")
	private int interconsulta;
	
	@Column(name = "CELULAR_PACIENTE")
	private String celular_paciente;
	
	@Column(name = "ATENDIDO")
	private Integer atentido;
	
	@Column(name = "ESTADO")
	private Integer estado;
	
	@OneToOne
	@JoinColumn(name = "PACIENTEID")
	private PacienteEntity paciente;
	
	@OneToOne
	@JoinColumn(name = "SERVICIOID")
	private ServicioEntity servicio;
	
	@Column(name="CHANGER")
	private String changer;
	
	@Column(name="CREATOR")
	private String creator;
	
	@Column(name="CREATED")
	private LocalDateTime created;
	
	@Column(name="CHANGED")
	private LocalDateTime changed;

	public Long getId_cita() {
		return id_cita;
	}

	public void setId_cita(Long id_cita) {
		this.id_cita = id_cita;
	}

	public LocalDate getFecha_cita() {
		return fecha_cita;
	}

	public void setFecha_cita(LocalDate fecha_cita) {
		this.fecha_cita = fecha_cita;
	}

	public LocalTime getHora_cita() {
		return hora_cita;
	}
	
	public void setHora_cita(LocalTime hora_cita) {
		this.hora_cita = hora_cita;
	}

	public int getInterconsulta() {
		return interconsulta;
	}

	public void setInterconsulta(int interconsulta) {
		this.interconsulta = interconsulta;
	}

	public String getCelular_paciente() {
		return celular_paciente;
	}

	public void setCelular_paciente(String celular_paciente) {
		this.celular_paciente = celular_paciente;
	}

	public Integer getAtentido() {
		return atentido;
	}

	public void setAtentido(Integer atentido) {
		this.atentido = atentido;
	}

	public Integer getEstado() {
		return estado;
	}

	public void setEstado(Integer estado) {
		this.estado = estado;
	}

	public PacienteEntity getPaciente() {
		return paciente;
	}

	public void setPaciente(PacienteEntity paciente) {
		this.paciente = paciente;
	}

	public String getChanger() {
		return changer;
	}

	public void setChanger(String changer) {
		this.changer = changer;
	}

	public LocalDateTime getChanged() {
		return changed;
	}
	
	public void setChanged(LocalDateTime changed) {
		this.changed = changed;
	}
	
	public LocalDateTime getCreated() {
		return created;
	}
	
	public void setCreated(LocalDateTime created) {
		this.created = created;
	}
	
	public void setCreator(String creator) {
		this.creator = creator;
	}
	
	public String getCreator() {
		return creator;
	}

	public ServicioEntity getServicio() {
		return servicio;
	}

	public void setServicio(ServicioEntity servicio) {
		this.servicio = servicio;
	}
	
}
