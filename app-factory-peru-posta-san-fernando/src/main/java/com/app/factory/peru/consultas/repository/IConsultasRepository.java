package com.app.factory.peru.consultas.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;

import com.app.factory.peru.consultas.entity.ConsultaEntity;
import com.app.factory.peru.generic.repository.IGenericRepo;

public interface IConsultasRepository extends IGenericRepo<ConsultaEntity, Integer>{

	@Query("FROM ConsultaEntity WHERE paciente.numero_documento = :numero_documento ORDER BY created DESC")
	List<ConsultaEntity> listarPorNumeroDNI(String numero_documento);
	
	@Query("FROM ConsultaEntity WHERE paciente.numero_documento = :numero_documento ORDER BY created DESC")
	List<ConsultaEntity> listarUltimaConsulta(String numero_documento);
	
	@Query("FROM ConsultaEntity ORDER BY created DESC")
	List<ConsultaEntity> listarTodas();
	
}
