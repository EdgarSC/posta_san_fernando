package com.app.factory.peru.controller;

import java.security.Principal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.app.factory.peru.dao.IPacienteDao;
import com.app.factory.peru.dao.IRegionDao;
import com.app.factory.peru.paciente.entity.PacienteEntity;
import com.app.factory.peru.region.entity.RegionEntity;

@RestController
public class PacienteController {
	
	private static final Logger LOGGER = LogManager.getLogger(ServiciosController.class);
	
	@Autowired
	private IPacienteDao pacienteDao;
	
	@Autowired
	private IRegionDao regionDao;
	
	@GetMapping("pacientes/listar_pacientes")
	@ResponseBody
	public ResponseEntity<?> listarPacientes(Principal p){		
		try {			
			if(p==null) return null;
			List<PacienteEntity> pacientes = pacienteDao.findAll();		
			List<RegionEntity> regiones = regionDao.listarProvinciasXDistritos(pacientes.stream().map(paciente -> paciente.getRegion().getRegionId()).collect(Collectors.toList()));
			Map<Integer, RegionEntity> id_regiones = regiones.stream().collect(Collectors.toMap(RegionEntity::getRegionNumero, region -> region));
			pacientes.forEach(paciente -> paciente.setProvincia(id_regiones.get(Integer.parseInt(paciente.getRegion().getProvincia()))));
			
			if(pacientes.size() > 0) {
				return new ResponseEntity<List<PacienteEntity>>(pacientes,HttpStatus.OK);
			}else {
				return new ResponseEntity<String>("No se encontraron resultados.",HttpStatus.BAD_REQUEST);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("Error getListarPacientes: detalle: " + e.getMessage());
			return new ResponseEntity<String>("Se tuvo un error al obtener la lista de pacientes, favor de contactar al administrador.",HttpStatus.BAD_REQUEST);
		}
	}
	
	@PostMapping("pacientes/guardar_paciente")
	@ResponseBody
	public String guardarPacientes(Principal p, String nombre, String apellidoPaterno, String apellidoMaterno, String numeroDocumento, int sexo,
								String fechaNacimiento, String telefono, String celular, String email, String lugarNacimiento, String peso, 
								String talla, String raza, String religion, String nacionalidad, String direccionDomiciliaria, int gradoInstruccion, 
								String ocupacion, int estadoCivil, int region, String nombreContacto, String telefonoContacto, String emailContacto){
		try {	
			if(p==null) return null;
			PacienteEntity dni = pacienteDao.obtenerPorDNI(numeroDocumento);
			LocalDate dataNacimiento = LocalDate.parse(fechaNacimiento, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
			if(nombre.equals("")) {return ":Por favor, llenar el nombre del paciente.";
			}else if(apellidoPaterno.equals("")) {return ":Por favor, llenar el apellido paterno.";									
			}else if(numeroDocumento.equals("")) {return ":Por favor, llenar número de documento del paciente.";
			}else if(dni != null) {	return ":El número de documento ya existe, ingrese el correcto.";
			}else if(sexo == -1) {return ":Por favor, ingrese sexo del paciente.";
			}else if(!email.equals("")) { 
				if(!Validaciones.compararExpresionesRegulares("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\\.[a-zA-Z0-9-]+)*$", email)) {
					return ":El email no corresponde con la expresión introducida.";			
				}		
			}
			
			PacienteEntity paciente = new PacienteEntity();
			paciente.setNombre(nombre);
			paciente.setApellido_paterno(apellidoPaterno);
			paciente.setApellido_materno(apellidoMaterno);
			paciente.setNumero_documento(numeroDocumento);
			paciente.setSexo(sexo);
			paciente.setFecha_nacimiento(dataNacimiento);
			paciente.setTelefono(telefono);
			paciente.setCelular(celular);
			paciente.setCorreo(email);
			paciente.setLugar_nacimiento(lugarNacimiento);
			paciente.setPeso(Double.parseDouble(peso.equals("") ? "0.0" : peso));
			paciente.setTalla(Double.parseDouble(talla.equals("") ? "0.0" : talla));
			paciente.setRaza(raza);
			paciente.setReligion(religion);
			paciente.setNacionalidad(nacionalidad);
			paciente.setDireccion_actual(direccionDomiciliaria);
			paciente.setGrado_instruccion(gradoInstruccion);
			paciente.setOcupacion(ocupacion);
			paciente.setEstado_civil(estadoCivil);
			paciente.setRegion(new RegionEntity(region));
			paciente.setNombre_contacto(nombreContacto);
			paciente.setTelefono_contacto(telefonoContacto);
			paciente.setCorreo_contacto(emailContacto);
			paciente.setCreator(p.getName());
			paciente.setCreated(LocalDateTime.now());
			paciente.setChanged(LocalDateTime.now());
			paciente.setEstado(1);
			paciente = pacienteDao.save(paciente);
			return "Listo";
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("Error guardarPaciente: detalle: " + e.getMessage());
			return null;
		}
	}
	
	@PostMapping("pacientes/editar_paciente")
	@ResponseBody
	public String editarPaciente(Principal p, long id_paciente,String nombre, String apellidoPaterno, String apellidoMaterno, 
			String numeroDocumento, int sexo, String fechaNacimiento, String telefono, String celular,
			String email, String lugarNacimiento, String peso, String talla, String raza, String religion, String nacionalidad,
			String direccionDomiciliaria, int gradoInstruccion, String ocupacion, int estadoCivil, int region, String nombreContacto,
			String telefonoContacto, String emailContacto){
		try {			
			if(p==null) return null;
			PacienteEntity pacienteXDni = pacienteDao.obtenerPorDNI(numeroDocumento);
			LocalDate dataNacimiento = LocalDate.parse(fechaNacimiento, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
			if(nombre.equals("")) {return ":Por favor, llenar el nombre del paciente.";			
			}else if(apellidoPaterno.equals("")) {return ":Por favor, llenar el apellido paterno.";
			}else if(sexo == -1) {return ":Por favor, ingrese sexo del paciente.";
			}else if(numeroDocumento.equals("")) {return ":Por favor, llenar número de documento del paciente.";
			}else if(pacienteXDni != null && pacienteXDni.getId_paciente() != id_paciente) {	return ":El número de documento ya existe, ingrese el correcto.";
			}else if(!email.equals("")) { 
				if(!Validaciones.compararExpresionesRegulares("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\\.[a-zA-Z0-9-]+)*$", email)) {
					return ":El email no corresponde con la expresión introducida.";			
				}		
			}	
			Optional<PacienteEntity> paciente = pacienteDao.findById(id_paciente);
			PacienteEntity paciente_update = paciente.get();
			paciente_update.setNombre(nombre);
			paciente_update.setApellido_paterno(apellidoPaterno);
			paciente_update.setApellido_materno(apellidoMaterno);
			paciente_update.setNumero_documento(numeroDocumento);
			paciente_update.setSexo(sexo);
			paciente_update.setFecha_nacimiento(dataNacimiento);
			paciente_update.setTelefono(telefono);
			paciente_update.setCelular(celular);
			paciente_update.setCorreo(email);
			paciente_update.setLugar_nacimiento(lugarNacimiento);
			paciente_update.setPeso(Double.parseDouble(peso.equals("") ? "0.0" : peso));
			paciente_update.setTalla(Double.parseDouble(talla.equals("") ? "0.0" : talla));
			paciente_update.setRaza(raza);
			paciente_update.setReligion(religion);
			paciente_update.setNacionalidad(nacionalidad);
			paciente_update.setDireccion_actual(direccionDomiciliaria);
			paciente_update.setGrado_instruccion(gradoInstruccion);
			paciente_update.setOcupacion(ocupacion);
			paciente_update.setEstado_civil(estadoCivil);
			paciente_update.setRegion(new RegionEntity(region));
			paciente_update.setNombre_contacto(nombreContacto);
			paciente_update.setTelefono_contacto(telefonoContacto);
			paciente_update.setCorreo_contacto(emailContacto);
			paciente_update.setChanger(p.getName());
			paciente_update.setChanged(LocalDateTime.now());
			pacienteDao.save(paciente_update);
			return "Listo";
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("Error guardarServicio: detalle: " + e.getMessage());
			return null;
		}
	}
	
	@GetMapping("pacientes/obtener_paciente_dni")
	@ResponseBody
	public PacienteEntity getPaciente(String dni_paciente) {
		PacienteEntity paciente = null;
		try {
			paciente = pacienteDao.obtenerPorDNI(dni_paciente);
			if(paciente != null) {
				int edad = (paciente.getFecha_nacimiento() == null ? -1 : (int) ChronoUnit.YEARS.between(paciente.getFecha_nacimiento(), LocalDate.now()));
				paciente.setEdad(edad);
			}
		}catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("######################################################Error al obtener paciente - paciente asociado. Detalle: " + e.getMessage());
		}
		return paciente;
	}
	
	@PostMapping("pacientes/cambiar_estado_paciente")
	@ResponseBody
	public ResponseEntity<?> cambiarEstadoPaciente(Principal p, int id_paciente, boolean estado_paciente){
		if(p == null) return null;
		PacienteEntity paciente = null;
		try {			
			PacienteEntity update_paciente = pacienteDao.findById((long) id_paciente).get();
			if(estado_paciente) {
				update_paciente.setEstado(0);	
			}else {
				update_paciente.setEstado(1);
			}
			paciente = pacienteDao.save(update_paciente);
			
			if(paciente != null) {
				return new ResponseEntity<String>("Listo",HttpStatus.OK);
			}else {
				return new ResponseEntity<String>("No se encontraron resultados.",HttpStatus.BAD_REQUEST);
			}
			
		} catch (Exception e) {
			LOGGER.error("Error actualizarUsuario: detalle: " + e.getMessage());
			return new ResponseEntity<String>("Se tuvo un error al actualizar el Usuario, favor de contactar al administrador.",HttpStatus.BAD_REQUEST);
		}
	}
}
