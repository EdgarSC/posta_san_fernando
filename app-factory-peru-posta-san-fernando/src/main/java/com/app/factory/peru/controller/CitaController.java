package com.app.factory.peru.controller;

import java.security.Principal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.app.factory.peru.cita.entity.CitaEntity;
import com.app.factory.peru.dao.ICitaDao;
import com.app.factory.peru.dao.IServicioDao;
import com.app.factory.peru.paciente.entity.PacienteEntity;
import com.app.factory.peru.servicios.entity.ServicioEntity;

@RestController
public class CitaController {
	
	private static final Logger LOGGER = LogManager.getLogger(ConsultasController.class);
	
	@Autowired
	private ICitaDao daoCita;
	
	@Autowired
	private IServicioDao servicioDao;
	
	@GetMapping("citas/listar_citas_paciente")
	@ResponseBody
	public List<CitaEntity> listarCitas(String dni_paciente){
		List<CitaEntity> citas = new ArrayList<>();
		try {
			if(dni_paciente.trim().equals(""))
				citas = daoCita.listarCitasXAtencion(1);
			else
				citas = daoCita.listarPorNumeroDNI(dni_paciente);
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("Error al listar citas asociado. Detalle: " + e.getMessage());
		}
		return citas;
	}
	
	@GetMapping("citas/listar_citas_no_atendidas")
	@ResponseBody
	public List<CitaEntity> listarCitasNoAtendidas(){
		List<CitaEntity> citas = new ArrayList<>();
		try {			
			citas = daoCita.listarCitasXAtencion(0);			
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("Error al listar citas no atendidas asociado. Detalle: " + e.getMessage());
		}
		return citas;
	}
	
	@PostMapping("citas/guardar_cita")
	@ResponseBody
	public ResponseEntity<?> guardarCita(Principal p, long servicio_cita, long id_paciente, String fecha_cita, String hora_cita, String celular_cita, int interconsulta){
		try {
			if(p == null) return null;
			LocalDate date_cita = LocalDate.parse(fecha_cita, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
			ServicioEntity servicio = servicioDao.getOne(servicio_cita);
			CitaEntity cita = new CitaEntity();
			cita.setPaciente(new PacienteEntity(id_paciente));
			cita.setFecha_cita(date_cita);
			cita.setHora_cita(LocalTime.parse(hora_cita));
			cita.setCelular_paciente(celular_cita);
			cita.setInterconsulta(interconsulta);
			cita.setServicio(servicio);
			cita.setAtentido(0);
			cita.setEstado(1);
			cita.setCreator(p.getName());
			cita.setCreated(LocalDateTime.now());
			daoCita.save(cita);
			return new ResponseEntity<String>("Listo",HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<String>("Ocurrió un error.",HttpStatus.BAD_REQUEST);
		}
	}
	
	@PostMapping("citas/editar_cita")
	@ResponseBody
	public ResponseEntity<?> editarCita(Principal p, long up_servicio, long id_cita, long id_paciente, String fecha_cita, String hora_cita, String celular_cita, int interconsulta){
		try {
			if(p == null) return null;
			LocalDate date_cita = LocalDate.parse(fecha_cita, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
			ServicioEntity servicio = servicioDao.getOne(up_servicio);
			CitaEntity cita = daoCita.findById(id_cita).get();
			cita.setPaciente(new PacienteEntity(id_paciente));
			cita.setFecha_cita(date_cita);
			cita.setHora_cita(LocalTime.parse(hora_cita));
			cita.setCelular_paciente(celular_cita);
			cita.setServicio(servicio);
			cita.setInterconsulta(interconsulta);
			cita.setChanger(p.getName());
			cita.setChanged(LocalDateTime.now());
			daoCita.save(cita);
			return new ResponseEntity<String>("Listo",HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<String>("Ocurrió un error.",HttpStatus.BAD_REQUEST);
		}
	}
	
	@PostMapping("citas/eliminar_cita")
	@ResponseBody
	public ResponseEntity<?> eliminarCita(Principal p, long id_cita){
		try {
			CitaEntity cita = daoCita.findById(id_cita).get();
			cita.setChanged(LocalDateTime.now());
			cita.setChanger(p.getName());
			cita.setEstado(0);
			daoCita.save(cita);
			return new ResponseEntity<String>("Listo",HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<String>("No se encontraron resultados.",HttpStatus.BAD_REQUEST);
		}
	}
	
	@PostMapping("citas/cambiar_atendido_cita")
	@ResponseBody
	public ResponseEntity<?> cambiarAtendidoCita(Principal p, int id_cita, boolean atendido_cita){
		if(p == null) return null;
		CitaEntity cita = null;
		try {			
			Optional<CitaEntity> citas = daoCita.findById((long) id_cita);
			CitaEntity update_citas = citas.get();
			update_citas.setAtentido(1);
			cita = daoCita.save(update_citas);
			
			if(cita != null) {
				return new ResponseEntity<String>("Listo",HttpStatus.OK);
			}else {
				return new ResponseEntity<String>("No se encontraron resultados.",HttpStatus.BAD_REQUEST);
			}
			
		} catch (Exception e) {
			LOGGER.error("Error actualizarAtendidoCita: detalle: " + e.getMessage());
			return new ResponseEntity<String>("Se tuvo un error al actualizar la cita, favor de contactar al administrador.",HttpStatus.BAD_REQUEST);
		}
	}
}
