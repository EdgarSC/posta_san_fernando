package com.app.factory.peru.controller;

import java.util.regex.Pattern;

public class Validaciones {
	
	public static boolean compararExpresionesRegulares(String regexp, String dato) {
		if(Pattern.matches(regexp, dato)) {
			return true;
		}
		return false;
	}
	
}
