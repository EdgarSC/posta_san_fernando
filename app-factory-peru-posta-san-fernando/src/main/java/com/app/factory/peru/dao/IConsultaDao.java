package com.app.factory.peru.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.app.factory.peru.consultas.entity.ConsultaEntity;

public interface IConsultaDao extends JpaRepository<ConsultaEntity, Long> {
	
	@Query("FROM ConsultaEntity WHERE paciente.numero_documento = :numero_documento ORDER BY created DESC")
	List<ConsultaEntity> listarPorNumeroDNI(String numero_documento);
	
	@Query("FROM ConsultaEntity WHERE paciente.numero_documento = :numero_documento ORDER BY created DESC")
	List<ConsultaEntity> listarUltimaConsulta(String numero_documento);
	
	@Query("FROM ConsultaEntity ORDER BY created DESC")
	List<ConsultaEntity> listarTodas();
	
}
