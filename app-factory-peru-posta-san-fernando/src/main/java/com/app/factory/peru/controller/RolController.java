package com.app.factory.peru.controller;

import java.security.Principal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.app.factory.peru.dao.IRolNewDao;
import com.app.factory.peru.rol.entity.RolEntity;

@RestController
public class RolController {
	
	private static final Logger LOGGER = LogManager.getLogger(RolController.class);
	
	@Autowired
	private IRolNewDao rolDao;
	
	@GetMapping("rol/listar_roles")
	@ResponseBody
	public ResponseEntity<?> listarRoles(Principal p){
		if(p == null) return null;
		try {			
			
			List<RolEntity> roles = rolDao.findAll();		
			if(roles.size() > 0) {
				return new ResponseEntity<List<RolEntity>>(roles,HttpStatus.OK);
			}else {
				return new ResponseEntity<String>("No se encontraron resultados.",HttpStatus.BAD_REQUEST);
			}
			
		} catch (Exception e) {
			LOGGER.error("Error getrolesList: detalle: " + e.getMessage());
			return new ResponseEntity<String>("Se tuvo un error al obtener la lista de roles, favor de contactar al administrador.",HttpStatus.BAD_REQUEST);
		}
	}
	
	@GetMapping("rol/listar_roles_activos")
	@ResponseBody
	public ResponseEntity<?> listarRolesActivos(Principal p){
		if(p == null) return null;
		try {			
			
			List<RolEntity> servicios = rolDao.findByestado(1);
			if(servicios.size() > 0) {
				return new ResponseEntity<List<RolEntity>>(servicios,HttpStatus.OK);
			}else {
				return new ResponseEntity<String>("No se encontraron resultados.",HttpStatus.BAD_REQUEST);
			}
			
		} catch (Exception e) {
			LOGGER.error("Error getRolesList: detalle: " + e.getMessage());
			return new ResponseEntity<String>("Se tuvo un error al obtener la lista de Roles, favor de contactar al administrador.",HttpStatus.BAD_REQUEST);
		}
	}
	
	@PostMapping("rol/guardar_roles")
	@ResponseBody
	public ResponseEntity<?> guardarRoles(Principal p, String nombre, String descripcion){
		if(p == null) return null;
		RolEntity rol = null;
		try {			
			RolEntity save_rol = new RolEntity();
			save_rol.setName(nombre);
			save_rol.setDescripcion(descripcion);
			save_rol.setEstado(1);			
			save_rol.setCreator(p.getName());
			save_rol.setCreated(LocalDateTime.now());
			
			rol = rolDao.save(save_rol);
			
			if(rol != null) {
				return new ResponseEntity<String>("Listo",HttpStatus.OK);
			}else {
				return new ResponseEntity<String>("No se encontraron resultados.",HttpStatus.BAD_REQUEST);
			}
			
		} catch (Exception e) {
			LOGGER.error("Error guardarServicio: detalle: " + e.getMessage());
			return new ResponseEntity<String>("Se tuvo un error al guarda el rol, favor de contactar al administrador.",HttpStatus.BAD_REQUEST);			
		}
	}
	
	@PostMapping("rol/cambiar_estado_rol")
	@ResponseBody
	public ResponseEntity<?> cambiarEstadoRol(Principal p, int id_rol, boolean estado){
		if(p == null) return null;
		RolEntity rol = null;
		try {			
			Optional<RolEntity> _rol = rolDao.findById((long) id_rol);
			RolEntity update_rol = _rol.get();
			if(estado) {
				update_rol.setEstado(0);	
			}else {
				update_rol.setEstado(1);
			}
			rol = rolDao.save(update_rol);
			
			if(rol != null) {
				return new ResponseEntity<String>("Listo",HttpStatus.OK);
			}else {
				return new ResponseEntity<String>("No se encontraron resultados.",HttpStatus.BAD_REQUEST);
			}
			
		} catch (Exception e) {
			LOGGER.error("Error actualizarServicio: detalle: " + e.getMessage());
			return new ResponseEntity<String>("Se tuvo un error al actualizar el rol, favor de contactar al administrador.",HttpStatus.BAD_REQUEST);
		}
	}
	
	@PostMapping("rol/actualizar_roles")
	@ResponseBody
	public ResponseEntity<?> actualizarRoles(Principal p, int id_rol, String new_nombre, String new_descripcion){
		if(p == null) return null;
		RolEntity rol = null;
		try {			
			Optional<RolEntity> _rol = rolDao.findById((long) id_rol);
			RolEntity update_rol = _rol.get();
			update_rol.setName(new_nombre);
			update_rol.setDescripcion(new_descripcion);
			update_rol.setChanger(p.getName());
			update_rol.setChanged(LocalDateTime.now());
			rol = rolDao.save(update_rol);
			
			if(rol != null) {
				return new ResponseEntity<String>("Listo",HttpStatus.OK);
			}else {
				return new ResponseEntity<String>("No se encontraron resultados.",HttpStatus.BAD_REQUEST);
			}
			
		} catch (Exception e) {
			LOGGER.error("Error actualizarServicio: detalle: " + e.getMessage());
			return new ResponseEntity<String>("Se tuvo un error al actualizar el rol, favor de contactar al administrador.",HttpStatus.BAD_REQUEST);
		}
	}
	
}
