package com.app.factory.peru.servicios.entity;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name ="SERVICIOS")
public class ServicioEntity {
	
	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	@Column(name ="SERVICIOID")
	private Long servicioId;
	@Column(name = "NOMBRE")
	private String nombre;
	@Column(name = "DESCRIPCION")
	private String descripcion;
	@Column(name = "ESTADO")
	private int estado;
	@Column(name ="CREATOR")
	private String creator;
	@JsonFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	@Column(name ="CREATED")
	private LocalDateTime created;
	@Column(name="CHANGER")
	private String changer;
	@JsonFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	@Column(name= "CHANGED")
	private LocalDateTime changed;
	
	public Long getServicioId() {
		return servicioId;
	}
	public void setServicioId(Long servicioId) {
		this.servicioId = servicioId;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public int getEstado() {
		return estado;
	}
	public void setEstado(int estado) {
		this.estado = estado;
	}
	public String getCreator() {
		return creator;
	}
	public void setCreator(String creator) {
		this.creator = creator;
	}
	public LocalDateTime getCreated() {
		return created;
	}
	public void setCreated(LocalDateTime created) {
		this.created = created;
	}
	public String getChanger() {
		return changer;
	}
	public void setChanger(String changer) {
		this.changer = changer;
	}
	public LocalDateTime getChanged() {
		return changed;
	}
	public void setChanged(LocalDateTime changed) {
		this.changed = changed;
	}
	@Override
	public String toString() {
		return "ServicioEntity [servicioId=" + servicioId + ", nombre=" + nombre + ", descripcion=" + descripcion
				+ ", estado=" + estado + ", creator=" + creator + ", created=" + created + ", changer=" + changer
				+ ", changed=" + changed + "]";
	}
	
	
}
