package com.app.factory.peru;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppFactoryPeruPostaSanFernandoApplication {

	public static void main(String[] args) {
		SpringApplication.run(AppFactoryPeruPostaSanFernandoApplication.class, args);
	}

}
