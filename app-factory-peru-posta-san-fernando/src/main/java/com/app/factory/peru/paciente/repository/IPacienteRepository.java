package com.app.factory.peru.paciente.repository;

import org.springframework.data.jpa.repository.Query;

import com.app.factory.peru.generic.repository.IGenericRepo;
import com.app.factory.peru.paciente.entity.PacienteEntity;

public interface IPacienteRepository extends IGenericRepo<PacienteEntity, Integer>{

	@Query("FROM PacienteEntity WHERE numero_documento = :numero_documento")
	PacienteEntity obtenerPorDNI(String numero_documento); 
	
}
