package com.app.factory.peru.usuario.entity;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

import com.app.factory.peru.rol.entity.RolEntity;
import com.app.factory.peru.servicios.entity.ServicioEntity;
import com.app.factory.peru.tipoUsuario.entity.TipoUsuarioEntity;
import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "USUARIO")
public class UsuarioEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="USUARIOID")
	private Long usuarioId;
	@Column(name="USUARIO")
	private String usuario;
	@Column(name="CONTRASENA")
	private String contrasena;
	@Column(name="NOMBRE")
	private String nombre;
	@Column(name="APELLIDOPATERNO")
	private String apellidoPaterno;
	@Column(name="APELLIDOMATERNO")
	private String apellidoMaterno;
	@Column(name="NUMERODOCUMENTO")
	private String numeroDocumento;
	@Column(name="FECHANACIMIENTO")
	private LocalDate fechaNacimiento;
	@Column(name="CORREO")
	private String correo;
	@Column(name="TELEFONO")
	private String telefono;
	@Column(name="CELULAR")
	private String celular;
	@Column(name="ESTADO")
	private int estado;
	@Column(name="CREATOR")
	private String creator;
	@JsonFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	@Column(name="CREATED")
	private LocalDateTime created;
	@Column(name="CHANGER")
	private String changer;
	@JsonFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	@Column(name="CHANGED")
	private LocalDateTime changed;
	@OneToOne
	@JoinColumn(name="TIPOUSUARIOID")
	private TipoUsuarioEntity tipousuario;
	@OneToOne
	@JoinColumn(name = "SERVICIOID")
	private ServicioEntity servicio;
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "USUARIOSROLES",
			joinColumns=@JoinColumn(name="USUARIOID"),
			inverseJoinColumns=@JoinColumn(name="ROLID"))
	private Set<RolEntity> roles;

	public UsuarioEntity() {
		super();
	}
	
	public ServicioEntity getServicio() {
		return servicio;
	}
	public void setServicio(ServicioEntity servicio) {
		this.servicio = servicio;
	}

	public Long getUsuarioId() {
		return usuarioId;
	}


	public void setUsuarioId(Long usuarioId) {
		this.usuarioId = usuarioId;
	}

	public String getUsuario() {
		return usuario;
	}


	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}


	public String getContrasena() {
		return contrasena;
	}


	public void setContrasena(String contrasena) {
		this.contrasena = contrasena;
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public String getApellidoPaterno() {
		return apellidoPaterno;
	}


	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}


	public String getApellidoMaterno() {
		return apellidoMaterno;
	}


	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}


	public String getNumeroDocumento() {
		return numeroDocumento;
	}


	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}


	public String getCorreo() {
		return correo;
	}


	public void setCorreo(String correo) {
		this.correo = correo;
	}


	public String getTelefono() {
		return telefono;
	}


	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}


	public String getCelular() {
		return celular;
	}


	public void setCelular(String celular) {
		this.celular = celular;
	}


	public int getEstado() {
		return estado;
	}


	public void setEstado(int estado) {
		this.estado = estado;
	}


	public String getCreator() {
		return creator;
	}


	public void setCreator(String creator) {
		this.creator = creator;
	}

	public String getChanger() {
		return changer;
	}


	public void setChanger(String changer) {
		this.changer = changer;
	}

	
	
	public TipoUsuarioEntity getTipousuario() {
		return tipousuario;
	}

	public void setTipousuario(TipoUsuarioEntity tipousuario) {
		this.tipousuario = tipousuario;
	}

	public Set<RolEntity> getRoles() {
		return roles;
	}

	public void setRoles(Set<RolEntity> roles) {
		this.roles = roles;
	}

	public LocalDate getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(LocalDate fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public LocalDateTime getCreated() {
		return created;
	}

	public void setCreated(LocalDateTime created) {
		this.created = created;
	}

	public LocalDateTime getChanged() {
		return changed;
	}

	public void setChanged(LocalDateTime changed) {
		this.changed = changed;
	}

	@Override
	public String toString() {
		return "UsuarioEntity [usuarioId=" + usuarioId + ", usuario=" + usuario + ", contrasena=" + contrasena
				+ ", nombre=" + nombre + ", apellidoPaterno=" + apellidoPaterno + ", apellidoMaterno=" + apellidoMaterno
				+ ", numeroDocumento=" + numeroDocumento + ", fechaNacimiento=" + fechaNacimiento + ", correo=" + correo
				+ ", telefono=" + telefono + ", celular=" + celular + ", estado=" + estado + ", creator=" + creator
				+ ", created=" + created + ", changer=" + changer + ", changed=" + changed + ", tipousuario="
				+ tipousuario + ", servicio=" + servicio + ", roles=" + roles + "]";
	}
	
}
