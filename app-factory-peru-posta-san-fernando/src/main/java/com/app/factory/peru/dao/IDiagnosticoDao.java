package com.app.factory.peru.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.app.factory.peru.diagnostico.entity.DiagnosticoEntity;


public interface IDiagnosticoDao extends JpaRepository<DiagnosticoEntity, Long> {
	
	@Query("FROM DiagnosticoEntity WHERE consulta.id_consulta = :id_consulta")
	List<DiagnosticoEntity> listarDiagnosticos(long id_consulta);
}
