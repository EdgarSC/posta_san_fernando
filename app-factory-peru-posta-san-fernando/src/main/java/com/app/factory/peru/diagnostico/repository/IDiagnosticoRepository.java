package com.app.factory.peru.diagnostico.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;

import com.app.factory.peru.diagnostico.entity.DiagnosticoEntity;
import com.app.factory.peru.generic.repository.IGenericRepo;

public interface IDiagnosticoRepository extends IGenericRepo<DiagnosticoEntity, Integer>{

	@Query("FROM DiagnosticoEntity WHERE consulta.id_consulta = :id_consulta")
	List<DiagnosticoEntity> listarDiagnosticos(long id_consulta);
	
}
