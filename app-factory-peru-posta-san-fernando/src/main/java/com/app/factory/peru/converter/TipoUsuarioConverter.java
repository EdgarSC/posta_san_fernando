package com.app.factory.peru.converter;

import java.text.SimpleDateFormat;
import org.springframework.stereotype.Component;
import com.app.factory.peru.dto.TipoUsuarioDTO;
import com.app.factory.peru.tipoUsuario.entity.TipoUsuarioEntity;

@Component("tipoUsuarioConverter")
public class TipoUsuarioConverter {
	
	public TipoUsuarioEntity dtoToEntity(TipoUsuarioDTO prTipoUsuarioDTO) throws Exception {

		TipoUsuarioEntity lTipoUsuarioEntity = new TipoUsuarioEntity();
		SimpleDateFormat lSimpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		lTipoUsuarioEntity.setTipoUsuarioId(prTipoUsuarioDTO.getTipoUsuarioId());
		lTipoUsuarioEntity.setNombre(prTipoUsuarioDTO.getNombre());
		lTipoUsuarioEntity.setDescripcion(prTipoUsuarioDTO.getDescripcion());
		lTipoUsuarioEntity.setEstado(prTipoUsuarioDTO.getEstado());
		
		if (prTipoUsuarioDTO.getCreatedDescripcion() != null && !prTipoUsuarioDTO.getCreatedDescripcion().isEmpty()
				&& prTipoUsuarioDTO.getCreated() == null) {
//			lTipoUsuarioEntity.setCreated(lSimpleDateFormat.parse(prTipoUsuarioDTO.getCreatedDescripcion()));
		}
		
		lTipoUsuarioEntity.setCreator(prTipoUsuarioDTO.getCreator());
		
		return lTipoUsuarioEntity;
	}

	public TipoUsuarioDTO entityToDTO(TipoUsuarioEntity prTipoUsuarioEntity) {

		TipoUsuarioDTO lTipoUsuarioDTO = new TipoUsuarioDTO();

		lTipoUsuarioDTO.setTipoUsuarioId(prTipoUsuarioEntity.getTipoUsuarioId());
		lTipoUsuarioDTO.setNombre(prTipoUsuarioEntity.getNombre());
		lTipoUsuarioDTO.setDescripcion(prTipoUsuarioEntity.getDescripcion());
		lTipoUsuarioDTO.setCreator(prTipoUsuarioEntity.getCreator());
		lTipoUsuarioDTO.setEstado(prTipoUsuarioEntity.getEstado());
		lTipoUsuarioDTO.setEstadoDescripcion((prTipoUsuarioEntity.getEstado() == 0) ? "Inactivo" : "Activo");

		SimpleDateFormat lSimpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

		lTipoUsuarioDTO.setCreatedDescripcion(lSimpleDateFormat.format(prTipoUsuarioEntity.getCreated()));

		return lTipoUsuarioDTO;
	}
}
