package com.app.factory.peru.consultas.entity;

import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

import com.app.factory.peru.paciente.entity.PacienteEntity;
import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "CONSULTA")
public class ConsultaEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="CONSULTAID")
	private Long id_consulta;
	
	@OneToOne
	@JoinColumn(name = "PACIENTEID")
	private PacienteEntity paciente;
	
	@Column(name="MENARQUIA")
	private String menarquia;
	
	@Column(name="METCONCEPTIVO")
	private String metconceptivo;
	
	@Column(name="REGIMENCATAMENIAL")
	private String regimen_catamenial;
	
	@Column(name="ORIENTACIONSEXUAL")
	private String orientacion_sexual;
	
	@JsonFormat(pattern = "dd/MM/yyyy")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Column(name="FUR")
	private LocalDate fur;
	
	@Column(name="NROPAREJASEXUALES")
	private String nro_parejas_sexuales;
	
	@Column(name="GESTACIONES")
	private String gestaciones;
	
	@Column(name="PARTOS")
	private String partos;
	
	@Column(name="TALLA")
	private double talla;
	
	@Column(name="PESO")
	private double peso;
	
	@Column(name="IMC")
	private double imc;
	
	@Column(name="PRESIONARTERIA")
	private String presion_arterial;
	
	@Column(name="FRECUENCIACARDIACA")
	private String frecuencia_cardiaca;
	
	@Column(name="TEMPERATURA")
	private String temperatura;
	
	@Column(name="FRECUENCIARESPIRATORIA")
	private String frecuencia_respiratoria;
	
	@Column(name="SATURACION")
	private String saturacion;
	
	@Column(name="MOTIVOCONSULTA")
	private String motivo_consulta;
	
	@Column(name="PLANTRABAJO")
	private String plan_trabajo;
	
	@Column(name="PRESUNCIONDIAGNOSTICA")
	private String presuncion_diagnostica;
	
	@Column(name="INTERCONSULTAA")
	private String inter_consulta;
	
	@Column(name="SERVICIO")
	private String servicio;
	
	@Column(name="PERSONALENCARGADO")
	private String personal_encargado;
	
	@JsonFormat(pattern = "dd/MM/yyyy")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Column(name="FECHA")
	private LocalDate fecha;
	
	@Column(name="CREATOR")
	private String creator;
	
	@JsonFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	@Column(name="CREATED")
	private LocalDateTime created;
	
	@Column(name="CHANGER")
	private String changer;
	
	@JsonFormat(pattern = "dd/MM/yyyy")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Column(name="CHANGED")
	private LocalDate changed;

	public Long getId_consulta() {
		return id_consulta;
	}

	public void setId_consulta(Long id_consulta) {
		this.id_consulta = id_consulta;
	}
	
	public PacienteEntity getPaciente() {
		return paciente;
	}

	public void setPaciente(PacienteEntity paciente) {
		this.paciente = paciente;
	}

	public String getMenarquia() {
		return menarquia;
	}

	public void setMenarquia(String menarquia) {
		this.menarquia = menarquia;
	}

	public String getMetconceptivo() {
		return metconceptivo;
	}

	public void setMetconceptivo(String metconceptivo) {
		this.metconceptivo = metconceptivo;
	}

	public String getRegimen_catamenial() {
		return regimen_catamenial;
	}

	public void setRegimen_catamenial(String regimen_catamenial) {
		this.regimen_catamenial = regimen_catamenial;
	}

	public String getOrientacion_sexual() {
		return orientacion_sexual;
	}

	public void setOrientacion_sexual(String orientacion_sexual) {
		this.orientacion_sexual = orientacion_sexual;
	}

	public LocalDate getFur() {
		return fur;
	}

	public void setFur(LocalDate fur) {
		this.fur = fur;
	}

	public String getNro_parejas_sexuales() {
		return nro_parejas_sexuales;
	}

	public void setNro_parejas_sexuales(String nro_parejas_sexuales) {
		this.nro_parejas_sexuales = nro_parejas_sexuales;
	}

	public String getGestaciones() {
		return gestaciones;
	}

	public void setGestaciones(String gestaciones) {
		this.gestaciones = gestaciones;
	}

	public String getPartos() {
		return partos;
	}

	public void setPartos(String partos) {
		this.partos = partos;
	}

	public double getTalla() {
		return talla;
	}

	public void setTalla(double talla) {
		this.talla = talla;
	}

	public double getPeso() {
		return peso;
	}

	public void setPeso(double peso) {
		this.peso = peso;
	}

	public double getImc() {
		return imc;
	}

	public void setImc(double imc) {
		this.imc = imc;
	}

	public String getPresion_arterial() {
		return presion_arterial;
	}

	public void setPresion_arterial(String presion_arterial) {
		this.presion_arterial = presion_arterial;
	}

	public String getFrecuencia_cardiaca() {
		return frecuencia_cardiaca;
	}

	public void setFrecuencia_cardiaca(String frecuencia_cardiaca) {
		this.frecuencia_cardiaca = frecuencia_cardiaca;
	}

	public String getSaturacion() {
		return saturacion;
	}

	public void setSaturacion(String saturacion) {
		this.saturacion = saturacion;
	}

	public String getMotivo_consulta() {
		return motivo_consulta;
	}

	public void setMotivo_consulta(String motivo_consulta) {
		this.motivo_consulta = motivo_consulta;
	}

	public String getPlan_trabajo() {
		return plan_trabajo;
	}

	public void setPlan_trabajo(String plan_trabajo) {
		this.plan_trabajo = plan_trabajo;
	}

	public String getPresuncion_diagnostica() {
		return presuncion_diagnostica;
	}

	public void setPresuncion_diagnostica(String presuncion_diagnostica) {
		this.presuncion_diagnostica = presuncion_diagnostica;
	}

	public String getInter_consulta() {
		return inter_consulta;
	}

	public void setInter_consulta(String inter_consulta) {
		this.inter_consulta = inter_consulta;
	}

	public String getServicio() {
		return servicio;
	}

	public void setServicio(String servicio) {
		this.servicio = servicio;
	}

	public String getPersonal_encargado() {
		return personal_encargado;
	}

	public void setPersonal_encargado(String personal_encargado) {
		this.personal_encargado = personal_encargado;
	}

	public LocalDate getFecha() {
		return fecha;
	}

	public void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}
	
	public LocalDateTime getCreated() {
		return created;
	}

	public void setCreated(LocalDateTime created) {
		this.created = created;
	}

	public String getChanger() {
		return changer;
	}

	public void setChanger(String changer) {
		this.changer = changer;
	}

	public LocalDate getChanged() {
		return changed;
	}

	public void setChanged(LocalDate changed) {
		this.changed = changed;
	}

	public String getTemperatura() {
		return temperatura;
	}

	public void setTemperatura(String temperatura) {
		this.temperatura = temperatura;
	}

	public String getFrecuencia_respiratoria() {
		return frecuencia_respiratoria;
	}

	public void setFrecuencia_respiratoria(String frecuencia_respiratoria) {
		this.frecuencia_respiratoria = frecuencia_respiratoria;
	}
	
}