package com.app.factory.peru.receta.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;

import com.app.factory.peru.generic.repository.IGenericRepo;
import com.app.factory.peru.receta.entity.RecetaEntity;

public interface IRecetaRepository extends IGenericRepo <RecetaEntity, Integer> {
	
	@Query("FROM RecetaEntity WHERE consulta.id_consulta = :id_consulta")
	List<RecetaEntity> listarMedicamentos(long id_consulta);
}
