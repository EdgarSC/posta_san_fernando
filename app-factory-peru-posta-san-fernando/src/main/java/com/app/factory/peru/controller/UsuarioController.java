package com.app.factory.peru.controller;

import java.security.Principal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.app.factory.peru.dao.IRolNewDao;
import com.app.factory.peru.dao.IServicioDao;
import com.app.factory.peru.dao.ITipoUsuarioNewDao;
import com.app.factory.peru.dao.IUsuarioNewDao;
import com.app.factory.peru.dao.IUsuariosRolesDao;
import com.app.factory.peru.rol.entity.RolEntity;
import com.app.factory.peru.servicios.entity.ServicioEntity;
import com.app.factory.peru.tipoUsuario.entity.TipoUsuarioEntity;
import com.app.factory.peru.usuario.entity.UsuarioEntity;
import com.app.factory.peru.usuarioRol.entity.UsuarioRolesEntity;

@RestController
public class UsuarioController {
	private static final Logger LOGGER = LogManager.getLogger(UsuarioController.class);
		
	@Autowired
	private IUsuarioNewDao usuarioDao;
	
	@Autowired
	private IServicioDao servicioDao;
	
	@Autowired
	private ITipoUsuarioNewDao tipoUsuarioDao;
	
	@Autowired
	private IUsuariosRolesDao useRolDao;
	
	@Autowired
	private IRolNewDao rolDao;
	
	@GetMapping("usuario/listar_usuarios")
	@ResponseBody
	public ResponseEntity<?> listarUsuarios(Principal p){
		if(p == null) return null;
		try {			
			
			List<UsuarioEntity> usuarios = usuarioDao.findAll();
			if(usuarios.size() > 0) {
				return new ResponseEntity<List<UsuarioEntity>>(usuarios,HttpStatus.OK);
			}else {
				return new ResponseEntity<String>("No se encontraron resultados.",HttpStatus.BAD_REQUEST);
			}
			
		} catch (Exception e) {
			LOGGER.error("Error getUsuariosList: detalle: " + e.getMessage());
			return new ResponseEntity<String>("Se tuvo un error al obtener la lista de Usuarios, favor de contactar al administrador.",HttpStatus.BAD_REQUEST);
		}
	}
	
	@PostMapping("usuario/guardar_usuarios")
	@ResponseBody
	public String guardarUsuarios(Principal p, String nw_usuario, String nw_pass, String nw_nueva_pass, String nw_nombres, String nw_ap_paterno,
			String nw_ape_materno,String nw_num_documento, String nw_f_nacimiento,String nw_telefono, String nw_celular,
			String nw_email, int nw_tipo_user, int nw_servicio){
		if(p == null) return null;
		String msj = "Listo";
		BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder(4);		
		UsuarioEntity dni = usuarioDao.findBynumeroDocumento(nw_num_documento);		
		if(nw_usuario.equals("")) {msj=":Importante!, debe llenar el campo usuario.";
		}else if(!Validaciones.compararExpresionesRegulares("[a-z]{8}", nw_usuario)) {msj =":El campo usuario solo permite 8 letras minúsculas.";
		}else if(!Validaciones.compararExpresionesRegulares("^(?=\\w*\\d)(?=\\w*[A-Z])(?=\\w*[a-z])\\S{8,16}$", nw_pass)) {msj =":La contraseña debe ser mayor o igual a 8 dígitos entre números, mayúsculas y minúsculas. No permite comillas.";
		}else if(!nw_pass.equals(nw_nueva_pass)) {msj =":Las contraseñas no coinciden.";
		}else if(!Validaciones.compararExpresionesRegulares("\\d{8}", nw_num_documento)) {msj = ":El N° documento solo permite 8 números.";
		}else if(dni != null) {	msj =":El dni ya existe, ingrese el correcto.";
		}else if(!Validaciones.compararExpresionesRegulares("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\\.[a-zA-Z0-9-]+)*$", nw_email)) {msj = ":El email no corresponde con la expresión introducida.";
		}else {		
			try {			
				ServicioEntity servicio = servicioDao.getOne((long)nw_servicio);
				TipoUsuarioEntity tipoUser = tipoUsuarioDao.getOne((long) nw_tipo_user );
				DateTimeFormatter fmt = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		        LocalDate fecha_nacimiento = LocalDate.parse(nw_f_nacimiento, fmt);
				UsuarioEntity save_usuario = new UsuarioEntity();
				save_usuario.setUsuario(nw_usuario);
				save_usuario.setContrasena(bCryptPasswordEncoder.encode(nw_pass));
				save_usuario.setNombre(nw_nombres);
				save_usuario.setApellidoPaterno(nw_ap_paterno);
				save_usuario.setApellidoMaterno(nw_ape_materno);
				save_usuario.setNumeroDocumento(nw_num_documento);
				save_usuario.setFechaNacimiento(fecha_nacimiento);
				save_usuario.setTelefono(nw_telefono);
				save_usuario.setCelular(nw_celular);
				save_usuario.setCorreo(nw_email);
				save_usuario.setTipousuario(tipoUser);
				save_usuario.setServicio(servicio);
				save_usuario.setEstado(1);
				save_usuario.setCreator(p.getName());
				save_usuario.setCreated(LocalDateTime.now());
				
				UsuarioRolesEntity save_userRol = new UsuarioRolesEntity();
				Optional<RolEntity> obtenerRol = rolDao.findById((long) UsuarioRolesEntity.CONSULTOR);
				save_userRol.setUsuario(save_usuario);
				save_userRol.setRol(obtenerRol.get());
				save_userRol.setEstado(1);
				
				usuarioDao.save(save_usuario);
				useRolDao.save(save_userRol);
							
			} catch (Exception e) {
				LOGGER.error("Error guardarUsuario: detalle: " + e.getMessage());
				return null;			
			}
		}
		return msj;
	}
	
	@PostMapping("usuario/cambiar_estado_usuario")
	@ResponseBody
	public ResponseEntity<?> cambiarEstadoUsuario(Principal p, int id_usuario, boolean estado){
		if(p == null) return null;
		UsuarioEntity user = null;
		try {			
			Optional<UsuarioEntity> usuario = usuarioDao.findById((long) id_usuario);
			UsuarioEntity update_usuario = usuario.get();
			if(estado) {
				update_usuario.setEstado(0);	
			}else {
				update_usuario.setEstado(1);
			}
			user = usuarioDao.save(update_usuario);
			
			if(user != null) {
				return new ResponseEntity<String>("Listo",HttpStatus.OK);
			}else {
				return new ResponseEntity<String>("No se encontraron resultados.",HttpStatus.BAD_REQUEST);
			}
			
		} catch (Exception e) {
			LOGGER.error("Error actualizarUsuario: detalle: " + e.getMessage());
			return new ResponseEntity<String>("Se tuvo un error al actualizar el Usuario, favor de contactar al administrador.",HttpStatus.BAD_REQUEST);
		}
	}
	
	@PostMapping("usuario/actualizar_usuarios")
	@ResponseBody
	public String actualizarUsuarios(Principal p, int id_user, String nw_usuario, String nw_nombres, String nw_ap_paterno,
			String nw_ape_materno,String nw_num_documento, String nw_f_nacimiento,String nw_telefono, String nw_celular,
			String nw_email, int nw_tipo_user, int nw_servicio){
		if(p == null) return null;
		String msj = "Listo";	
		UsuarioEntity userXdni = usuarioDao.findBynumeroDocumento(nw_num_documento);		
		if(nw_usuario.equals("")) {msj=":Importante!, debe llenar el campo usuario.";
		}else if(!Validaciones.compararExpresionesRegulares("[a-z]{8}", nw_usuario)) {msj =":El campo usuario solo permite 8 letras minúsculas.";				
		}else if(!Validaciones.compararExpresionesRegulares("\\d{8}", nw_num_documento)) {msj = ":El N° documento solo permite 8 números.";
		}else if(userXdni != null && userXdni.getUsuarioId() != id_user) {	msj =":El dni ya existe, ingrese el correcto.";
		}else if(!Validaciones.compararExpresionesRegulares("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\\.[a-zA-Z0-9-]+)*$", nw_email)) {msj = ":El email no corresponde con la expresión introducida.";
		}else {	
			try {			
				Optional<UsuarioEntity> usuarioId = usuarioDao.findById((long) id_user);
				UsuarioEntity update_usuario = usuarioId.get();
				ServicioEntity servicio = servicioDao.getOne((long)nw_servicio);
				TipoUsuarioEntity tipoUser = tipoUsuarioDao.getOne((long) nw_tipo_user );
				DateTimeFormatter fmt = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		        LocalDate fecha_nacimiento = LocalDate.parse(nw_f_nacimiento, fmt);
				update_usuario.setUsuario(nw_usuario);
				update_usuario.setNombre(nw_nombres);
				update_usuario.setApellidoPaterno(nw_ap_paterno);
				update_usuario.setApellidoMaterno(nw_ape_materno);
				update_usuario.setNumeroDocumento(nw_num_documento);
				update_usuario.setFechaNacimiento(fecha_nacimiento);
				update_usuario.setTelefono(nw_telefono);
				update_usuario.setCelular(nw_celular);
				update_usuario.setCorreo(nw_email);
				update_usuario.setTipousuario(tipoUser);
				update_usuario.setServicio(servicio);			
				update_usuario.setChanger(p.getName());
				update_usuario.setChanged(LocalDateTime.now());
						
				usuarioDao.save(update_usuario);
			} catch (Exception e) {
				LOGGER.error("Error actualizarUsuarios: detalle: " + e.getMessage());
				return null;
			}
		}
		return msj;
	}
	
	@GetMapping("usuario/listar_usuarios_activos")
	@ResponseBody
	public ResponseEntity<?> listarUsuariosActivos(Principal p){
		if(p == null) return null;
		try {			
			
			List<UsuarioEntity> usuarios = usuarioDao.findByestado(1);
			if(usuarios.size() > 0) {
				return new ResponseEntity<List<UsuarioEntity>>(usuarios,HttpStatus.OK);
			}else {
				return new ResponseEntity<String>("No se encontraron resultados.",HttpStatus.BAD_REQUEST);
			}
			
		} catch (Exception e) {
			LOGGER.error("Error getUsuariosList: detalle: " + e.getMessage());
			return new ResponseEntity<String>("Se tuvo un error al obtener la lista de Usuarios, favor de contactar al administrador.",HttpStatus.BAD_REQUEST);
		}
	}
	
}
