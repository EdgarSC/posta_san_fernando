package com.app.factory.peru.util;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class Passgenerator {
	//===========NO ELIMINAR LO COMENTADO======
//	public static void main(String ...args) {
//        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder(4);
       
        //El String que mandamos al metodo encode es el password que queremos encriptar.
//        System.out.println(bCryptPasswordEncoder.encode("123"));
//        /*
//         * Resultado: $2a$04$n6WIRDQlIByVFi.5rtQwEOTAzpzLPzIIG/O6quaxRKY2LlIHG8uty
//         */
//    }
	
	public static String encodePassword (String password) {
		 BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder(4);
		 return bCryptPasswordEncoder.encode(password);
	}
}