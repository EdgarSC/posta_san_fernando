package com.app.factory.peru.controller;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.app.factory.peru.dao.IUsuarioNewDao;
import com.app.factory.peru.dao.IUsuariosRolesDao;
import com.app.factory.peru.usuario.entity.UsuarioEntity;
import com.app.factory.peru.usuarioRol.entity.UsuarioRolesEntity;


@Controller
public class AdminPageController {
	
	@Autowired
	private IUsuariosRolesDao userRolDao;
	
	@Autowired
	private IUsuarioNewDao usuarioDao;
	
	@RequestMapping({"/","/index","/login"})
    public String login(){
        return "admin/Login/Login";
    }
	
	@RequestMapping({"/admin/dashboard","/admin"})
    public String dashboard(){
        return "admin/dashboard";
    }
    
    @RequestMapping("/admin/user/paciente")
    public String searchPatient() {
    	return  "admin/GestionPacientes/BuscarGestionPaciente";
    }
    
    @RequestMapping("/admin/citas")
    public String searchCita() {
    	return  "admin/GestionCitas/Citas";
    }

    @RequestMapping("/admin/user/listar_consultas")
    public String searchConsult() {
    	return "admin/GestionConsulta/ListarConsultas";
    }
    
    @RequestMapping("/admin/tipoUsuario")
    public String searchTipoUsuario(Principal p){
    	UsuarioEntity usuario = usuarioDao.obtenerXusuarioYEstado(p.getName(), 1);
    	UsuarioRolesEntity agente_admin = userRolDao.obtenerXUsuarioRol(usuario.getUsuarioId(), UsuarioRolesEntity.CONSULTOR);
    	if(agente_admin == null) {
    		return "admin/Configuraciones/TipoUsuario/TipoUsuario";
    	}else {
    		return "admin/Error/404";
    	}
    }
    
    @RequestMapping("/tipoUsuario/tipoUsuarioView")
    public String tipoUsuarioView() {
    	return "admin/Configuraciones/TipoUsuario/NewTipoUsuario";
    }
    
    @RequestMapping("/admin/usuario")
    public String usuarios(Principal p) {
    	UsuarioEntity usuario = usuarioDao.obtenerXusuarioYEstado(p.getName(), 1);
    	UsuarioRolesEntity agente_admin = userRolDao.obtenerXUsuarioRol(usuario.getUsuarioId(), UsuarioRolesEntity.CONSULTOR);
    	if(agente_admin == null) {
    		return "admin/GestionUsuarios/Usuarios";
    	}else {
    		return "admin/Error/404";
    	}
    }
    
    @RequestMapping("/admin/servicios")
    public String servicios(Principal p) {
    	UsuarioEntity usuario = usuarioDao.obtenerXusuarioYEstado(p.getName(), 1);
    	UsuarioRolesEntity agente_admin = userRolDao.obtenerXUsuarioRol(usuario.getUsuarioId(), UsuarioRolesEntity.CONSULTOR);
    	if(agente_admin == null) {
    		return "admin/GestionServicios/Servicios";
    	}else {
    		return "admin/Error/404";
    	}	
    }
    
    @RequestMapping("/admin/rol")
    public String roles(Principal p) {
    	UsuarioEntity usuario = usuarioDao.obtenerXusuarioYEstado(p.getName(), 1);
    	UsuarioRolesEntity agente_admin = userRolDao.obtenerXUsuarioRol(usuario.getUsuarioId(), UsuarioRolesEntity.CONSULTOR);
    	if(agente_admin == null) {
    		return "admin/GestionRoles/NewRol";
    	}else {
    		return "admin/Error/404";
    	}	
    }
    
    @RequestMapping("/admin/usuarioRol")
    public String asignarRoles(Principal p) {
    	UsuarioEntity usuario = usuarioDao.obtenerXusuarioYEstado(p.getName(), 1);
    	UsuarioRolesEntity agente_admin = userRolDao.obtenerXUsuarioRol(usuario.getUsuarioId(), UsuarioRolesEntity.CONSULTOR);
    	if(agente_admin == null) {
    		return "admin/GestionUserRol/UserRol";
    	}else {
    		return "admin/Error/404";
    	}
    }
}
