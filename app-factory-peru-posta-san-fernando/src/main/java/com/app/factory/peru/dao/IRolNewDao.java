package com.app.factory.peru.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.app.factory.peru.rol.entity.RolEntity;

public interface IRolNewDao extends JpaRepository<RolEntity, Long>{
	
	List<RolEntity> findByestado(int estado);
}
