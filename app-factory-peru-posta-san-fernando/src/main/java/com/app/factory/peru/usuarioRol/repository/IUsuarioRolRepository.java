package com.app.factory.peru.usuarioRol.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;

import com.app.factory.peru.generic.repository.IGenericRepo;
import com.app.factory.peru.usuarioRol.entity.UsuarioRolesEntity;

public interface IUsuarioRolRepository extends IGenericRepo<UsuarioRolesEntity, Integer>{

	List<UsuarioRolesEntity> findByestado(int estado);
	UsuarioRolesEntity findByusuario(int user);
	@Query("FROM UsuarioRolesEntity  WHERE usuarioId = ?1 and rolId = ?2 and estado = ?3")
	UsuarioRolesEntity obtenerXUsuarioRolYEstado(Long user, int rol, int estado); 
	
	@Query(value = "SELECT sum(if(ROLID = 1,1,0)), sum(if(ROLID = 3,1,0)) FROM USUARIOSROLES WHERE ESTADO =1", nativeQuery = true)
	Object[][] cantUsuariosXRol();
	
	@Query("FROM UsuarioRolesEntity  WHERE usuarioId = ?1 and rolId = ?2")
	UsuarioRolesEntity obtenerXUsuarioRol(Long user, int rol);
	
}
