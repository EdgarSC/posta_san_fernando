package com.app.factory.peru.controller;

import java.security.Principal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.app.factory.peru.dao.IServicioDao;
import com.app.factory.peru.servicios.entity.ServicioEntity;

@RestController
public class ServiciosController {
	
	private static final Logger LOGGER = LogManager.getLogger(ServiciosController.class);
	
	@Autowired
	private IServicioDao servicioDao;
	
	@GetMapping("servicios/listar_servicios")
	@ResponseBody
	public ResponseEntity<?> listarServicios(Principal p){
		if(p == null) return null;
		try {			
			
			List<ServicioEntity> servicios = servicioDao.findAll();	
//			servicios.forEach(x -> {System.out.println(x);});
//			servicios.forEach(System.out::println); //toString
			if(servicios.size() > 0) {
				return new ResponseEntity<List<ServicioEntity>>(servicios,HttpStatus.OK);
			}else {
				return new ResponseEntity<String>("No se encontraron resultados.",HttpStatus.BAD_REQUEST);
			}
			
		} catch (Exception e) {
			LOGGER.error("Error getServiciosList: detalle: " + e.getMessage());
			return new ResponseEntity<String>("Se tuvo un error al obtener la lista de Servicios, favor de contactar al administrador.",HttpStatus.BAD_REQUEST);
		}
	}
	
	@GetMapping("servicios/listar_servicios_activos")
	@ResponseBody
	public ResponseEntity<?> listarServiciosActivos(Principal p){
		if(p == null) return null;
		try {			
			
			List<ServicioEntity> servicios = servicioDao.findByestado(1);
			if(servicios.size() > 0) {
				return new ResponseEntity<List<ServicioEntity>>(servicios,HttpStatus.OK);
			}else {
				return new ResponseEntity<String>("No se encontraron resultados.",HttpStatus.BAD_REQUEST);
			}
			
		} catch (Exception e) {
			LOGGER.error("Error getServiciosList: detalle: " + e.getMessage());
			return new ResponseEntity<String>("Se tuvo un error al obtener la lista de Servicios, favor de contactar al administrador.",HttpStatus.BAD_REQUEST);
		}
	}
	
	@PostMapping("servicios/guardar_servicios")
	@ResponseBody
	public ResponseEntity<?> guardarServicios(Principal p, String nombre, String descripcion){
		if(p == null) return null;
		ServicioEntity service = null;
		try {			
			ServicioEntity save_servicio = new ServicioEntity();
			save_servicio.setNombre(nombre);
			save_servicio.setDescripcion(descripcion);
			save_servicio.setEstado(1);			
			save_servicio.setCreator(p.getName());
			save_servicio.setCreated(LocalDateTime.now());
			
			service = servicioDao.save(save_servicio);
			
			if(service != null) {
				return new ResponseEntity<String>("Listo",HttpStatus.OK);
			}else {
				return new ResponseEntity<String>("No se encontraron resultados.",HttpStatus.BAD_REQUEST);
			}
			
		} catch (Exception e) {
			LOGGER.error("Error guardarServicio: detalle: " + e.getMessage());
			return new ResponseEntity<String>("Se tuvo un error al guarda el Servicio, favor de contactar al administrador.",HttpStatus.BAD_REQUEST);			
		}
	}
	
	@PostMapping("servicios/cambiar_estado_servicio")
	@ResponseBody
	public ResponseEntity<?> cambiarEstadoServicio(Principal p, int id_servicio, boolean estado){
		if(p == null) return null;
		ServicioEntity service = null;
		try {			
			Optional<ServicioEntity> servicio = servicioDao.findById((long) id_servicio);
			ServicioEntity update_service = servicio.get();
			if(estado) {
				update_service.setEstado(0);	
			}else {
				update_service.setEstado(1);
			}
			service = servicioDao.save(update_service);
			
			if(service != null) {
				return new ResponseEntity<String>("Listo",HttpStatus.OK);
			}else {
				return new ResponseEntity<String>("No se encontraron resultados.",HttpStatus.BAD_REQUEST);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("Error actualizarServicio: detalle: " + e.getMessage());
			return new ResponseEntity<String>("Se tuvo un error al actualizar el Servicio, favor de contactar al administrador.",HttpStatus.BAD_REQUEST);
		}
	}
	
	@PostMapping("servicios/actualizar_servicios")
	@ResponseBody
	public ResponseEntity<?> actualizarServicios(Principal p, int id_servicio, String new_nombre, String new_descripcion){
		if(p == null) return null;
		ServicioEntity service = null;
		try {			
			Optional<ServicioEntity> servicio = servicioDao.findById((long) id_servicio);
			ServicioEntity update_service = servicio.get();
			update_service.setNombre(new_nombre);
			update_service.setDescripcion(new_descripcion);
			update_service.setChanger(p.getName());
			update_service.setChanged(LocalDateTime.now());
			service = servicioDao.save(update_service);
			
			if(service != null) {
				return new ResponseEntity<String>("Listo",HttpStatus.OK);
			}else {
				return new ResponseEntity<String>("No se encontraron resultados.",HttpStatus.BAD_REQUEST);
			}
			
		} catch (Exception e) {
			LOGGER.error("Error actualizarServicio: detalle: " + e.getMessage());
			return new ResponseEntity<String>("Se tuvo un error al actualizar el Servicio, favor de contactar al administrador.",HttpStatus.BAD_REQUEST);
		}
	}
	
}
