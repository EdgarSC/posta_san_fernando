package com.app.factory.peru.servicios.repository;

import java.util.List;

import com.app.factory.peru.generic.repository.IGenericRepo;
import com.app.factory.peru.servicios.entity.ServicioEntity;

public interface IServiciosRepository extends IGenericRepo<ServicioEntity, Integer>{
	List<ServicioEntity> findByestado(int estado);

}
