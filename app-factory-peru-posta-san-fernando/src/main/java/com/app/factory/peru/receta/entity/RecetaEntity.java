package com.app.factory.peru.receta.entity;

import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.app.factory.peru.consultas.entity.ConsultaEntity;

@Entity
@Table(name = "RECETA")
public class RecetaEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="RECETAID")
	private Long id_receta;
	
	@OneToOne
	@JoinColumn(name = "CONSULTAID")
	private ConsultaEntity consulta;
	
	@Column(name="MEDICAMENTO")
	private String medicamento;
	
	@Column(name="CONCENTRACION")
	private String concentracion;
	
	@Column(name="FF")
	private String ff;
	
	@Column(name="CANTIDAD")
	private String cantidad;
	
	@Column(name="DOSIS")
	private String dosis;
	
	@Column(name="VIA")
	private String via;
	
	@Column(name="FRECUENCIA")
	private String frecuencia;
	
	@Column(name="DURACION")
	private String duracion;
	
	@Column(name="FECHA_EXPIRACION")
	private LocalDate fecha_expiracion;
	
	@Column(name="FECHA_REGISTRO")
	private LocalDateTime fecha_registro;

	public Long getId_receta() {
		return id_receta;
	}

	public void setId_receta(Long id_receta) {
		this.id_receta = id_receta;
	}

	public ConsultaEntity getConsulta() {
		return consulta;
	}

	public void setConsulta(ConsultaEntity consulta) {
		this.consulta = consulta;
	}

	public String getMedicamento() {
		return medicamento;
	}

	public void setMedicamento(String medicamento) {
		this.medicamento = medicamento;
	}

	public String getConcentracion() {
		return concentracion;
	}

	public void setConcentracion(String concentracion) {
		this.concentracion = concentracion;
	}

	public String getFf() {
		return ff;
	}

	public void setFf(String ff) {
		this.ff = ff;
	}

	public String getCantidad() {
		return cantidad;
	}

	public void setCantidad(String cantidad) {
		this.cantidad = cantidad;
	}

	public LocalDateTime getFecha_registro() {
		return fecha_registro;
	}

	public void setFecha_registro(LocalDateTime fecha_registro) {
		this.fecha_registro = fecha_registro;
	}

	public String getDosis() {
		return dosis;
	}

	public void setDosis(String dosis) {
		this.dosis = dosis;
	}

	public String getVia() {
		return via;
	}

	public void setVia(String via) {
		this.via = via;
	}

	public String getFrecuencia() {
		return frecuencia;
	}

	public void setFrecuencia(String frecuencia) {
		this.frecuencia = frecuencia;
	}

	public String getDuracion() {
		return duracion;
	}

	public void setDuracion(String duracion) {
		this.duracion = duracion;
	}

	public LocalDate getFecha_expiracion() {
		return fecha_expiracion;
	}

	public void setFecha_expiracion(LocalDate fecha_expiracion) {
		this.fecha_expiracion = fecha_expiracion;
	}
	
}