package com.app.factory.peru.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.app.factory.peru.cita.entity.CitaEntity;


public interface ICitaDao extends JpaRepository<CitaEntity, Long> {
	@Query("FROM CitaEntity WHERE estado != 0 and atendido = ?1")
	List<CitaEntity> listarCitasXAtencion(int atendido);
	
	@Query("SELECT COUNT(*) FROM CitaEntity WHERE atendido = 1")
	long contarCitasAtendidas();
	
	@Query("FROM CitaEntity WHERE paciente.numero_documento = :numero_documento and estado != 0")
	List<CitaEntity> listarPorNumeroDNI(String numero_documento); 
	
}
