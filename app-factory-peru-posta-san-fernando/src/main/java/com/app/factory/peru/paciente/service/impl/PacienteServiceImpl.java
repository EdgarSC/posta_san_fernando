package com.app.factory.peru.paciente.service.impl;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.factory.peru.generic.repository.IGenericRepo;
import com.app.factory.peru.generic.service.impl.CRUDImpl;
import com.app.factory.peru.paciente.entity.PacienteEntity;
import com.app.factory.peru.paciente.repository.IPacienteRepository;
import com.app.factory.peru.paciente.service.IPacienteService;
import com.app.factory.peru.region.entity.RegionEntity;
import com.app.factory.peru.region.repository.IRegionRepository;

@Service
public class PacienteServiceImpl extends CRUDImpl<PacienteEntity, Integer> implements IPacienteService{

	@Autowired
	private IPacienteRepository repo;
	@Autowired
	private IRegionRepository repoRegion;

	@Override
	protected IGenericRepo<PacienteEntity, Integer> getRepo() {
		return repo;
	}

	@Override
	public List<PacienteEntity> listarPacientes() throws Exception {
		List<PacienteEntity> pacientes = this.listar();		
		List<RegionEntity> regiones = repoRegion.listarProvinciasXDistritos(pacientes.stream().map(paciente -> paciente.getRegion().getRegionId()).collect(Collectors.toList()));
		Map<Integer, RegionEntity> id_regiones = regiones.stream().collect(Collectors.toMap(RegionEntity::getRegionNumero, region -> region));
		pacientes.forEach(paciente -> paciente.setProvincia(id_regiones.get(Integer.parseInt(paciente.getRegion().getProvincia()))));
		return pacientes;
	}
	
}
