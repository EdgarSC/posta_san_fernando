package com.app.factory.peru.rol.repository;

import java.util.List;

import com.app.factory.peru.generic.repository.IGenericRepo;
import com.app.factory.peru.rol.entity.RolEntity;

public interface IRolRepository  extends IGenericRepo<RolEntity, Integer> {
	List<RolEntity> findByestado(int estado);
}
