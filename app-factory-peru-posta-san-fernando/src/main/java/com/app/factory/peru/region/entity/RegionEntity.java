package com.app.factory.peru.region.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "REGION")
public class RegionEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "REGIONID")
	private long regionId;
	
	@Column(name = "DEPARTAMENTO")
	private String departamento;
	
	@Column(name = "PROVINCIA")
	private String provincia;
	
	@Column(name = "REGIONUMERO")
	private int regionNumero;
	
	
	@Column(name = "NOMBRE")
	private String nombre;
	
	public RegionEntity() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public RegionEntity(int regionId) {
		this.regionId = regionId;
	}


	public RegionEntity(long regionId, int regionNumero, String departamento, String provincia, String nombre) {
		super();
		this.regionId = regionId;
		this.regionNumero = regionNumero;
		this.departamento = departamento;
		this.provincia = provincia;
		this.nombre = nombre;
	}
	
	

	public long getRegionId() {
		return regionId;
	}


	public void setRegionId(long regionId) {
		this.regionId = regionId;
	}


	public int getRegionNumero() {
		return regionNumero;
	}


	public void setRegionNumero(int regionNumero) {
		this.regionNumero = regionNumero;
	}

	public String getDepartamento() {
		return departamento;
	}


	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}


	public String getProvincia() {
		return provincia;
	}


	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	

	@Override
	public String toString() {
		return "RegionEntity [regionId=" + regionId + ", regionNumero=" + regionNumero + ", departamento="
				+ departamento + ", provincia=" + provincia + ", nombre=" + nombre + "]";
	}



	private static final long serialVersionUID = 1952044519636141355L;

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
