package com.app.factory.peru.controller;

import java.security.Principal;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.app.factory.peru.dao.IRolNewDao;
import com.app.factory.peru.dao.IUsuarioNewDao;
import com.app.factory.peru.dao.IUsuariosRolesDao;
import com.app.factory.peru.rol.entity.RolEntity;
import com.app.factory.peru.usuario.entity.UsuarioEntity;
import com.app.factory.peru.usuarioRol.entity.UsuarioRolesEntity;

@RestController
public class UsuarioRolController {
	
	private static final Logger LOGGER = LogManager.getLogger(UsuarioRolController.class);
	
	@Autowired
	private IUsuariosRolesDao userRolDao;
	
	@Autowired
	private IRolNewDao rolDao;
	
	@Autowired
	private IUsuarioNewDao usuarioDao;
	
	@GetMapping("rolUser/listar_userRol")
	@ResponseBody
	public ResponseEntity<?> listarUserRol(Principal p){
		if(p == null) return null;
		try {			
			
			List<UsuarioRolesEntity> userRol = userRolDao.findAll();	
			if(userRol.size() > 0) {
				return new ResponseEntity<List<UsuarioRolesEntity>>(userRol,HttpStatus.OK);
			}else {
				return new ResponseEntity<String>("No se encontraron resultados.",HttpStatus.BAD_REQUEST);
			}
			
		} catch (Exception e) {
			LOGGER.error("Error getUserRolesList: detalle: " + e.getMessage());
			return new ResponseEntity<String>("Se tuvo un error al obtener la lista de User Roles, favor de contactar al administrador.",HttpStatus.BAD_REQUEST);
		}
	}
	
	@GetMapping("rolUser/listar_userRol_activos")
	@ResponseBody
	public ResponseEntity<?> listarServiciosActivos(Principal p){
		if(p == null) return null;
		try {			
			
			List<UsuarioRolesEntity> userRol = userRolDao.findByestado(1);
			if(userRol.size() > 0) {
				return new ResponseEntity<List<UsuarioRolesEntity>>(userRol,HttpStatus.OK);
			}else {
				return new ResponseEntity<String>("No se encontraron resultados.",HttpStatus.BAD_REQUEST);
			}
			
		} catch (Exception e) {
			LOGGER.error("Error getUserRolesList: detalle: " + e.getMessage());
			return new ResponseEntity<String>("Se tuvo un error al obtener lista de User Roles, favor de contactar al administrador.",HttpStatus.BAD_REQUEST);
		}
	}
	
	@PostMapping("rolUser/guardar_rolUser")
	@ResponseBody
	public ResponseEntity<?> guardarServicios(Principal p, int id_user, int id_rol){
		if(p == null) return null;
		UsuarioRolesEntity userRol = null;
		try {						
			UsuarioRolesEntity save_userRol = new UsuarioRolesEntity();
			RolEntity rol = rolDao.getOne((long) id_rol);
			UsuarioEntity usuario  = usuarioDao.getOne((long) id_user);
			
			save_userRol.setRol(rol);
			save_userRol.setUsuario(usuario);
			save_userRol.setEstado(1);
			
			userRol = userRolDao.save(save_userRol);
			
			if(userRol != null) {
				return new ResponseEntity<String>("Listo",HttpStatus.OK);
			}else {
				return new ResponseEntity<String>("No se encontraron resultados.",HttpStatus.BAD_REQUEST);
			}
			
		} catch (Exception e) {
			LOGGER.error("Error guardarUserRol: detalle: " + e.getMessage());
			return new ResponseEntity<String>("Se tuvo un error al guarda el UserRol, favor de contactar al administrador.",HttpStatus.BAD_REQUEST);			
		}
	}
	
	@PostMapping("rolUser/cambiar_estado_rolUser")
	@ResponseBody
	public ResponseEntity<?> cambiarEstadoRolUser(Principal p, int id_userRol, boolean estado){
		if(p == null) return null;
		UsuarioRolesEntity usuarioRol = null;
		try {			
			Optional<UsuarioRolesEntity> userRol = userRolDao.findById((long) id_userRol);
			UsuarioRolesEntity update_userRol = userRol.get();
			if(estado) {
				update_userRol.setEstado(0);	
			}else {
				update_userRol.setEstado(1);
			}
			usuarioRol = userRolDao.save(update_userRol);
			
			if(usuarioRol != null) {
				return new ResponseEntity<String>("Listo",HttpStatus.OK);
			}else {
				return new ResponseEntity<String>("No se encontraron resultados.",HttpStatus.BAD_REQUEST);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("Error actualizarRolUser: detalle: " + e.getMessage());
			return new ResponseEntity<String>("Se tuvo un error al actualizar el RolUser, favor de contactar al administrador.",HttpStatus.BAD_REQUEST);
		}
	}
	
	@PostMapping("rolUser/actualizar_rolUser")
	@ResponseBody
	public ResponseEntity<?> actualizarRolUser(Principal p, int id_userRol, int id_user, int id_rol){
		if(p == null) return null;
		UsuarioRolesEntity usuarioRol = null;
		try {			
			Optional<UsuarioRolesEntity> userRol = userRolDao.findById((long) id_userRol);
			UsuarioRolesEntity update_userRol = userRol.get();
			RolEntity rol = rolDao.getOne((long) id_rol);
			UsuarioEntity usuario  = usuarioDao.getOne((long) id_user);
			update_userRol.setRol(rol);
			update_userRol.setUsuario(usuario);
			
			usuarioRol = userRolDao.save(update_userRol);
			
			if(usuarioRol != null) {
				return new ResponseEntity<String>("Listo",HttpStatus.OK);
			}else {
				return new ResponseEntity<String>("No se encontraron resultados.",HttpStatus.BAD_REQUEST);
			}
			
		} catch (Exception e) {
			LOGGER.error("Error actualizarRolUser: detalle: " + e.getMessage());
			return new ResponseEntity<String>("Se tuvo un error al actualizar RolUser, favor de contactar al administrador.",HttpStatus.BAD_REQUEST);
		}
	}
	
	@GetMapping("/obtener_rol_agente_logueado")
	@ResponseBody
	public Map<String, Object> obtenerRolAgenteLogueado(Principal p){
		if(p == null) return null;
		Map<String, Object> resultado = new HashMap<>();
		UsuarioEntity usuario = usuarioDao.obtenerXusuarioYEstado(p.getName(), 1);
		UsuarioRolesEntity agente_admin = userRolDao.obtenerXUsuarioRolYEstado(usuario.getUsuarioId(), UsuarioRolesEntity.ADMINISTRADOR, 1);
		UsuarioRolesEntity agente_super = userRolDao.obtenerXUsuarioRolYEstado(usuario.getUsuarioId(), UsuarioRolesEntity.SUPERVISOR, 1);
		UsuarioRolesEntity agente_col = userRolDao.obtenerXUsuarioRolYEstado(usuario.getUsuarioId(), UsuarioRolesEntity.CONSULTOR, 1);
		resultado.put("ROL_AGENTE_ADMIN", agente_admin != null ? agente_admin.getRol().getRolId() : -1);
		resultado.put("ROL_AGENTE_SUPER", agente_super != null ? agente_super.getRol().getRolId() : -1);
		resultado.put("ROL_AGENTE_CON", agente_col != null ? agente_col.getRol().getRolId() : -1);
		resultado.put("ROL_PERMITIDO", Arrays.asList(UsuarioRolesEntity.ADMINISTRADOR, UsuarioRolesEntity.SUPERVISOR, UsuarioRolesEntity.CONSULTOR));
		return resultado;
	}
}
