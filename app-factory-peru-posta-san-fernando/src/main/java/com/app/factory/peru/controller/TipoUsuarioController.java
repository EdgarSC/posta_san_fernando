package com.app.factory.peru.controller;

import java.security.Principal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.app.factory.peru.dao.ITipoUsuarioNewDao;
import com.app.factory.peru.tipoUsuario.entity.TipoUsuarioEntity;

@Controller
public class TipoUsuarioController {

	private static final Logger LOGGER = LogManager.getLogger(TipoUsuarioController.class);
	
	@Autowired
	private ITipoUsuarioNewDao tipoUsuarioDao;
	
	@GetMapping("tipoUsuario/listar_tipoUsuarios")
	@ResponseBody
	public ResponseEntity<?> listarTiposUsuarios(Principal p){
		if(p == null) return null;
		try {			
			
			List<TipoUsuarioEntity> tipoUsuarios = tipoUsuarioDao.findAll();
			if(tipoUsuarios.size() > 0) {
				return new ResponseEntity<List<TipoUsuarioEntity>>(tipoUsuarios,HttpStatus.OK);
			}else {
				return new ResponseEntity<String>("No se encontraron resultados.",HttpStatus.BAD_REQUEST);
			}
			
		} catch (Exception e) {
			LOGGER.error("Error getTipoUsuariosList: detalle: " + e.getMessage());
			return new ResponseEntity<String>("Se tuvo un error al obtener la lista de Tipo de  Usuarios, favor de contactar al administrador.",HttpStatus.BAD_REQUEST);
		}
	}
	
	@GetMapping("/tipoUsuario/listar_tipoUsuarios_activos")
	@ResponseBody
	public ResponseEntity<?> listarTiposUsuariosActivos(Principal p){
		if(p == null) return null;
		try {			
			
			List<TipoUsuarioEntity> tipoUsuarios = tipoUsuarioDao.findByestado(1);
			if(tipoUsuarios.size() > 0) {
				return new ResponseEntity<List<TipoUsuarioEntity>>(tipoUsuarios,HttpStatus.OK);
			}else {
				return new ResponseEntity<String>("No se encontraron resultados.",HttpStatus.BAD_REQUEST);
			}
			
		} catch (Exception e) {
			LOGGER.error("Error getTipoUsuariosList: detalle: " + e.getMessage());
			return new ResponseEntity<String>("Se tuvo un error al obtener la lista de Tipo de  Usuarios, favor de contactar al administrador.",HttpStatus.BAD_REQUEST);
		}
	}
	
	@PostMapping("tipoUsuario/guardar_tipoUsuarios")
	public ResponseEntity<?> guardarTipoUsuarios(Principal p, String nombre, String descripcion){
		if(p == null) return null;
		TipoUsuarioEntity tipoUsuarios = null;
		try {			
			TipoUsuarioEntity save_tipoUsuario = new TipoUsuarioEntity();
			save_tipoUsuario.setNombre(nombre);
			save_tipoUsuario.setDescripcion(descripcion);
			save_tipoUsuario.setEstado(1);			
			save_tipoUsuario.setCreator(p.getName());
			save_tipoUsuario.setCreated(LocalDateTime.now());
			save_tipoUsuario.setChanged(LocalDateTime.now());
			
			tipoUsuarios = tipoUsuarioDao.save(save_tipoUsuario);
			
			if(tipoUsuarios != null) {
				return new ResponseEntity<String>("Listo",HttpStatus.OK);
			}else {
				return new ResponseEntity<String>("No se encontraron resultados.",HttpStatus.BAD_REQUEST);
			}
			
		} catch (Exception e) {
			LOGGER.error("Error guardarTipoUsuario: detalle: " + e.getMessage());
			return new ResponseEntity<String>("Se tuvo un error al guarda el Tipo de Usuario, favor de contactar al administrador.",HttpStatus.BAD_REQUEST);			
		}
	}
	
	@PostMapping("tipoUsuario/actualizar_tipoUsuarios")
	@ResponseBody
	public ResponseEntity<?> actualizarTipoUsuario(Principal p, int id_tipoUser, String new_nombre, String new_descripcion){
		if(p == null) return null;
		TipoUsuarioEntity tipoUsuarios = null;
		try {			
			Optional<TipoUsuarioEntity> _tipoUsuario = tipoUsuarioDao.findById((long) id_tipoUser);
			TipoUsuarioEntity update_tipoUsuario = _tipoUsuario.get();
			update_tipoUsuario.setNombre(new_nombre);
			update_tipoUsuario.setDescripcion(new_descripcion);
			update_tipoUsuario.setChanger(p.getName());
			update_tipoUsuario.setChanged(LocalDateTime.now());
			tipoUsuarios = tipoUsuarioDao.save(update_tipoUsuario);
			
			if(tipoUsuarios != null) {
				return new ResponseEntity<String>("Listo",HttpStatus.OK);
			}else {
				return new ResponseEntity<String>("No se encontraron resultados.",HttpStatus.BAD_REQUEST);
			}
			
		} catch (Exception e) {
			LOGGER.error("Error actualizar tipoUsuario: detalle: " + e.getMessage());
			return new ResponseEntity<String>("Se tuvo un error al actualizar el Tipo de Usuario, favor de contactar al administrador.",HttpStatus.BAD_REQUEST);
		}
	}
	
	@PostMapping("tipoUsuario/cambiar_estado_tipoUser")
	@ResponseBody
	public ResponseEntity<?> cambiarEstadoTipoUser(Principal p, int id_tipoUser, boolean estado){
		if(p == null) return null;
		TipoUsuarioEntity tipoUsuarios = null;
		try {			
			Optional<TipoUsuarioEntity> _tipoUsuario = tipoUsuarioDao.findById((long) id_tipoUser);
			TipoUsuarioEntity update_tipoUsuario = _tipoUsuario.get();
			if(estado) {
				update_tipoUsuario.setEstado(0);	
			}else {
				update_tipoUsuario.setEstado(1);
			}
			tipoUsuarios = tipoUsuarioDao.save(update_tipoUsuario);
			
			if(tipoUsuarios != null) {
				return new ResponseEntity<String>("Listo",HttpStatus.OK);
			}else {
				return new ResponseEntity<String>("No se encontraron resultados.",HttpStatus.BAD_REQUEST);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("Error actualizar estado tipo user: detalle: " + e.getMessage());
			return new ResponseEntity<String>("Se tuvo un error al actualizar el estado del Tipo de Usuario, favor de contactar al administrador.",HttpStatus.BAD_REQUEST);
		}
	}
	
}
