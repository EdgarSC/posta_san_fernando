package com.app.factory.peru.service.ServiceImpl;

import java.util.HashSet;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.factory.peru.dao.IUsuarioNewDao;
import com.app.factory.peru.rol.entity.RolEntity;
import com.app.factory.peru.usuario.entity.UsuarioEntity;
@Service
@Transactional
public class UserDetailsServiceImpl implements UserDetailsService {
	
	@Autowired
	IUsuarioNewDao daoUsuario;
	
	private static final Logger LOGGER = LogManager.getLogger(UserDetailsServiceImpl.class);

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		LOGGER.warn("user name " + username);
		UsuarioEntity usuarioEntity = daoUsuario.findByUsuarioAndEstado(username, 1)
				.orElseThrow(() -> new UsernameNotFoundException("No existe usuario."));
		Set<GrantedAuthority> roles = new HashSet<>();
		for (RolEntity role : usuarioEntity.getRoles()) {
			GrantedAuthority grantedAuthority = new SimpleGrantedAuthority(role.getName());
			roles.add(grantedAuthority);
		}

		LOGGER.warn("user entity " + usuarioEntity);
		UserDetails user = (UserDetails) new User(usuarioEntity.getUsuario(), usuarioEntity.getContrasena(),
				roles);
		LOGGER.warn("user " + user);
		return user;
	}

}
