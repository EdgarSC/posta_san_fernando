var table_servicios;
var actualizar_id_ser = -1;
function init() {
    listarServicios();
}

function listarServicios() {
    table_servicios = $("#table_servicios").DataTable({
        responsive: true,
        "ajax": {
            "url": ip + "servicios/listar_servicios",
            "dataSrc": ""
        },
        "columns": [
            { data: "nombre" },
            { data: "descripcion" },
            { data: "created" },
            {
                data: null,
                className: "dt-center",
                render: function(data) {
                    var id_servicio = data.servicioId;
                    var estado_servicio = data.estado;
                    var nom_servicio = data.nombre;
                    if (estado_servicio == 1) {
                        return '<a href="#" onclick="cambiar_estado_servicio(`'+id_servicio+'`,`'+nom_servicio+'`,`'+estado_servicio+'`)"><i class="fa fa-toggle-on green font-md"></i></a>';
                    } else {
                        return '<a href="#" onclick="cambiar_estado_servicio(`'+id_servicio+'`,`'+nom_servicio+'`,`'+estado_servicio+'`)"><i class="fa fa-toggle-off red font-md"></i></a>';
                    }
                }
            },
            { data: null,
              className: "dt-center",
              render: function(data){
              	var id_servicio = data.servicioId;
              	var nom_servicio = data.nombre;
              	var desc_servicio = data.descripcion;
              	return '<button type="button" class="btn btn-sm" onclick="ver_actualizar_servicios(`'+id_servicio+'`,`'+nom_servicio+'`,`'+desc_servicio+'`)"><i class="fa fa-edit style="font-size:80px" "></i></button>';
              }
            }
        ],
        language
    });
}

function cambiar_estado_servicio(id_servicio, nombre_servicio, estado_servicio) {
    var objRequest = {
        id_servicio: id_servicio,
        estado: estado_servicio
    };
    posta.post(
        'servicios/cambiar_estado_servicio',
        objRequest,
        function(response) {
            if (response == "Listo") {
                table_servicios.ajax.reload();
                var estado = estado_servicio == 1 ? "deshabilitó " : "habilitó ";
                mostrarMensajeTemporal("contenedor_mensaje_servicios", "verde", "Se "+estado+ nombre_servicio+" con éxito!", 4);
            } else {
               mostrarMensajeTemporal("contenedor_mensaje_servicios", "rojo", "Ocurrió un error, contacte con el administrador.", 6);
            }
        }
    );
}

function agregarServicio() {
    if ($("#nombre_servicio").val() == "") {
        mostrarMensajeTemporal("contenedor_mensaje_add_servicio", "amarillo", "Ingrese nombre del servicio.", 2);
        return;
    }
    posta.post(
        'servicios/guardar_servicios',
        data= {
	        nombre: $("#nombre_servicio").val(),
	        descripcion: $("#descripcion_servicio").val()
	    },
        function(response) {
        	if(response == "Listo"){               		 	
	            table_servicios.ajax.reload();
	            $("#nombre_servicio").val("");
	            $("#descripcion_servicio").val("");
	            mostrarMensajeTemporal("contenedor_mensaje_servicios", "verde", "Se registró con éxito!", 4);
	        }else{
	     		mostrarMensajeTemporal("contenedor_mensaje_servicios", "rojo", "Ocurrió un error, contacte con el administrador.", 6);	
	        }
        }
    );
}

function ver_actualizar_servicios(id, nombre, descripcion){
	$("#mdl_actualizar_servicio").modal('show');
	$("#update_nombreServicio").val(nombre);
	$("#update_descripcionServicio").val(descripcion);
	actualizar_id_ser = id;
}

function actualizar_servicio(){
	posta.post(
        'servicios/actualizar_servicios',
        data = {
        	id_servicio: actualizar_id_ser,
	        new_nombre: $("#update_nombreServicio").val(),
	        new_descripcion: $("#update_descripcionServicio").val()
        },
        function(response) {
        	if(response == "Listo"){        
        		table_servicios.ajax.reload();
        		$("#mdl_actualizar_servicio").modal('hide');
	            mostrarMensajeTemporal("contenedor_mensaje_servicios", "verde", "Se actualizó con éxito!", 4);
	        }else{
	     		mostrarMensajeTemporal("contenedor_mensaje_servicios", "rojo", "Ocurrió un error, contacte con el administrador.", 6);	
	        }
        }
    );
}