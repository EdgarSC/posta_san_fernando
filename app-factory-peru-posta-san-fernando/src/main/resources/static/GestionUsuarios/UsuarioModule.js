var table_usuarios;
var actualizar_id_user = -1;
function init() {
    listarUsuarios();
    listar_servicios("#nw_servicio");
    listar_servicios("#upd_servicio");
    listar_tipo_usuario("#nw_tipo_usuario");
    listar_tipo_usuario("#upd_tipo_usuario");
}

function listarUsuarios() {
    table_usuarios = $("#table_usuarios").DataTable({
        responsive: true,
        "ajax": {
            "url": ip + "usuario/listar_usuarios",
            "dataSrc": ""
        },
        "columns": [
            { data: "usuario" },
            { data: "nombre" },
            { 
	 			data: null,
                className: "dt-center",
                render: function(data) {
                	return data.apellidoPaterno+" "+data.apellidoMaterno;
                }
			},
            { data: "numeroDocumento" },
            { data: "servicio.nombre" },
            { data: "tipousuario.nombre" },
            { data: "correo" },
            { data: "celular" },
            {
                data: null,
                className: "dt-center",
                render: function(data) {
                    var id_usuario = data.usuarioId;
                    var estado_usuario = data.estado;
                    var nom_usuario = data.usuario;
                    if (estado_usuario == 1) {
                        return '<button type="button" class="btn btn-sm" onclick="cambiar_estado_usuario(`'+id_usuario+'`,`'+nom_usuario+'`,`'+estado_usuario+'`)"><i class="fa fa-toggle-on green font-md"></i></button>';
                    } else {
                        return '<button type="button" class="btn btn-sm" onclick="cambiar_estado_usuario(`'+id_usuario+'`,`'+nom_usuario+'`,`'+estado_usuario+'`)"><i class="fa fa-toggle-off red font-md"></i></button>';
                    }
                }
            },
            { data: null,
              className: "dt-center",
              render: function(data){
              	var id_usuario = data.usuarioId;
              	var usuario = data.usuario;
              	var nombres = data.nombre;
              	var a_paterno = data.apellidoPaterno;
              	var a_materno = data.apellidoMaterno;
				var dni = data.numeroDocumento;
				var f_nacimiento = data.fechaNacimiento;
				var telefono = data.telefono;
				var celular = data.celular;
				var email = data.correo;
				var tipoUsuario = data.tipousuario.tipoUsuarioId;
				var servicio = data.servicio.servicioId;
              	return '<button type="button" class="btn btn-sm" onclick="ver_actualizar_usuarios(`'+id_usuario+'`,`'+usuario+'`,`'+nombres+'`,`'+a_paterno+'`,`'+a_materno+'`,`'+dni+'`,`'+f_nacimiento+'`,`'+telefono+'`,`'+celular+'`,`'+email+'`,`'+tipoUsuario+'`,`'+servicio+'`)"><i class="fa fa-edit style="font-size:80px" "></i></button>';
              }
            }
        ],
        language
    });
}

function cambiar_estado_usuario(id_usuario, nom_usuario, estado_usuario) {
    var objRequest = {
        id_usuario: id_usuario,
        estado: estado_usuario
    };
    posta.post(
        'usuario/cambiar_estado_usuario',
        objRequest,
        function(response) {
            if (response == "Listo") {
                table_usuarios.ajax.reload();
                var estado = estado_usuario == 1 ? "deshabilitó " : "habilitó ";
                mostrarMensajeTemporal("contenedor_mensaje_usuarios", "verde", "Se "+estado + nom_usuario+" con éxito!", 4);
            } else {
               mostrarMensajeTemporal("contenedor_mensaje_usuarios", "rojo", "Ocurrió un error, contacte con el administrador.", 6);
            }
        }
    );
}

function listar_servicios(_servicio) {
    posta.get(
        'servicios/listar_servicios_activos',
        objRequest = {},
        function(response) {
            if (response.length >0) {
               for(var i=0; i<response.length;i++ ){
               		$(_servicio).append('<option value="'+response[i].servicioId+'">'+response[i].nombre+'</option>');
               }
            } else {
               mostrarMensajeTemporal("contenedor_mensaje_usuarios", "rojo", "Ocurrió un error, contacte con el administrador.", 6);
            }
        }
    );
}

function listar_tipo_usuario(_tipoUsuario) {
    posta.get(
        'tipoUsuario/listar_tipoUsuarios_activos',
        objRequest = {},
        function(response) {
            if (response.length >0) {
               for(var i=0; i<response.length;i++ ){
               		$(_tipoUsuario).append('<option value="'+response[i].tipoUsuarioId+'">'+response[i].nombre+'</option>');
               }
            } else {
               mostrarMensajeTemporal("contenedor_mensaje_usuarios", "rojo", "Ocurrió un error, contacte con el administrador.", 6);
            }
        }
    );
}
		
function agregar_usuario() {
    
    if ($("#nw_usuario").val() == "") {
        mostrarMensajeTemporal("contenedor_mensaje_add_usuario", "amarillo", "Por favor, ingrese usuario.", 3);
        return;
    }
    
    if ($("#nw_contrasenia").val() == "") {
        mostrarMensajeTemporal("contenedor_mensaje_add_usuario", "amarillo", "Por favor, ingrese contraseña.", 3);
        return;
    }
    if ($("#nw_nombres").val() == "") {
        mostrarMensajeTemporal("contenedor_mensaje_add_usuario", "amarillo", "Por favor, ingrese nombre del usuario.", 3);
        return;
    }
    if ($("#nw_ape_paterno").val() == "") {
        mostrarMensajeTemporal("contenedor_mensaje_add_usuario", "amarillo", "Por favor, ingrese apellido del usuario.", 3);
        return;
    }
    if ($("#nw_nun_doc").val() == "" ) {
        mostrarMensajeTemporal("contenedor_mensaje_add_usuario", "amarillo", "Por favor, ingrese DNI.", 3);
        return;
    }
    if ($("#nw_email").val() == "" ) {
        mostrarMensajeTemporal("contenedor_mensaje_add_usuario", "amarillo", "Por favor, ingrese email del usuario.", 3);
        return;
    }
    if ($("#nw_tipo_usuario").val() == "-1") {
        mostrarMensajeTemporal("contenedor_mensaje_add_usuario", "amarillo", "Por favor, ingrese tipo de usuario.", 3);
        return;
    }
    if ($("#nw_servicio").val() == "-1") {
        mostrarMensajeTemporal("contenedor_mensaje_add_usuario", "amarillo", "Por favor, ingrese servicio.", 3);
        return;
    } 
    posta.post(
        'usuario/guardar_usuarios',
        data= {
	        nw_usuario: $("#nw_usuario").val(),
	        nw_pass: $("#nw_contrasenia").val(),
	        nw_nueva_pass: $("#nw_rep_contrasenia").val(),
	        nw_nombres: $("#nw_nombres").val(),
	        nw_ap_paterno: $("#nw_ape_paterno").val(),
	        nw_ape_materno: $("#nw_ape_materno").val(),
	        nw_num_documento: $("#nw_nun_doc").val(),
	        nw_f_nacimiento: $("#fecha_nac").val(),
	        nw_telefono: $("#nw_telefono").val(),
	        nw_celular: $("#nw_celular").val(),
	        nw_email: $("#nw_email").val(),
	        nw_tipo_user: $("#nw_tipo_usuario").val(),
	        nw_servicio: $("#nw_servicio").val()
	    },
        function(response) {
        	if(response == "Listo"){               		 	
	            table_usuarios.ajax.reload();
	            mostrarMensajeTemporal("contenedor_mensaje_add_usuario", "verde", "Se registró con éxito!", 4);
		        $("#nw_usuario").val("");
		        $("#nw_contrasenia").val("");
		        $("#nw_rep_contrasenia").val("");
		        $("#nw_nombres").val("");
		        $("#nw_ape_paterno").val("");
		        $("#nw_ape_materno").val("");
		        $("#nw_nun_doc").val("");
		        $("#fecha_nac").val("");
		        $("#nw_telefono").val("");
		        $("#nw_celular").val("");
		        $("#nw_email").val("");
		        $("#nw_tipo_usuario").val("");
		        $("#nw_servicio").val("");
	        }else if(response.startsWith(":")){
	        	var msj = response.slice(1)
	        	mostrarMensajeTemporal("contenedor_mensaje_add_usuario", "amarillo", msj,6);
	        }else{
	     		mostrarMensajeTemporal("contenedor_mensaje_add_usuario", "rojo", "Ocurrió un error, contacte con el administrador.", 6);	
	        }
        }
    );
}

function ver_actualizar_usuarios(id, usuario, nombres, a_paterno, a_materno,
	dni, f_nacimiento, telefono, celular, email, tipoUsuario, servicio ){
	$("#modal_actualizar_usuario").modal('show');
	$("#upd_usuario").val(usuario),
	$("#upd_nombres").val(nombres),
	$("#upd_ape_paterno").val(a_paterno),
	$("#upd_ape_materno").val(a_materno),
	$("#upd_nun_doc").val(dni),
	$("#upd_fecha_nac").val(f_nacimiento),
	$("#upd_telefono").val(telefono),
	$("#upd_celular").val(celular),
	$("#upd_email").val(email),
	$("#upd_tipo_usuario").val(tipoUsuario),
	$("#upd_servicio").val(servicio)
	actualizar_id_ser = id;
}

function actualizar_usuario(){

	if ($("#upd_usuario").val() == "") {
        mostrarMensajeTemporal("contenedor_mensaje_update_usuario", "amarillo", "Por favor, ingrese usuario.", 3);
        return;
    }
     if ($("#upd_nombres").val() == "") {
        mostrarMensajeTemporal("contenedor_mensaje_update_usuario", "amarillo", "Por favor, ingrese nombre del usuario.", 3);
        return;
    }
    if ($("#upd_ape_paterno").val() == "") {
        mostrarMensajeTemporal("contenedor_mensaje_update_usuario", "amarillo", "Por favor, ingrese apellido del usuario.", 3);
        return;
    }
    if ($("#upd_nun_doc").val() == "" ) {
        mostrarMensajeTemporal("contenedor_mensaje_update_usuario", "amarillo", "Por favor, ingrese DNI.", 3);
        return;
    }
    if ($("#upd_email").val() == "" ) {
        mostrarMensajeTemporal("contenedor_mensaje_update_usuario", "amarillo", "Por favor, ingrese email del usuario.", 3);
        return;
    }
    if ($("#upd_tipo_usuario").val() == -1) {
        mostrarMensajeTemporal("contenedor_mensaje_update_usuario", "amarillo", "Por favor, ingrese tipo de usuario.", 3);
        return;
    }
    if ($("#upd_servicio").val() == -1) {
        mostrarMensajeTemporal("contenedor_mensaje_update_usuario", "amarillo", "Por favor, ingrese servicio.", 3);
        return;
    }
	posta.post(
        'usuario/actualizar_usuarios',
        data = {
        	id_user: actualizar_id_ser,
        	nw_usuario: $("#upd_usuario").val(),
	        nw_nombres: $("#upd_nombres").val(),
	        nw_ap_paterno: $("#upd_ape_paterno").val(),
	        nw_ape_materno: $("#upd_ape_materno").val(),
	        nw_num_documento: $("#upd_nun_doc").val(),
	        nw_f_nacimiento: $("#upd_fecha_nac").val(),
	        nw_telefono: $("#upd_telefono").val(),
	        nw_celular: $("#upd_celular").val(),
	        nw_email: $("#upd_email").val(),
	        nw_tipo_user: $("#upd_tipo_usuario").val(),
	        nw_servicio: $("#upd_servicio").val()
        },
        function(response) {
        	if(response == "Listo"){               		 	
	            table_usuarios.ajax.reload();
	            $("#modal_actualizar_usuario").modal('hide'); 
	            mostrarMensajeTemporal("contenedor_mensaje_usuarios", "verde", "Se actualizó con éxito!", 4);
	        }else if(response.startsWith(":")){
	        	var msj = response.slice(1)
	        	mostrarMensajeTemporal("contenedor_mensaje_update_usuario", "amarillo", msj,6);
	        }else{
	     		mostrarMensajeTemporal("contenedor_mensaje_update_usuario", "rojo", "Ocurrió un error, contacte con el administrador.", 6);	
	        }
        }
    );
}