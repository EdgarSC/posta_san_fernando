﻿function coWO(link) {
    if (link != undefined && link != null && link != "") {
        window.open(link, "_blank");
    }
}

(function ($) {
    $.fn.onlyNumbers = function (options) {
        var elem = this;
        var settings = $.extend({}, $.fn.onlyNumbers.defaults, options);

        $(elem).bind("keypress cut copy paste", function (event) {

            setTimeout(function () {
                var text = $(elem).val();
                elem.val(text.replace(/\D/g, ''));
            }, 100);

        });

    }
    $.fn.onlyNumbers.defaults = {
    }
})(jQuery);

(function ($) {
    $.fn.onlyNumbersAndPunto = function (options) {
        var elem = this;
        var settings = $.extend({}, $.fn.onlyNumbersAndPunto.defaults, options);

        $(elem).bind("keypress cut copy paste", function (event) {

            setTimeout(function () {
                var text = $(elem).val();
                elem.val(text.replace(/[^0-9\.]/g, ''));
            }, 100);

        });

    }
    $.fn.onlyNumbersAndPunto.defaults = {
    }
})(jQuery);

(function ($) {
    $.fn.onlyNumbersAndCaracteresEspeciales = function (options) {
        var elem = this;
        var settings = $.extend({}, $.fn.onlyNumbersAndCaracteresEspeciales.defaults, options);

        $(elem).bind("keypress cut copy paste", function (event) {

            setTimeout(function () {
                var text = $(elem).val();
                elem.val(text.replace(/[^A-Za-z0-9\-– /*Ñ&._<>(),]/g, ''));

            }, 100);

        });

    }
    $.fn.onlyNumbersAndCaracteresEspeciales.defaults = {
    }
})(jQuery);

(function ($) {
    $.fn.onlyNumbersAndPuntoAndNegative = function (options) {
        var elem = this;
        var settings = $.extend({}, $.fn.onlyNumbersAndPunto.defaults, options);

        $(elem).bind("keypress cut copy paste", function (event) {

            setTimeout(function () {
                var text = $(elem).val();
                elem.val(text.replace(/[^0-9\.-]/g, ''));
            }, 100);

        });

    }
    $.fn.onlyNumbersAndPunto.defaults = {
    }
})(jQuery);

(function ($) {
    $.fn.onlyLetters = function (options) {
        var elem = this;
        var settings = $.extend({}, $.fn.onlyLetters.defaults, options);

        $(elem).bind("keypress cut copy paste", function (event) {

            setTimeout(function () {
                var text = $(elem).val();
                elem.val(text.replace(/[^A-Za-z\s]/g, ''));
            }, 100);

        });

    }
    $.fn.onlyLetters.defaults = {
    }
})(jQuery);

(function ($) {
    $.fn.onlyLettersAndNumbers = function (options) {
        var elem = this;
        var settings = $.extend({}, $.fn.onlyLetters.defaults, options);

        $(elem).bind("keypress cut copy paste", function (event) {

            setTimeout(function () {
                var text = $(elem).val();

                if (settings.upperCase)
                    text = text.toUpperCase();
                    
                elem.val(text.replace(/[^A-Za-z0-9]/g, ''));

            }, 100);

        });

    }
    $.fn.onlyLettersAndNumbers.defaults = {
        upperCase: false
    }
})(jQuery);

(function ($) {
    $.fn.onlyLettersAndNumbersAndGuion = function (options) {
        var elem = this;
        var settings = $.extend({}, $.fn.onlyLettersAndNumbersAndGuion.defaults, options);

        $(elem).bind("keypress cut copy paste", function (event) {

            setTimeout(function () {
                var text = $(elem).val();

                if (settings.upperCase)
                    text = text.toUpperCase();

                elem.val(text.replace(/[^A-Za-z0-9\-]/g, ''));

            }, 100);

        });

    }
    $.fn.onlyLettersAndNumbersAndGuion.defaults = {
        upperCase: false
    }
})(jQuery);

(function ($) {
    $.fn.onlyLettersAndCharSpecial = function (options) {
        var elem = this;
        var settings = $.extend({}, $.fn.onlyLettersAndCharSpecial.defaults, options);

        $(elem).bind("keypress cut copy paste", function (event) {

            setTimeout(function () {
                var text = $(elem).val();
                elem.val(text.replace(/[^A-Za-z\sáéíóúÁÉÍÓÚñÑüÜ]/g, ''));
            }, 100);
        });
    }
    $.fn.onlyLettersAndCharSpecial.defaults = {
    }
})(jQuery);

(function ($) {
    $.fn.ForCompany = function (options) {
        var elem = this;
        var settings = $.extend({}, $.fn.ForCompany.defaults, options);

        $(elem).bind("keypress cut copy paste", function (event) {

            setTimeout(function () {
                var text = $(elem).val();
                //elem.val(text.replace(/[^A-Za-z\s0-9áéíóúÁÉÍÓÚñÑüÜ&.]+$/, ''));
                elem.val(text.replace(/[^A-Za-z\s0-9áéíóúÁÉÍÓÚñÑüÜ&.]/g, ''));
            }, 100);
        });
    }
    $.fn.ForCompany.defaults = {
    }
})(jQuery);


(function ($) {
    $.fn.onlyLettersAndNumbersAndSpace = function (options) {
        var elem = this;
        var settings = $.extend({}, $.fn.onlyLettersAndNumbersAndSpace.defaults, options);

        $(elem).bind("keypress cut copy paste", function (event) {

            setTimeout(function () {
                var text = $(elem).val();

                if (settings.upperCase)
                    text = text.toUpperCase();

                elem.val(text.replace(/[^A-Za-z0-9" "-sáéíóúÁÉÍÓÚñÑüÜ!¡¡¿-]+/g, ''));

            }, 100);

        });

    }
    $.fn.onlyLettersAndNumbersAndSpace.defaults = {
        upperCase: false
    }
})(jQuery);

(function ($) {
    $.fn.ForAddress = function (options) {
        var elem = this;
        var settings = $.extend({}, $.fn.ForAddress.defaults, options);

        $(elem).bind("keypress cut copy paste", function (event) {

            setTimeout(function () {
                var text = $(elem).val();
                elem.val(text.replace(/[^A-Za-z\s0-9áéíóúÁÉÍÓÚñÑüÜ#.]/g, ''));
            }, 100);
        });
    }
    $.fn.ForAddress.defaults = {
    }
})(jQuery);

(function ($) {
    $.fn.onlyLettersNumbersSpaceCaracterSimple = function (options) {
        var elem = this;
        var settings = $.extend({}, $.fn.onlyLettersAndNumbersAndSpace.defaults, options);

        $(elem).bind("keypress cut copy paste", function (event) {

            setTimeout(function () {
                var text = $(elem).val();

                if (settings.upperCase)
                    text = text.toUpperCase();

                elem.val(text.replace(/[^A-Za-z0-9\s(),*\-\/.]+/g, ''));

            }, 100);

        });

    }
    $.fn.onlyLettersNumbersSpaceCaracterSimple.defaults = {
        upperCase: false
    }
})(jQuery);

(function ($) {
    $.fn.onlyLettersNumbersSpace = function (options) {
        var elem = this;
        var settings = $.extend({}, $.fn.onlyLettersAndNumbersAndSpace.defaults, options);

        $(elem).bind("keypress cut copy paste", function (event) {

            setTimeout(function () {
                var text = $(elem).val();

                if (settings.upperCase)
                    text = text.toUpperCase();

                elem.val(text.replace(/[^A-Za-z0-9" "]+/g, ''));

            }, 100);

        });

    }
    $.fn.onlyLettersNumbersSpace.defaults = {
        upperCase: false
    }
})(jQuery);

(function ($) {
    $.fn.onlyLettersNumbersSpacePoint = function (options) {
        var elem = this;
        var settings = $.extend({}, $.fn.onlyLettersAndNumbersAndSpace.defaults, options);

        $(elem).bind("keypress cut copy paste", function (event) {

            setTimeout(function () {
                var text = $(elem).val();

                if (settings.upperCase)
                    text = text.toUpperCase();

                elem.val(text.replace(/[^A-Za-z0-9" ".]+/g, ''));

            }, 100);

        });

    }
    $.fn.onlyLettersNumbersSpace.defaults = {
        upperCase: false
    }
})(jQuery);

(function ($) {
    $.fn.coloadbox = function (options) {
        elem = this;
        var settings = $.extend({}, $.fn.coloadbox.defaults, options);
        var aLoading = $("").cowaitscreen({ modal: true });

        var coloadbox_ =
        {
            Show: function (data) {
                var w = window.open('', 'Debug', '');
                w.document.write(data);
                w.document.close();
            }
        }

        if (settings.load) {
            $(".loading").show();
            aLoading.show();
        }

        $.ajax({
            type: settings.type,
            url: settings.action,
            data: "TS=" + (new Date().getTime()) + "&" + settings.data + "&sessionID=" + getSessionID() + "&TopologySessionID=" + getTopologySessionID(),
            dataType: "html",
            traditional: true,
            success: function (data) {

                if (dialogSessionExpired(data) == false) {
                    if (settings.debug)
                        coloadbox_.Show(data);

                    $(settings.elemdialog).empty();
                    $(settings.elemtag).empty();
                    $(settings.elemtag).show();
                    $(settings.elemtag).html(data);

                    if (($.validator != undefined) && ($.validator != null))
                        $.validator.unobtrusive.parse(settings.elemtag);

                }

                if (settings.load) {
                    $(".loading").hide();
                    if (aLoading != null) {
                        aLoading.close();
                    }

                }
            },
            error: function (xhr, ajaxOptions, thrownError) {

                if (settings.load) {
                    $(".loading").hide();
                    if (aLoading != null) {
                        aLoading.close();
                    }
                }
                if (coenv != KDCoEnv.Producao)
                    alert("Alerta: coloadbox s=" + xhr.status + " te=" + thrownError + " rt=" + xhr.responseText);
            }

        });

    }

    $.fn.coloadbox.defaults = {
        type: "GET",
        action: "",
        elemtag: ".corpo-box",
        elemdialog: "corpo-dialog",
        debug: false,
        load: true,
        data: ""
    }

})(jQuery);


(function ($) {
    $.fn.cowaitscreen = function (options) {
        var elem = this;
        var settings = $.extend({}, $.fn.cowaitscreen.defaults, options);

        var aLoadingForm = null;

        var Sessao = "";
        Sessao += "<section  id='" + settings.nomediv + "'>";
        Sessao += "    <i class='fa fa-spinner fa-spin fa-3x fa-fw margin-bottom'></i><br />Procesando...";
        Sessao += "</section>";

        var ExisteDiv = $('body').find('#' + settings.nomediv).length > 0 ? true : false;
        if (!ExisteDiv) {
            $('body').append(Sessao);

            $('#' + settings.nomediv).css("display", "none");
            $('#' + settings.nomediv).css("width", "auto");
            $('#' + settings.nomediv).css("height", "auto");
            $('#' + settings.nomediv).css("border", "1px solid rgba(255, 255, 255, 0)");
            $('#' + settings.nomediv).css("background-color", "rgba(255, 255, 255, 0)");
            //$('#'+settings.nomediv).css("box-shadow","4px 4px 13px #696969");
            //$('#'+settings.nomediv).css("border-radius","4px");
            $('#' + settings.nomediv).css("opacity", "2.9");
        }

        var lsplashscreenform =
        {
            close: function () {
                aLoadingForm.close();
            },
            show: function () {

                aLoadingForm = $("").coloadmodalform({
                    form: settings.nomediv,
                    center: settings.center,
                    modal: settings.modal,
                    zindex: 999999,
                    load: false
                });
            }
        }

        return lsplashscreenform;

    }
    $.fn.cowaitscreen.defaults = {
        nomediv: "FormcoWaitScreen",
        modal: false,
        center: true
    }
})(jQuery);

//** teste // 
(function ($) {
    $.fn.coloadmodalform = function (options) {
        var aCoLoadModalElemOrigem = this;
        var settings = $.extend({}, $.fn.coloadmodalform.defaults, options);
        var aLoading = null;

        if (settings.waitscreen == true) {
            aLoading = $("").cowaitscreen({ modal: true }); //carregando o loading;
        }

        var lZIndex = $(this).css("z-index");
        if ((lZIndex != 'auto') && (lZIndex != null)) {
            settings.zindex = parseInt(lZIndex) + 10;
        }

        var lloadmodalform =
        {
            close: function () {
                if (settings.afterclose != null) { settings.afterclose(); }

                if (settings.modal == true) { var DivMask = "#divMask" + settings.form; }
                var DivModalForm = "#divModalForm" + settings.form;

                if (settings.modal == true) { $(DivMask).remove(); }
                $(DivModalForm).remove();

                if (settings.load == false) { $('#' + DivFom).css({ 'display': 'none' }); }
            },
            getzindex: function () {
                return settings.zindex;
            },
            buttoncloseform: function (divremove, divdisplaynone) {
                $('#' + settings.buttonclose).click(function (e) {
                    e.preventDefault();
                    lloadmodalform.close();
                    //caso se defina uma funcao para quando a tela fechar
                    if (settings.functionclose != null)
                        settings.functionclose();
                });
            },
            atualizecss: function (divatualizecss) {
                if (settings.width != null) {
                    $('#' + divatualizecss).css({ 'width': settings.width });
                }
                if (settings.height != null) {
                    $('#' + divatualizecss).css({ 'height': settings.height });
                }

                if (settings.center == true) {
                    lloadmodalform.centralizamodal();
                }
                else {
                    lloadmodalform.posicaofixamodal();
                }

                $('#' + divatualizecss).css({ 'z-index': settings.zindex + 20 });
                $('#' + divatualizecss).css({ 'position': 'absolute' });
                $('#' + divatualizecss).css({ 'display': 'none' });
                $('#' + divatualizecss).css({ 'padding': '0px' });


                if (settings.draggable == true)
                    $('#' + DivFom).draggable();
                if (settings.resizable == true)
                    $('#' + DivFom).resizable();

            },
            posicaofixamodal: function () {
                var posicionX = settings.positionX;
                var posicionY = settings.positionY;
                if (posicionY < 0) { posicionY = 10; }
                if (posicionX < 0) { posicionX = 10; }

                $("#" + settings.form).css({ 'top': posicionY, 'left': posicionX });
            },
            centralizamodal: function () {

                if (settings.center == true) {

                    //if (aCoLoadModalElemOrigem.height() == null && aCoLoadModalElemOrigem.width() == null) {
                    if ((settings.load == true) || ((aCoLoadModalElemOrigem.height() == null && aCoLoadModalElemOrigem.width() == null))) {

                        var winH = $(window.parent).height();
                        var winW = $(window.parent).width();

                        var posicionY = (winH / 2 - $("#" + settings.form).height() / 2);
                        var posicionX = (winW / 2 - $("#" + settings.form).width() / 2);

                        posicionY = posicionY + $(window.parent).scrollTop();
                        posicionX = posicionX + $(window.parent).scrollLeft();
                    }
                    else {
                        var difH = ($('body').height() - aCoLoadModalElemOrigem.height()) / 2;
                        var difW = ($('body').width() - aCoLoadModalElemOrigem.width()) / 2;

                        var winH = ($(window.parent).height() / 2);
                        var winW = ($(window.parent).width() / 2);

                        var posicionY = ((winH - ($("#" + settings.form).height() / 2))) - difH;
                        var posicionX = ((winW - ($("#" + settings.form).width() / 2))) - difW;

                        posicionY = posicionY + $(window.parent).scrollTop();
                        posicionX = posicionX + $(window.parent).scrollLeft();
                    }

                    if (posicionY < 0) { posicionY = 10; }
                    if (posicionX < 0) { posicionX = 10; }
                    $("#" + settings.form).css({ 'top': posicionY, 'left': posicionX });
                }

            },
            formtomodal: function () {
                var DivModalForm = "#divModalForm" + settings.form;
                $(DivModalForm).find('.nopModal').show();
                $(DivModalForm).find("#" + settings.form).css({ 'margin': '0 auto' });
                $(DivModalForm).find("#" + settings.form).css({ 'border': 'solid 1px #8F9EB6' });
                $(DivModalForm).find("#" + settings.form).css({ 'border-radius': '5px' });
                $(DivModalForm).find("#" + settings.form).css({ 'margin': '0 auto' });
                $(DivModalForm).find("#" + settings.form).css({ 'padding': '10px' });
                $(DivModalForm).find("#" + settings.form).css({ 'box-shadow': '5px 5px 10px #ccc' });
                $(DivModalForm).find("#" + settings.form).css({ 'background': 'White' });
                $("#" + settings.form).css({ 'top': 0, 'left': 0 });

                if (settings.width != null) {
                    $("#" + settings.form).css({ 'width': settings.width });
                }
                if (settings.height != null) {
                    $("#" + settings.form).css({ 'height': settings.height });
                }

                if (settings.center == true) {
                    lloadmodalform.centralizamodal();
                }
                else {
                    lloadmodalform.posicaofixamodal();
                }
            }
        }

        if (settings.modal == true) { var DivMask = "divMask" + settings.form; }
        var DivModalForm = "divModalForm" + settings.form;
        var DivFom = settings.form;

        var ExisteDiv = $('body').find("#" + DivMask).length > 0 ? true : false;
        if ((!ExisteDiv) && (settings.modal == true)) {
            var lDiv = "<div id='" + DivMask + "' style='position:absolute;left:0;top:0;background-color:#000;display:none;'></div>";
            $('body').append(lDiv);
            $('#' + DivMask).css({ 'z-index': settings.zindex + 10 });

            $(window).scroll(function () {
                if ($('body').find("#" + DivMask).length > 0) {
                    $('#' + DivMask).css({ 'width': $(window).width(), 'height': $(document).height() });
                }
            });

            $(window).resize(function () {
                if ($('body').find("#" + DivMask).length > 0) {
                    $('#' + DivMask).css({ 'width': $(window).width(), 'height': $(document).height() });
                }
            });

        }

        $(window).resize(function () {
            //quando diminuir a tela tem que centralizar
            lloadmodalform.centralizamodal();
        });

        var ExistDivModalForm = $('body').find("#" + DivModalForm).length > 0 ? true : false;
        if (!ExistDivModalForm) {
            var lDivModalForm = "<section id='" + DivModalForm + "'></section>";
            $('body').append(lDivModalForm);
        }

        if (settings.modal == true) { $('#' + DivMask).css({ 'width': $(window).width(), 'height': $(document).height() }); }

        //var aLoading = $("").cowaitscreen(); //carregando o loading;
        //if (aLoading != null) aLoading.show(); //mostrando o loading;

        if (settings.load == true) {
            if (aLoading != null) aLoading.show(); //mostrando o loading;

            $("#" + DivModalForm).load(settings.action, { jsonparametros: settings.jsonparametros }, function (response, status, xhr) {

                if (aLoading != null) aLoading.close(); //mostrando o loading;

                if (status == "success") {

                    if (settings.modal == true) { $('#' + DivMask).fadeTo("fast", 0.2); }

                    lloadmodalform.atualizecss(DivFom);
                    $("#" + DivModalForm).find("#" + DivFom).show();
                    if (settings.afterload != null)
                        settings.afterload(lloadmodalform);

                    if (settings.formtomodal == true)
                        lloadmodalform.formtomodal();

                    lloadmodalform.buttoncloseform(DivModalForm, null);
                    //if (aLoading != null) aLoading.close(); //mostrando o loading;

                }
            });
        }
        else {

            if (settings.modal == true) { $('#' + DivMask).fadeTo("fast", 0.2); }

            lloadmodalform.atualizecss(DivFom);
            $("#" + DivFom).show();

            if (settings.afterload != null)
                settings.afterload();

            lloadmodalform.buttoncloseform(DivModalForm, DivFom);
        }

        if (settings.buttonconfirm != "") {
            var lBotaoConfirm = $('#' + settings.buttonconfirm);
            lBotaoConfirm.unbind("click");
            lBotaoConfirm.click(function (e) {
                settings.functionconfirm(lloadmodalform);
            });

        }

        if (settings.modal == true) {
            $('#' + DivMask).css({ 'width': $(window).width(), 'height': $(document).height() });
            $('#' + DivMask).fadeTo("fast", 0.2);
        }

        if (settings.draggable == true)
            $('#' + DivFom).draggable();
        if (settings.resizable == true)
            $('#' + DivFom).resizable();

        return lloadmodalform;

    }
    $.fn.coloadmodalform.defaults = {
        form: "dialog",
        load: true,
        modal: true,
        html: null,
        action: null,
        positionX: 10,
        positionY: 10,
        center: false,
        buttonclose: "",
        buttonconfirm: "",
        functionconfirm: null,
        functionclose: null,
        zindex: 9000,
        afterload: null,
        width: null,
        height: null,
        formtomodal: false,
        draggable: false,
        resizable: false,
        afterclose: null,
        jsonparametros: "",
        waitscreen: true
    }
})(jQuery);

(function ($) {
    $.fn.showDiv = function (options) {
        var settings = $.extend({}, $.fn.showDiv.defaults, options);
        $('#' + settings.section).children().each(function (n, i) {
            var id = this.id;
            if (id == settings.divId) {
                $('#' + id).removeAttr('style').children(':last').hide().effect("slide", options, 100);
            }
            else {
                if (id != settings.divIdVisible)
                    $('#' + id).attr('style', 'display:none');
            }
        });
    }

    $.fn.showDiv.defaults = {
        section: null,
        divId: null,
        divIdVisible: null
    }

})(jQuery);

//returns the first index found
function arrayFirstIndexOf(array, predicate, predicateOwner) {
    for (var i = 0, j = array.length; i < j; i++) {
        if (predicate.call(predicateOwner, array[i])) {
            return i;
        }
    }
    return -1;
}


//returns the age
function getAge(prDate) {

    var dateParts = prDate.split("/");

    prDate = new Date(dateParts[2], (dateParts[1] - 1), dateParts[0]);

    var lYear = prDate.getFullYear();
    var lMonth = prDate.getMonth();
    var lDay = prDate.getDate();

    var lCurrentDate = new Date,
        lCurrentYear = lCurrentDate.getFullYear(),
        lCurrentMonth = lCurrentDate.getMonth() + 1,
        lCurrentDay = lCurrentDate.getDate(),
        lYear = +lYear,
        lMonth = +lMonth,
        lDay = +lDay,

        lAge = lCurrentYear - lYear;

    if (lCurrentMonth < lMonth || lCurrentMonth == lMonth && lCurrentDay < lDay) {
        lAge--;
    }

    return lAge < 0 ? 0 : lAge;
}

//-----------------------------INI Form Wizard -------------------------//
if (jQuery().bootstrapWizard) {
    $('#form-wizard-1').bootstrapWizard({
        'nextSelector': '.button-next',
        'previousSelector': '.button-previous',
        onTabClick: function (tab, navigation, index) {
            alert('on tab click disabled');
            return false;
        },
        onNext: function (tab, navigation, index) {
            var total = navigation.find('li').length;
            var current = index + 1;
            // set wizard title
            $('.step-title', $('#form-wizard-1')).text('Step ' + (index + 1) + ' of ' + total);
            // set done steps
            jQuery('li', $('#form-wizard-1')).removeClass("done");
            var li_list = navigation.find('li');
            for (var i = 0; i < index; i++) {
                jQuery(li_list[i]).addClass("done");
            }

            if (current == 1) {
                $('#form-wizard-1').find('.button-previous').hide();
            } else {
                $('#form-wizard-1').find('.button-previous').show();
            }

            if (current >= total) {
                $('#form-wizard-1').find('.button-next').hide();
                $('#form-wizard-1').find('.button-submit').show();
            } else {
                $('#form-wizard-1').find('.button-next').show();
                $('#form-wizard-1').find('.button-submit').hide();
            }
            var $percent = (current / total) * 100;
            $('#form-wizard-1').find('.progress-bar').css('width', $percent + '%');

            $('html, body').animate({ scrollTop: $("#form-wizard-1").offset().top }, 900);
        },
        onPrevious: function (tab, navigation, index) {
            var total = navigation.find('li').length;
            var current = index + 1;
            // set wizard title
            $('.step-title', $('#form-wizard-1')).text('Step ' + (index + 1) + ' of ' + total);
            // set done steps
            jQuery('li', $('#form-wizard-1')).removeClass("done");
            var li_list = navigation.find('li');
            for (var i = 0; i < index; i++) {
                jQuery(li_list[i]).addClass("done");
            }

            if (current == 1) {
                $('#form-wizard-1').find('.button-previous').hide();
            } else {
                $('#form-wizard-1').find('.button-previous').show();
            }

            if (current >= total) {
                $('#form-wizard-1').find('.button-next').hide();
                $('#form-wizard-1').find('.button-submit').show();
            } else {
                $('#form-wizard-1').find('.button-next').show();
                $('#form-wizard-1').find('.button-submit').hide();
            }
            var $percent = (current / total) * 100;
            $('#form-wizard-1').find('.progress-bar').css('width', $percent + '%');

            $('html, body').animate({ scrollTop: $("#form-wizard-1").offset().top }, 900);
        },
        onTabShow: function (tab, navigation, index) {
            var total = navigation.find('li').length;
            var current = index + 1;
            var $percent = (current / total) * 100;
            $('#form-wizard-1').find('.progress-bar').css({
                width: $percent + '%'
            });
        }
    });

    $('#form-wizard-1').find('.button-previous').hide();
    $('#form-wizard-1 .button-submit').click(function () {
        alert('Finished0!');
    }).hide();


    //Validation of wizard form
    if (jQuery().validate) {
        var removeSuccessClass = function (e) {
            $(e).closest('.form-group').removeClass('has-success');
        }
        var jq_validator = $('#wizard-validation').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            errorPlacement: function (error, element) {
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            },
            focusInvalid: false, // do not focus the last invalid input

            invalidHandler: function (event, validator) { //display error alert on form submit              

            },

            highlight: function (element) { // hightlight error inputs
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error'); // set error class to the control group
            },

            unhighlight: function (element) { // revert the change dony by hightlight
                $(element).closest('.form-group').removeClass('has-error'); // set error class to the control group
                setTimeout(function () { removeSuccessClass(element); }, 3000);
            },

            success: function (label) {
                label.closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
            }
        });
    }
    //Look at onNext function to see how add validation to wizard
    $('#form-wizard-2').bootstrapWizard({
        'nextSelector': '.button-next',
        'previousSelector': '.button-previous',
        onTabClick: function (tab, navigation, index) {
            alert('on tab click disabled');
            return false;
        },
        onNext: function (tab, navigation, index) {
            var valid = $("#wizard-validation").valid();
            if (!valid) {
                jq_validator.focusInvalid();
                return false;
            }

            var total = navigation.find('li').length;
            var current = index + 1;
            // set wizard title
            $('.step-title', $('#form-wizard-2')).text('Step ' + (index + 1) + ' of ' + total);
            // set done steps
            jQuery('li', $('#form-wizard-2')).removeClass("done");
            var li_list = navigation.find('li');
            for (var i = 0; i < index; i++) {
                jQuery(li_list[i]).addClass("done");
            }

            if (current == 1) {
                $('#form-wizard-2').find('.button-previous').hide();
            } else {
                $('#form-wizard-2').find('.button-previous').show();
            }

            if (current >= total) {
                $('#form-wizard-2').find('.button-next').hide();
                $('#form-wizard-2').find('.button-submit').show();
            } else {
                $('#form-wizard-2').find('.button-next').show();
                $('#form-wizard-2').find('.button-submit').hide();
            }
            var $percent = (current / total) * 100;
            $('#form-wizard-2').find('.progress-bar').css('width', $percent + '%');

            $('html, body').animate({ scrollTop: $("#form-wizard-2").offset().top }, 900);
        },
        onPrevious: function (tab, navigation, index) {
            var total = navigation.find('li').length;
            var current = index + 1;
            // set wizard title
            $('.step-title', $('#form-wizard-2')).text('Step ' + (index + 1) + ' of ' + total);
            // set done steps
            jQuery('li', $('#form-wizard-2')).removeClass("done");
            var li_list = navigation.find('li');
            for (var i = 0; i < index; i++) {
                jQuery(li_list[i]).addClass("done");
            }

            if (current == 1) {
                $('#form-wizard-2').find('.button-previous').hide();
            } else {
                $('#form-wizard-2').find('.button-previous').show();
            }

            if (current >= total) {
                $('#form-wizard-2').find('.button-next').hide();
                $('#form-wizard-2').find('.button-submit').show();
            } else {
                $('#form-wizard-2').find('.button-next').show();
                $('#form-wizard-2').find('.button-submit').hide();
            }
            var $percent = (current / total) * 100;
            $('#form-wizard-2').find('.progress-bar').css('width', $percent + '%');

            $('html, body').animate({ scrollTop: $("#form-wizard-2").offset().top }, 900);
        },
        onTabShow: function (tab, navigation, index) {
            var total = navigation.find('li').length;
            var current = index + 1;
            var $percent = (current / total) * 100;
            $('#form-wizard-2').find('.progress-bar').css({
                width: $percent + '%'
            });
        }
    });

    $('#form-wizard-2').find('.button-previous').hide();
    $('#form-wizard-2 .button-submit').click(function () {
        alert('Finished2!');
    }).hide();
}

//----------------------------- END Form Wizard -------------------------//



//INI: Globalization
function foundGlobalizationResource(prcoGlobalization, prResourceKey) {
    var result = $.grep(prcoGlobalization, function (e) { return e.Key == prResourceKey; });
    return result[0].Value;
};


function getGlobalizationResource(prViewBagGlobalization) {
 
    var lcoGlobalization = prViewBagGlobalization;
    lcoGlobalization = lcoGlobalization.ReplaceCharSpecial();
    lcoGlobalization = ko.utils.parseJson(lcoGlobalization);
    
    return lcoGlobalization;
};

//END: Globalization


//----------------------------- END Form Wizard -------------------------//
function compareDates(prdtData1, prdtData2) { //this function 

    var lMensagem = undefined;
    var dateToday = new Date();
    var date = dateToday.getDate();
    if (date.toString().length == 1) {
        date = "0" + date;
    }
    var month = dateToday.getMonth()+1;
    if (month.toString().length == 1) {
        month = "0" + month;
    }
    var year = dateToday.getFullYear();

    var currentDay = new Date(year, month - 1, date);
    
    var date1 = prdtData1.substring(0, 2);     
    var month1 = prdtData1.substring(3, 5);
    var year1 = prdtData1.substring(6, 10);

    var dateToCompare1 = new Date(year1, month1 - 1, date1);

    var date2 = prdtData2.substring(0, 2);
    var month2 = prdtData2.substring(3, 5);
    var year2 = prdtData2.substring(6, 10);
    var dateToCompare2 = new Date(year2, month2 - 1, date2);

    var litReturn = 0;
    if (dateToCompare1 === dateToCompare2) {
        litReturn = 0;
        lMensagem = "Datas Iguais";
    }

    if (dateToCompare1 > dateToCompare2) {
        litReturn = 1;
        lMensagem = "Data inicial maior que data final";
    }

    if (dateToCompare1 < currentDay || dateToCompare2 < currentDay) {
        litReturn = 2;
        lMensagem = "Data menor que a data atual";
    }

    return lMensagem; 
};

function intervalDates(prdtStart, prdtEnd) {
    isValid = true;

    var date1 = prdtStart.substring(0, 2);
    var month1 = prdtStart.substring(3, 5);
    var year1 = prdtStart.substring(6, 10);
    var dateStart = new Date(year1, month1 - 1, date1);

    var date2 = prdtEnd.substring(0, 2);
    var month2 = prdtEnd.substring(3, 5);
    var year2 = prdtEnd.substring(6, 10);
    var dateEnd = new Date(year2, month2 - 1, date2);

    if (dateStart > dateEnd) {
        isValid = false;
    }

    return isValid;
}


function compareDateToday(prdtData) {

    var lMensagem = undefined;
    var dateToday = new Date();
    var date = dateToday.getDate();
    if (date.toString().length == 1) {
        date = "0" + date;
    }
    var month = dateToday.getMonth() + 1;
    if (month.toString().length == 1) {
        month = "0" + month;
    }
    var year = dateToday.getFullYear();
    var currentDay = new Date(year, month - 1, date);

    var date1 = prdtData.substring(0, 2);
    var month1 = prdtData.substring(3, 5);
    var year1 = prdtData.substring(6, 10);
    var dateToCompare1 = new Date(year1, month1 - 1, date1);

    if (dateToCompare1 < currentDay) {
        lMensagem = "Invalid Date";
    }

    return lMensagem;
};

function refreshCaptcha(prIDImg) {

    var element = '#' + prIDImg;
    src = $(element).attr('src');
    // check for existing ? and remove if found
    queryPos = src.indexOf('?');
    if (queryPos != -1) {
        src = src.substring(0, queryPos);
    }
    $(element).attr('src', src + '?' + Math.random());
};


(function ($) {
    $.fn.coserverUpload = function (options) {
        var settings = $.extend({}, $.fn.coserverUpload.defaults, options);

        var coserverUpload_ =
        {
            parameters: "",

            errorHandling: function (dado) {
                var lerro = "";
                if (dado != null && dado.astType != undefined && dado.astType != null)
                    lerro = dado.astMessage;
                else {
                    $.map(dado, function (item) {
                        lerro += (item.astType == KDCoMessage_Type.kdError) ? item.astMessage + "</br>" : "";
                        return {};
                    });
                }

                if (settings.onError != null) {
                    if (dialogSessionExpired(dado) == false)
                        settings.onError(dado);
                }
            },

            progressHandlingFunction: function (e) {
                if (e.lengthComputable) {
                    $('progress').attr({ value: e.loaded, max: e.total });
                }
            },

            callService: function (parameters) {
                var aLoading = $("").cowaitscreen({ modal: true }); 

                if (settings.splashscreen == true) {
                    aLoading.show(); 
                }

                coserverUpload_.parameters = parameters;              
                
                var lFormData = new FormData();

                $.each($("input[type=file]"), function (i, obj) {
                    $.each(obj.files, function (j, file) {
                        lFormData.append('file[' + i + ']', file);

                        if (!isNullWhiteSpaceUndefined(settings.keyName))
                        {
                            var lKeyNames = settings.keyName.split(",");
                            for (var lCont = 0; lCont < lKeyNames.length; lCont++) {
                                var lKey = lKeyNames[lCont];
                                lFormData.append('form[' + i + ']', $("#" + lKey + (settings.allFiles ? i : "")).val());
                            }
                        }
                    })
                });

                if (parameters != null && parameters.length > 0)
                {
                    for (var i = 0; i < parameters.length; i++) {
                        lFormData.append(parameters[i].key, parameters[i].value);
                    }
                }

                lFormData.append('sessionID', getSessionID());
                lFormData.append('TopologySessionID', getTopologySessionID());

                $.ajax({
                    type: settings.type,
                    url: settings.url,
                    data: lFormData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    dataType: "json",
                    //traditional: true,
                    success: function (dado) {
                        var erro = false;
                        var information = "";
                        var warning = "";
                        var message = "";

                        $(".loading").hide();

                        if (settings.splashscreen == true) {
                            if (aLoading != null) aLoading.close(); 
                        }

                        if (dado != null && dado.astType != undefined && dado.astType != null)
                            erro = (dado.astType == KDCoMessage_Type.kdError) ? true : false;

                        if (!erro) {
                            $.map(dado, function (item) {
                                if (item != null && item.astType != undefined && item.astType != null) {
                                    if (!erro) {
                                        erro = (item.astType == KDCoMessage_Type.kdError) ? true : false;
                                    }

                                    message += (item.astType == KDCoMessage_Type.kdError) ? item.astMessage : "";
                                    information += (item.astType == KDCoMessage_Type.kdInformation) ? item.astMessage : "";
                                    warning += (item.astType == KDCoMessage_Type.kdWarning) ? item.astMessage : "";

                                }
                                else
                                    coserverUpload_.qtdResult++;

                                return {};
                            });
                        }

                        if (erro) {
                            coserverUpload_.errorHandling(dado);
                        }
                        else {

                            if (settings.onSuccess != null) {
                                settings.onSuccess(dado);
                            }
                        }

                        if (information != "" && settings.clsMensagemInformation != null) {
                            alert(information);
                        } 
                        else if (warning != "" && settings.clsMensagemWarning != null) {
                            alert(warning);
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {

                        $(".loading").hide();

                        if (settings.splashscreen) {
                            if (aLoading != null) aLoading.close(); 
                        }

                        if (settings.onError != null) {
                            settings.onError(xhr, ajaxOptions, thrownError);
                        }
                        else {
                            alert("Alerta: coloadbox s=" + xhr.status + " te=" + thrownError + " rt=" + xhr.responseText);
                        }
                    },
                    xhr: function () {
                        var myXhr = $.ajaxSettings.xhr();
                        if (myXhr.upload) { // Checa se a propriedade de upload existe
                            myXhr.upload.addEventListener('progress', coserverUpload_.progressHandlingFunction, false); // Controla o progresso do upload
                        } else {
                            alert("no existe upload")
                        }
                        return myXhr;
                    },
                });

            }
        }

        return coserverUpload_;
    }

    $.fn.coserverUpload.defaults = {
        type: "POST",
        url: "",
        onSuccess: null,
        onError: null,
        clsMensagemRetorno: null,
        clsMensagemWarning: null,
        clsMensagemInformation: null,
        splashscreen: false,
        multiple: false,
        allFiles: false,
        keyName: ""
    }

})(jQuery);

(function ($) {
    $.fn.coLoadDiv = function (options) {
        var settings = $.extend({}, $.fn.coLoadDiv.defaults, options);

        var coLoadDiv_ =
        {
            
            callService: function (parameters) {

                var aLoading = $("").cowaitscreen({ modal: true });

                if (settings.splashscreen == true) {
                    aLoading.show();
                }

                $(settings.elementID).load(settings.action, { sessionID: getSessionID(), TopologySessionID: getTopologySessionID() }, function (response, status, xhr) {

                    if (settings.splashscreen == true) {
                        if (aLoading != null) aLoading.close();
                    }

                    if (dialogSessionExpired(response) == false) {
                        if (status == "success") {
                            settings.cbhSucess(response);
                        }
                        else {
                            $.growl.error(null, 'Error', 'No hay conexión a internet.');
                        }
                    }
                });
            }
        }

        return coLoadDiv_;
    }

    $.fn.coLoadDiv.defaults = {
        action: "",
        cbhSucess: null,
        cbhError: null,
        splashscreen: false,
        elementID: ""
    }

})(jQuery);


function dialogSessionExpired(dado)
{
    var lSource = "";

    if (dado != null && dado[0] != null) {

        lSource = dado[0].astSource;
        if (isNullWhiteSpaceUndefined(lSource))
        {
            try {
                var obj = JSON.parse(dado);
                lSource = obj[0].astSource;
            }
            catch (e) {
                return false;
            }
        }
    }

    if (lSource == "SessionExpired") {
        BootstrapDialog.show({
            type: BootstrapDialog.TYPE_DANGER,
            title: 'ERROR SESSION',
            message: "Sesión ha finalizado.",
            cssClass: 'modal-danger',
            buttons: [{
                label: 'OK',
                action: function (dialog) {
                    window.location.reload(true);
                    dialog.close();
                }
            }]
        });

        window.setTimeout(function () {
            window.location.reload(true);
        }
        , 2000);
    }
    else
    {
        return false;
    }
};

function isNullWhiteSpaceUndefined(prString) {
    if (prString == undefined || prString == "" || prString == null || prString == "-1") {
        return true;
    }
    else {
        return false;
    }
};

function getSessionID()
{
    return $('#PostLoginIDColoadModal').val();
}

function getTopologySessionID() {
    return $('#PostLoginIDTopology').val();
}


var testEmailBase = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
var isValidEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;