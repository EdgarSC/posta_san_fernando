﻿
/* Enviorment */
var KDCoEnv = {
    "Desenvolvimento": 0,
    "Teste": 1,
    "Homologacao": 2,
    "Producao": 3
};

var coenv = KDCoEnv.Producao; 

var KDCoMessage_Type = {
    "kdInformation" : 0,
    "kdWarning" : 1,
    "kdError" : 2,
    "kdSystem": 3,
    "kdRemove": 4
};


$(function() {
    $(window).scroll(function()
    {
        var lTop = $('#header').height(); // height of the top
        var lScrollTop = $(window).scrollTop(); // ScrollTop
        
        if(lScrollTop >= (lTop  - 30)){                                
            $('#header_main_alternate').css({'position' : 'absolute', 'margin-top' : Math.abs(lScrollTop - lTop ) , 'width' : '100%' });
          
        }else{
          $('#header_main_alternate').css({'position' : 'relative', 'margin-top' : 0});          
        }
    });
});

function coWO(link) {
    if (link != undefined && link != null && link != "") {
        window.open(link, "_blank");
    }
}

function ArrayFind(prarray, campo, value)
{
       var existe = false;       
       for (var i = 0; i < prarray.length; i++) {            
            if (prarray[i][campo] == value){
                existe = true;
                break;
            }else if (prarray[i].aCoEntity[campo] == value){
                existe = true;
                break;
            }
        }       
        
        return existe;
}

(function ($) {
    $.fn.embTab = function (options){
        var aTab = this;
        
        $(".tab").click(function () {
            
            $(".active_tab").removeClass("active_tab");
            $(".active_tab_content").removeClass("active_tab_content");

            $($(this).attr("data-fake-id")  + "-container").addClass("active_tab_content");
            $(this).addClass("active_tab");
        });
    };

    $.fn.embTab.defaults = {
        width: 300
    };
})(jQuery);

function validarCampos(){
}

(function ($) {
    $.fn.validacao = function (options){        
         
        var settings = $.extend({}, $.fn.validacao.defaults, options);
        $(this).attr('data-ErrorMessage', settings.mensagemErro);
        $(this).attr('data-type-validation', settings.tipo);
        
        var icon = $('<i/>').addClass('fa fa-exclamation-circle').attr({id:'err' + $(this).attr('id'), title: settings.mensagem } ).css('margin','-22px').css('color','#B02B2C').hide();
        $(this)['after'](icon);
        $(this).attr({title:'Required Field'});
        $(this).on('blur',function(){_onBlur(this)});

        _onBlur = function(lInput){            
            switch($(lInput).attr('data-type-validation'))
            {
                case 'vazio':
                    _validarVazio(lInput);
                break;
                case 'cpf':                    
                    _validarCpf(lInput);
                break;
                case 'data':
                    _validarData(lInput);
                break;
            }
        }

        _validarVazio = function(prinput){
            if($(prinput).val() == ""){
                 $('#err' + $(prinput).attr('id')).show();                 
                 _MostrarMensagemErro(prinput);
                 
            }else{
                $(prinput).removeAttr('title');
                _RemoverMensagemErro(prinput)
            }
        }
        
        _validarCpf = function(prinput){                        
            if($(prinput).val() == "" || !_IsCPFValido($(prinput).val())){
                 $('#err' + $(prinput).attr('id')).show();                 
                 _MostrarMensagemErro(prinput);                 
            }else{
                    $(prinput).removeAttr('title');
                    _RemoverMensagemErro(prinput);
            }
        }

        _validarData = function(prinput){                                    
            if($(prinput).val() == "" || !_IsDataValida($(prinput).val())){
                $('#ico' + $(prinput).attr('id')).hide();
                $('#err' + $(prinput).attr('id')).show();
                
                _MostrarMensagemErro(prinput);
                
            }else{                
                //$('#err' + $(prinput).attr('id')).hide();
                $('#ico' + $(prinput).attr('id')).show();
                _RemoverMensagemErro(prinput);
            }
        }

        _MostrarMensagemErro = function(prinput){
            $('.nopDefault').hide();

            if(!$('aside > div > section > div > h3').hasClass('nopTituloAtencao'))
            {
                //$('aside > div > section > div > h3').remove();
                var h3 = $('<h3/>').css('color','#B02B2C').addClass('nopTituloAtencao');
                var ico = $('<i/>').addClass('fa fa-asterisk nopTituloAtencao');
                $(h3).append(ico);
                $(h3).append("&#160;Verifique a(s) mensagem(s) abaixo");
                $("aside > div > section > div").prepend(h3);
            }

            if(!$(prinput).hasClass('hasError'))
            {
                var p = $('<p/>').attr('id','p' + $(prinput).attr('id')).text($(prinput).attr('data-ErrorMessage')).addClass('nopDataInvalida nopErro');

                $("aside > div > section > div").prepend(h3);
                $('aside > div > section > div > section ').prepend(p);
            }
            $(prinput).addClass('hasError');
        }

        _RemoverMensagemErro = function(prinput){
            $('.nopDefault').hide();
            $(prinput).removeClass('hasError');
            var id = $(prinput).attr('id');
            $('#p' + id ).remove();            
            $('#err' + $(prinput).attr('id')).hide();

            if (!$('aside > div > section > div > section > p').hasClass('nopErro'))
            {
                $(".nopTituloAtencao").remove();
                $('.nopDefault').show();
            }
        }

        _IsDataValida = function(value){
            var check = false;

            var adata = value.split('/');  
            var gg = parseInt(adata[0],10);  
            var mm = parseInt(adata[1],10);  
            var aaaa = parseInt(adata[2],10);  
            var xdata = new Date(aaaa,mm-1,gg);  
            if ( ( xdata.getFullYear() == aaaa ) && ( xdata.getMonth () == mm - 1 ) && ( xdata.getDate() == gg ) )  
                    check = true;  
            else  
                    check = false;              
            return check;
        }

        _IsCPFValido = function(value){
            value = value.replace(/[^\d]+/g,'');
            var numeros, digitos, soma, i, resultado, digitos_iguais;
            digitos_iguais = 1;
            if (value.length < 11)
                return false;
    
            for (i = 0; i < value.length - 1; i++)
                if (value.charAt(i) != value.charAt(i + 1))
                {
                    digitos_iguais = 0;
                    break;
                }
    
            if (!digitos_iguais)
            {
              numeros = value.substring(0,9);
              digitos = value.substring(9);
              soma = 0;
              for (i = 10; i > 1; i--)
                soma += numeros.charAt(10 - i) * i;
                resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
                if (resultado != digitos.charAt(0))
                    return false;
                numeros = value.substring(0,10);
                soma = 0;
                for (i = 11; i > 1; i--)
                    soma += numeros.charAt(11 - i) * i;
                
                resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
                if (resultado != digitos.charAt(1))
                    return false;
                return true;
              }
            else
            return false;
        }

            $.fn.validacao.CheckOff = function(){
                $('.form_element > .required, .text_input').each(function(){                    
                    $(this).unbind('blur');
                    _RemoverMensagemErro(this);                    
                });
            }

            $.fn.validacao.CheckOn = function(){
                $('.form_element > .required, .text_input').each(function(){
                    $(this).on('blur',function(){_onBlur(this)});
                });        
    }


    };

    $.fn.validacao.Check = function(){
        var valido = true;        
        $('.fa-exclamation-circle').each(function(){
            $('#' + $(this).attr('id').substring(3)).trigger('blur');
            
            if ($(this).is(':visible')){
                valido = false;
            }
        });

        return valido;

    }

    $.fn.validacao.defaults = {
        tipo: 'vazio', // 'cpf', 'number', data || All type include the empty validation
        mensagemErro : 'Fields with an asterisk are required',
        onoff : 'on'
    };
})(jQuery);

(function ($) {
    $.fn.cad = function (options){
        var aCadMO = this;
        var settings = $.extend({}, $.fn.cad.defaults, options);
        var KDOperacao = {
            "kdInclusao" : 0,
            "kdAlteracao" : 1,
            "kdExclusao" : 2,
            "kdPesquisa" : 3
        };

        var operacaoAndamento;

        var validarFormulario = function(){
            
            $("#caduo").find('input:text, input:radio, select, input:checkbox').each(function (index, value) {
                if ($(value).attr("data-requerido"))
                {                    
                    if ($(value).is('select'))
                    {
                        alert($(value).attr('id'))
                    }
                    else if ($(value).is('input:text'))
                    {
                        alert($(value).attr('id'));
                    }else if ($(value).is('input:radio'))
                    {
                        alert($(value).attr('id'));
                    } else if ($(value).is('input:checkbox'))
                    {
                        alert($(value).attr('id'));
                    }
                }
            });
                                               
            return false;
        };

        var embcaduo =
        {                        
            AddButtonOk: function (de){
                var divButtonOk = $("<div>").addClass("avia-button-wrap avia-button-left  avia-builder-el-7  el_after_av_button  el_before_av_button ");
                var aButtonOk = $("<a>").addClass("avia-button avia-icon_select-yes avia-color-green avia-size-small avia-position-right").attr("href","#");
                var spanIconButtonOk = $("<span>").addClass("avia_button_icon fa fa-check");
                var spanTextButtonOk = $("<span>").html("Ok").addClass("avia_iconbox_title");
                
                divButtonOk.prependTo($("#caduo"));
                aButtonOk.appendTo(divButtonOk);
                spanIconButtonOk.appendTo(aButtonOk);
                spanTextButtonOk.appendTo(aButtonOk);
                
                aButtonOk.bind("click", function (event) {
                    
                    switch(operacaoAndamento) {
                        case KDOperacao.kdInclusao:                            
                            if (settings.validaFormulario == null)
                            {                            
                                if (validarFormulario())
                                    settings.actionInsert();
                            }else if (settings.validaFormulario()){                                
                                settings.actionInsert();
                            }
                                                        
                            break;
                        case KDOperacao.kdAlteracao:
                            if (settings.validaFormulario == null)
                            {                            
                                if (validarFormulario())
                                    settings.actionInsert();
                            }else if (settings.validaFormulario()){                                
                                settings.actionUpdate();
                            }

                            break;
                        case KDOperacao.kdExclusao:
                            settings.actionDelete();
                            alert("kdExclusao");
                            break;
                        case KDOperacao.kdPesquisa:
                            settings.actionSearch();
                            alert("kdPesquisa");
                            break;
                    }
                });
            }, 

            AddButtonBack: function (de){
                var divButtonBack = $("<div>").addClass("avia-button-wrap avia-button-left  avia-builder-el-7  el_after_av_button  el_before_av_button ");
                var aButtonBack = $("<a>").addClass("avia-button avia-size-small avia-position-left").attr("href","#");
                var spanIconButtonBack = $("<span>").addClass("avia_button_icon fa fa-arrow-left");

                divButtonBack.prependTo($("#caduo"));
                aButtonBack.appendTo(divButtonBack);
                spanIconButtonBack.appendTo(aButtonBack);
                
                aButtonBack.bind("click", function (event) {
                    var options = {};

                    $("#cadmo").removeAttr('style').children(':last').hide().effect("slide", options, 300);
                    $("#caduo").attr('style', 'display:none');
                });
            }
        };

        var embcadmo = 
        {
            AddButtonNew: function (de) {                
                
                var divButtonNew = $("<div>").addClass("avia-button-wrap avia-button-left  avia-builder-el-7  el_after_av_button  el_before_av_button ");
                var aButtonNew = $("<a>").addClass("avia-button avia-icon_select-yes avia-color-green avia-size-small avia-position-left").attr("href","#");
                var spanIconButtonNew = $("<span>").addClass("avia_button_icon fa fa-plus-circle");
                var spanTextButtonNew = $("<span>").html("Novo").addClass("avia_iconbox_title");

                divButtonNew.prependTo($("#cadmo"));
                aButtonNew.appendTo(divButtonNew);
                spanIconButtonNew.appendTo(aButtonNew);
                spanTextButtonNew.appendTo(aButtonNew);

                aButtonNew.bind("click", function (event) {
                    var options = {};
                    $("#cadmo").attr('style', 'display:none');
                    $("#caduo").removeAttr('style').children(':last').hide().effect("slide", options, 300);
                    operacaoAndamento = KDOperacao.kdInclusao;
                });
            },
            AddButtonSearch: function (de) {                
                
                var divButtonSearch = $("<div>").addClass("avia-button-wrap avia-button-left  avia-builder-el-7  el_after_av_button  el_before_av_button ");                                                
                var aButtonSearch = $("<a>").addClass("avia-button avia-icon_select-yes avia-color-blue avia-size-small avia-position-left");
                var spanIconButtonSearch = $("<span>").addClass("avia_button_icon fa fa-plus-circle");
                var spanTextButtonSearch = $("<span>").html("Pesquisar").addClass("avia_iconbox_title");

                divButtonSearch.prependTo($("#cadmo"));
                aButtonSearch.appendTo(divButtonSearch);
                spanIconButtonSearch.appendTo(aButtonSearch);
                spanTextButtonSearch.appendTo(aButtonSearch);
            },
            
                
        };
        embcadmo.AddButtonSearch(aCadMO);
        embcadmo.AddButtonNew(aCadMO); 
        
        embcaduo.AddButtonOk(aCadMO);
        embcaduo.AddButtonBack(aCadMO);       
    }

    $.fn.cad.defaults = {
        width: 300
    }
})(jQuery);

(function ($) {
    $.fn.caduo = function (options){
        var aCadUO = this;
    };

    $.fn.caduo.defaults = {
        width: 300
    }
})(jQuery);

(function ($) {
    $.fn.coac = function (options) {
        var divelem = this;
        var inputelem = $(":input", divelem);
        
        var settings = $.extend({}, $.fn.coac.defaults, options);

        if (!settings.extend) {
            $(inputelem).change(function () {
                $(inputelem).val("");
            });
        }

        var ahlautocomplete =
        {
            aSerialize: "",   //For the sake of performance, I did another function or a get method
            aDivElem: divelem,
            aCount: 0,
            btRemove : null,
            getInputs: function () {
                var inputs = new Array();
                var input, stAux;
                var contador = 0;

                $(divelem).find('span').each(function (index, value) {
                    if (contador == 0) {
                        stAux = $(value).html();
                        contador++;
                    }
                    else {
                        contador = 0;
                        input = new Array(2);
                        input[0] = stAux;
                        input[1] = $(value).html();
                        inputs.push(input);
                        ahlautocomplete.aSerialize += "&Id" + index + "=" + stAux + "&Valor" + index + "=" + input[1];
                    }
                });

                return inputs;
            },

            AddInput: function (de, ie) {                
                var vdiv = $("<div>").addClass("divinpac");
                var vdiv = $("<div>").addClass("");                
                vdiv.appendTo(de);
                $(ie).appendTo(vdiv);
                $(vdiv).css({ width: settings.width });
                $(ie).css({ width: settings.width });
                $(de).focus();
            },

            MergeMulti: function (inputsold, inputsnew) {
                var ajson = [];

                for (var nov = 0; nov < inputsnew.length; nov++) {
                    if (inputsnew[nov][0] == null) {
                        var clMultiac = new Object();
                        clMultiac.Entity = "MultiAC";
                        clMultiac.Id = null;
                        clMultiac.Name = inputsnew[nov][1];
                        ajson.push(clMultiac);
                    }
                    else {
                        for (var ant = 0; ant < inputsold.length; ant++) {
                            if (inputsnew[nov][0] == inputsold[ant][0])
                                break;
                        }

                        if (ant == inputsold.length) {
                            var clMultiac = new Object();
                            clMultiac.Entity = "MultiAC";
                            clMultiac.Id = inputsnew[nov][0];
                            clMultiac.Name = inputsnew[nov][1];
                            ajson.push(clMultiac);
                        }
                    }
                }

                for (var ant = 0; ant < inputsold.length; ant++) {
                    for (var nov = 0; nov < inputsnew.length; nov++) {
                        if (inputsnew[nov][0] == inputsold[ant][0])
                            break;
                    }

                    // Not found in the previous, DELETE
                    if (nov == inputsnew.length) {
                        var clMultiac = new Object();
                        clMultiac.Entity = "MultiAC";
                        clMultiac.Id = inputsold[ant][0];
                        clMultiac.Name = null;
                        ajson.push(clMultiac);
                    }
                }

                return ajson;
            },

            RemoveSelected: function () {
               if (ahlautocomplete.btRemove!=null)
                  $(ahlautocomplete.btRemove).click();
            },

            getID: function () {
                return settings.ID;
            },
            setID: function (prid, enabled) {
                settings.enabled = enabled;
                if ( (settings.elempos != null) && (settings.elempos != "") )
                {
                    $.ajax({
                        type: "POST",
                        url: settings.elempos,
                        data: "prid="+prid,
                        dataType: "json",
                        traditional: true,
                        success: function (dado) {
                            ahlautocomplete.RemoveSelected();
                            if (settings.elemselected == null)
                                ahlautocomplete.ItemSelected(dado.id, dado.name, dado.image);
                            else
                                settings.elemselected(dado.id, dado.name, dado.image);
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            alert("Alerta: coac s=" + xhr.status + " te=" + thrownError + " rt=" + xhr.responseText);
                        }
                    });
                }
            },

            forceValue: function (dado, bool) {
                settings.enabled = bool;
                if (settings.elemselected == null) {
                    ahlautocomplete.ItemSelected(dado.id, dado.name, dado.image);
                }
                else {
                    settings.elemselected(dado.id, dado.name, dado.image);
                }
            },

            forceValueAutocom: function (dadoId,name, bool) {
                settings.enabled = bool;
                if (settings.elemselected == null) {
                    ahlautocomplete.ItemSelected(dadoId, name, "");
                }
                else {
                    settings.elemselected(dadoId, name, "");
                }
            },
   
            getValue: function () {
                return settings.Value;
            },

            setParentID: function (prParentID) {
                settings.ParentID = prParentID;
            },
            getParentID: function () {
                return settings.ParentID;
            },

            setFiltroID: function (prfilterID) {                
                settings.filterID = prfilterID;
            },
            getFiltroID: function () {
                return settings.filterID;
            },

            ItemSelected: function (id, valor, imagem) {
                var vimg, vspanvalue, vspanid, vimgremove ;  
                ahlautocomplete.aCount++;
                
                settings.ID = id;

                settings.Value = valor;

                vspanvalue = $("<span>").html(valor).addClass("spanac");
                vspanvalue.css({ width: (settings.width) });
                
                vspanvalue.hide();
                vspanvalue.insertBefore(inputelem);
                
                vspanid = $("<span>").text(id);                
                vspanid.hide();
                vspanid.insertBefore(vspanvalue);

                if ((!settings.extend) || (settings.extend && settings.multi)) {
                    vspanvalue.show();
                }
                
                if(settings.enabled)
                {
                    vimgremove = $("<i>").attr({style: "float:right" ,title: settings.remove }).addClass("fa fa-times");
                    ahlautocomplete.btRemove = vimgremove;
                    $(vimgremove).css({ cursor: "blocked" });
                }
                else {
                    vimgremove = $("<i>").attr({style: "float:right" ,title: settings.remove }).addClass("");
                    ahlautocomplete.btRemove = vimgremove;
                    $(vimgremove).css({ cursor: "pointer" });
                }
                vimgremove.appendTo(vspanvalue);

                if (settings.extend && !settings.multi) {
                    $(inputelem).css({ width: (settings.width - 40) });
                    $(inputelem).css("background-color", settings.background);
                }

                if (settings.multi)
                    ahlautocomplete.AddInput(divelem, inputelem);

                if (!settings.extend && !settings.multi)
                    $(inputelem).hide();

                    vimgremove.bind("click", function (event) {
                        ahlautocomplete.aCount--;
                        settings.ID = ""; 
                        if (!settings.extend && !settings.multi) {
                            $(inputelem).val("");
                            $(inputelem).show();
                            $(inputelem).focus();
                        }
                        
                        if ((!settings.multi) || (settings.extend && settings.multi && ahlautocomplete.aCount < 0)) {
                            vspanid.remove();
                            vspanvalue.remove();
                            vimgremove.remove();
                            ahlautocomplete.btRemove = null;
                            $(inputelem).css({ width: (settings.width - 5) });
                            $(inputelem).addClass("text_input");
                            $(inputelem).val("").focus();
                            if (settings.elemremoved != null)
                                settings.elemremoved();
                        }
                        else {
                            vimgremove.parent().remove();
                        }

                        $(divelem).focus();
                        $(inputelem).focus();
                        $(inputelem).trigger('change');
                        $(this).unbind(event);
                        settings.ID = "";
                    });

                if (settings.extend) {
                    $(inputelem).bind("keypress", function (event) {
                        if (!settings.multi) {
                            vspanid.remove();
                            vspanvalue.remove();
                            vimg.remove();
                            vimgremove.remove();
                            ahlautocomplete.btRemove = null;
                            $(inputelem).css({ width: (settings.width - 5) });
                            $(inputelem).css("background-color", "white");
                            $(this).unbind(event);
                        }
                    });

                    $(inputelem).bind("keydown", function (event) {
                        if ((event.keyCode == $.ui.keyCode.ENTER) || (event.keyCode == $.ui.keyCode.TAB)) {
                            if (settings.multi && settings.extend) {
                                if ($(inputelem).val() != "")
                                    ahlautocomplete.ItemSelected("", $(inputelem).val(), "");
                            }

                            event.preventDefault();
                        }
                        else {
                            if (!settings.multi) {
                                vspanid.remove();
                                vspanvalue.remove();
                                vimg.remove();
                                vimgremove.remove();
                                ahlautocomplete.btRemove = null;
                                $(inputelem).css({ width: (settings.width - 5) });
                                $(inputelem).css("background-color", "white");
                                $(this).unbind(event);
                            }
                        }
                    });

                }

                if (!settings.extend || settings.multi)
                    return false;
                else {
                    if (!settings.multi)
                        $(inputelem).val(valor);
                }
            }
        }

        //Initialing: First element        
        ahlautocomplete.AddInput(divelem, inputelem);
        
        vjac = $(inputelem).autocomplete({            
            source: function (request, response) {
            
                if (settings.ParentID == "") {
                    
                    $.ajax({
                        url: settings.action, type: "POST", dataType: "json",
                        data: {filterID: settings.filterID, searchText: request.term, maxResults: 8, imagem: settings.image, classe: settings.classe },
                        success: function (dado) {
                            response($.map(dado, function (item) {

                                if (isNullWhiteSpaceUndefined(item.Name))
                                    item.Name = item.NAMEAUTOCOMPLETE;

                                return { id: item.id, value: item.name, label: item.image }
                            }))
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            if (coenv != KDCoEnv.Producao)
                                alert("Alerta: coac s=" + xhr.status + " te=" + thrownError + " rt=" + xhr.responseText);
                        }
                    })
                }
                else
                {
                    $.ajax({
                        url: settings.action, type: "POST", dataType: "json",
                        data: { parentid: settings.ParentID, searchText: request.term, maxResults: 8, imagem: settings.image, classe: settings.classe },
                        success: function (dado) {
                            response($.map(dado, function (item) {

                                if (isNullWhiteSpaceUndefined(item.Name))
                                    item.Name = item.NAMEAUTOCOMPLETE;

                                return { id: item.id, value: item.name, label: item.image }
                            }))
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            if (coenv != KDCoEnv.Producao)
                                alert("Alerta: coac s=" + xhr.status + " te=" + thrownError + " rt=" + xhr.responseText);
                        }
                    })
                }
            },

            select: function (e, ui) {
                settings.ID = ui.item.id; 
                if (settings.elemselected == null)
                    return ahlautocomplete.ItemSelected(ui.item.id, ui.item.value, ui.item.label);
                else
                    return settings.elemselected(ui.item.id, ui.item.value, ui.item.label);
            }

        });
        
        vjac._renderItem = function (ul, item) {

            var vardata = $("<li></li>").data("item.autocomplete", item);
            vardata.css({ width: settings.width-5, 'background-color': "#E5F7F9", cursor: "pointer" });
            
            va = $("<a>").text(item.value);
            va.addClass("textac");            

            vardata.append(va).appendTo(ul);
            
            return vardata;
        };
        
        return ahlautocomplete;

    };

    $.fn.coac.defaults = {
        width: "100%",
        extend: false,
        image: false,
        multi: false,
        elemselected: null,
        elemremoved: null,
        elempos: null, 
        background: "#f0f1f5",
        adressimage: "./admin_static/Theme/images/acelemento.png",
        adressremove: "./admin_static/Theme/images/acremove.png",
        remove: "Remove",
        classe: -1,
        action: "",
        csstext : "",
        ID: "",
        Value: "",
        ParentID: "",
        enabled: true  
    }

})(jQuery);

(function ($) {
    $.fn.coacException = function (options) {
        var divelem = this;
        var inputelem = $(":input", divelem);

        var settings = $.extend({}, $.fn.coac.defaults, options);

        if (!settings.extend) {
            $(inputelem).change(function () {
                //$(inputelem).val("");
            });
        }

        var ahlautocomplete =
        {
            aSerialize: "",   //For the sake of performance, I did another function or a get method
            aDivElem: divelem,
            aCount: 0,
            btRemove: null,
            getInputs: function () {
                var inputs = new Array();
                var input, stAux;
                var contador = 0;

                $(divelem).find('span').each(function (index, value) {
                    if (contador == 0) {
                        stAux = $(value).html();
                        contador++;
                    }
                    else {
                        contador = 0;
                        input = new Array(2);
                        input[0] = stAux;
                        input[1] = $(value).html();
                        inputs.push(input);
                        ahlautocomplete.aSerialize += "&Id" + index + "=" + stAux + "&Valor" + index + "=" + input[1];
                    }
                });

                return inputs;
            },

            AddInput: function (de, ie) {
                var vdiv = $("<div>").addClass("divinpac");
                var vdiv = $("<div>").addClass("");
                vdiv.appendTo(de);
                $(ie).appendTo(vdiv);
                $(vdiv).css({ width: settings.width });
                $(ie).css({ width: settings.width });
                $(de).focus();
            },

            MergeMulti: function (inputsold, inputsnew) {
                var ajson = [];

                for (var nov = 0; nov < inputsnew.length; nov++) {
                    if (inputsnew[nov][0] == null) {
                        var clMultiac = new Object();
                        clMultiac.Entity = "MultiAC";
                        clMultiac.Id = null;
                        clMultiac.Name = inputsnew[nov][1];
                        ajson.push(clMultiac);
                    }
                    else {
                        for (var ant = 0; ant < inputsold.length; ant++) {
                            if (inputsnew[nov][0] == inputsold[ant][0])
                                break;
                        }

                        if (ant == inputsold.length) {
                            var clMultiac = new Object();
                            clMultiac.Entity = "MultiAC";
                            clMultiac.Id = inputsnew[nov][0];
                            clMultiac.Name = inputsnew[nov][1];
                            ajson.push(clMultiac);
                        }
                    }
                }

                for (var ant = 0; ant < inputsold.length; ant++) {
                    for (var nov = 0; nov < inputsnew.length; nov++) {
                        if (inputsnew[nov][0] == inputsold[ant][0])
                            break;
                    }

                    // Not found in the previous, DELETE
                    if (nov == inputsnew.length) {
                        var clMultiac = new Object();
                        clMultiac.Entity = "MultiAC";
                        clMultiac.Id = inputsold[ant][0];
                        clMultiac.Name = null;
                        ajson.push(clMultiac);
                    }
                }

                return ajson;
            },

            RemoveSelected: function () {
                if (ahlautocomplete.btRemove != null)
                    $(ahlautocomplete.btRemove).click();
            },

            getID: function () {
                return settings.ID;
            },
            setID: function (prid, enabled) {
                settings.enabled = enabled;
                if ((settings.elempos != null) && (settings.elempos != "")) {
                    $.ajax({
                        type: "POST",
                        url: settings.elempos,
                        data: "prid=" + prid,
                        dataType: "json",
                        traditional: true,
                        success: function (dado) {
                            ahlautocomplete.RemoveSelected();
                            if (settings.elemselected == null)
                                ahlautocomplete.ItemSelected(dado.id, dado.name, dado.image);
                            else
                                settings.elemselected(dado.id, dado.name, dado.image);
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            alert("Alerta: coac s=" + xhr.status + " te=" + thrownError + " rt=" + xhr.responseText);
                        }
                    });
                }
            },

            forceValue: function (dado, bool) {
                settings.enabled = bool;
                if (settings.elemselected == null) {
                    ahlautocomplete.ItemSelected(dado.id, dado.name, dado.image);
                }
                else {
                    settings.elemselected(dado.id, dado.name, dado.image);
                }
            },

            forceValueAutocom: function (dadoId, name, bool) {
                settings.enabled = bool;
                if (settings.elemselected == null) {
                    ahlautocomplete.ItemSelected(dadoId, name, "");
                }
                else {
                    settings.elemselected(dadoId, name, "");
                }
            },

            getValue: function () {
                return settings.Value;
            },

            setParentID: function (prParentID) {
                settings.ParentID = prParentID;
            },
            getParentID: function () {
                return settings.ParentID;
            },

            setFiltroID: function (prfilterID) {
                settings.filterID = prfilterID;
            },
            getFiltroID: function () {
                return settings.filterID;
            },

            ItemSelected: function (id, valor, imagem) {
                var vimg, vspanvalue, vspanid, vimgremove;
                ahlautocomplete.aCount++;

                settings.ID = id;

                settings.Value = valor;

                vspanvalue = $("<span>").html(valor).addClass("spanac");
                vspanvalue.css({ width: (settings.width) });

                vspanvalue.hide();
                vspanvalue.insertBefore(inputelem);

                vspanid = $("<span>").text(id);
                vspanid.hide();
                vspanid.insertBefore(vspanvalue);

                if ((!settings.extend) || (settings.extend && settings.multi)) {
                    vspanvalue.show();
                }

                if (settings.enabled) {
                    vimgremove = $("<i>").attr({ style: "float:right", title: settings.remove }).addClass("fa fa-times");
                    ahlautocomplete.btRemove = vimgremove;
                    $(vimgremove).css({ cursor: "blocked" });
                }
                else {
                    vimgremove = $("<i>").attr({ style: "float:right", title: settings.remove }).addClass("");
                    ahlautocomplete.btRemove = vimgremove;
                    $(vimgremove).css({ cursor: "pointer" });
                }
                vimgremove.appendTo(vspanvalue);

                if (settings.extend && !settings.multi) {
                    $(inputelem).css({ width: (settings.width - 40) });
                    $(inputelem).css("background-color", settings.background);
                }

                if (settings.multi)
                    ahlautocomplete.AddInput(divelem, inputelem);

                if (!settings.extend && !settings.multi)
                    $(inputelem).hide();

                vimgremove.bind("click", function (event) {
                    ahlautocomplete.aCount--;
                    settings.ID = "";
                    if (!settings.extend && !settings.multi) {
                        $(inputelem).val("");
                        $(inputelem).show();
                        $(inputelem).focus();
                    }

                    if ((!settings.multi) || (settings.extend && settings.multi && ahlautocomplete.aCount < 0)) {
                        vspanid.remove();
                        vspanvalue.remove();
                        vimgremove.remove();
                        ahlautocomplete.btRemove = null;
                        $(inputelem).css({ width: (settings.width - 5) });
                        $(inputelem).addClass("text_input");
                        $(inputelem).val("").focus();
                        if (settings.elemremoved != null)
                            settings.elemremoved();
                    }
                    else {
                        vimgremove.parent().remove();
                    }

                    $(divelem).focus();
                    $(inputelem).focus();
                    $(inputelem).trigger('change');
                    $(this).unbind(event);
                    settings.ID = "";
                });

                if (settings.extend) {
                    $(inputelem).bind("keypress", function (event) {
                        if (!settings.multi) {
                            vspanid.remove();
                            vspanvalue.remove();
                            vimg.remove();
                            vimgremove.remove();
                            ahlautocomplete.btRemove = null;
                            $(inputelem).css({ width: (settings.width - 5) });
                            $(inputelem).css("background-color", "white");
                            $(this).unbind(event);
                        }
                    });

                    $(inputelem).bind("keydown", function (event) {
                        if ((event.keyCode == $.ui.keyCode.ENTER) || (event.keyCode == $.ui.keyCode.TAB)) {
                            if (settings.multi && settings.extend) {
                                if ($(inputelem).val() != "")
                                    ahlautocomplete.ItemSelected("", $(inputelem).val(), "");
                            }

                            event.preventDefault();
                        }
                        else {
                            if (!settings.multi) {
                                vspanid.remove();
                                vspanvalue.remove();
                                vimg.remove();
                                vimgremove.remove();
                                ahlautocomplete.btRemove = null;
                                $(inputelem).css({ width: (settings.width - 5) });
                                $(inputelem).css("background-color", "white");
                                $(this).unbind(event);
                            }
                        }
                    });

                }

                if (!settings.extend || settings.multi)
                    return false;
                else {
                    if (!settings.multi)
                        $(inputelem).val(valor);
                }
            }
        }

        //Initialing: First element        
        ahlautocomplete.AddInput(divelem, inputelem);

        vjac = $(inputelem).autocomplete({
            source: function (request, response) {

                if (settings.ParentID == "") {

                    $.ajax({
                        url: settings.action, type: "POST", dataType: "json",
                        data: { filterID: settings.filterID, searchText: request.term, maxResults: 8, imagem: settings.image, classe: settings.classe },
                        success: function (dado) {
                            response($.map(dado, function (item) {

                                if (isNullWhiteSpaceUndefined(item.Name))
                                    item.Name = item.NAMEAUTOCOMPLETE;

                                return { id: item.id, value: item.name, label: item.image }
                            }))
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            if (coenv != KDCoEnv.Producao)
                                alert("Alerta: coac s=" + xhr.status + " te=" + thrownError + " rt=" + xhr.responseText);
                        }
                    })
                }
                else {
                    $.ajax({
                        url: settings.action, type: "POST", dataType: "json",
                        data: { parentid: settings.ParentID, searchText: request.term, maxResults: 8, imagem: settings.image, classe: settings.classe },
                        success: function (dado) {
                            response($.map(dado, function (item) {

                                if (isNullWhiteSpaceUndefined(item.Name))
                                    item.Name = item.NAMEAUTOCOMPLETE;

                                return { id: item.id, value: item.name, label: item.image }
                            }))
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            if (coenv != KDCoEnv.Producao)
                                alert("Alerta: coac s=" + xhr.status + " te=" + thrownError + " rt=" + xhr.responseText);
                        }
                    })
                }
            },

            select: function (e, ui) {
                settings.ID = ui.item.id;
                if (settings.elemselected == null)
                    return ahlautocomplete.ItemSelected(ui.item.id, ui.item.value, ui.item.label);
                else
                    return settings.elemselected(ui.item.id, ui.item.value, ui.item.label);
            }

        });

        vjac._renderItem = function (ul, item) {

            var vardata = $("<li></li>").data("item.autocomplete", item);
            vardata.css({ width: settings.width - 5, 'background-color': "#E5F7F9", cursor: "pointer" });

            va = $("<a>").text(item.value);
            va.addClass("textac");

            vardata.append(va).appendTo(ul);

            return vardata;
        };

        return ahlautocomplete;

    };

    $.fn.coac.defaults = {
        width: "100%",
        extend: false,
        image: false,
        multi: false,
        elemselected: null,
        elemremoved: null,
        elempos: null,
        background: "#f0f1f5",
        adressimage: "./admin_static/Theme/images/acelemento.png",
        adressremove: "./admin_static/Theme/images/acremove.png",
        remove: "Remove",
        classe: -1,
        action: "",
        csstext: "",
        ID: "",
        Value: "",
        ParentID: "",
        enabled: true
    }

})(jQuery);

(function ($) {
    $.fn.comenu = function (options) {
        var settings = $.extend({}, $.fn.comenu.defaults, options);

        $('.menu-drag li a').click(function (event) {
            $('.menu-drag li').removeClass('menu-dragselected');
            $(this).parent().addClass('menu-dragselected');
        });

        $(this).find('li:not(.submenu-drag)').click(function () {
            $(".submenu-drag").hide();
            $("." + $(this).attr("nome")).show();
        });

        $(".menu-drag li a").hover(
          function () {
              $(this).parent().addClass("menu-draghover");
          },
          function () {
              $(this).parent().removeClass("menu-draghover");
          }
        );

    }

    $.fn.comenu.defaults = {
    }

})(jQuery);

(function ($) {
    $.fn.comenupop = function (options) {
        var ulmenu = this;
        var settings = $.extend({}, $.fn.comenupop.defaults, options);

        $(window).resize(function () {
            comenupop_.closeOpenedMenu();
        });

        var comenupop_ =
        {
            closeOpenedMenu: function () {
                $('.' + settings.fixedClass).remove();
                $('li.' + settings.fixedSelectedClass).removeClass(settings.selectedClass);
            },

            Iniciar: function () {
                ulmenu.children('li').click(function (event) {
                    comenupop_.closeOpenedMenu();
                    event.stopImmediatePropagation(); //Stop event propagation

                    var submenu = $(this).children('ul').clone();
                    submenu.addClass(settings.submenuClass); //For remove system
                    submenu.addClass(settings.fixedClass);
                    $('body').append(submenu);

                    var li = $(this);
                    if (submenu.size() > 0) {
                        li.addClass(settings.selectedClass);
                        li.addClass(settings.fixedSelectedClass);

                        var left = li.offset().left;
                        if (!settings.left) {
                            left += li.outerWidth() - submenu.outerWidth();
                        } else {
                        }

                        var top = li.offset().top;

                        submenu.attr(
					  'style',
					  'position: absolute;' +
					  'z-index: ' + settings.zIndex + ';' +
					  'left: ' + left + 'px; ' +
					  'top: ' + (top + li.outerHeight()) + 'px;'
				   );

                        $('body').click(function () {
                            comenupop_.closeOpenedMenu();
                            $('body').unbind('click');
                        });
                    }
                });

            }

        }

        comenupop_.Iniciar();

        return comenupop_;
    }

    $.fn.comenupop.defaults = {
        submenuClass: 'ahl-simple-class',
        selectedClass: 'selected',
        left: false,
        zIndex: 1000,
        fixedClass: 'ahl-class-fixed',
        fixedSelectedClass: 'ahl-selected-class-fixed'
    }

})(jQuery);

(function ($) {
    $.fn.coaba = function (options) {
        var settings = $.extend({}, $.fn.coaba.defaults, options);

        $(".conteudo-aba").hide();
        $("ul.menu-aba li:first").addClass("active").show();
        $(".conteudo-aba:first").show("slow");
        $("ul.menu-aba li").click(function () {
            $('ul.menu-aba li').removeClass('active')
            $(this).addClass('active');
            $('.conteudo-aba').hide();
            var activeaba = $(this).find('a').attr('href');
            $(activeaba).fadeIn("slow");

            return false;
        });

    }

    $.fn.coaba.defaults = {
    }

})(jQuery);


(function ($) {
    $.fn.cogridselect = function (options) {
        var elem = this;
        var settings = $.extend({}, $.fn.cogridselect.defaults, options);
        
        if (settings.container=="" || settings.evento !=null ) {  //multi            
            
            if (settings.evento == null)
            {                
                if ($(elem).hasClass(settings.classname))
                    $(elem).removeClass(settings.classname);
                else
                    $(elem).addClass(settings.classname);
            }
            else
            {                                     
                if (settings.container=="" || settings.linha=="")
                   alert("GridSelected não pode ser multi com container nulo ou linha nulo se evento<>null");
                else
                {
                    $("."+settings.container).find("."+settings.linha).each(function () {                                                                                            
                        // tirei o item do parametro da function e usei o this
                        if ($(this).hasClass(settings.classname))
                        {                            
                            if (settings.evento.altKey)   //desmarcar tudo                                                                                                          
                               $(this).removeClass(settings.classname);                               
                        }
                        else
                        {                            
                            if (settings.evento.ctrlKey)  //marcar tudo
                               $(this).addClass(settings.classname);
                        }

                    });
                }
            }
        }
        else
        {   //unico            
            $("."+settings.container).find("."+settings.classname).each(function () {
                $(this).removeClass(settings.classname);
            });

            $(elem).addClass(settings.classname);
        }

    }

    $.fn.cogridselect.defaults = {
       evento: null,
       container: "",
       linha: "",
       classname: "tr-selec-grdview"
    }

})(jQuery);

(function ($) {
    $.fn.cogridclean = function (options) {
        var elem = this;
        var settings = $.extend({}, $.fn.cogridclean.defaults, options);

        $(elem).each(function () {
            $(this).remove();
        });
    }
    $.fn.cogridclean.defaults = {
        container: ""
    }
})(jQuery);

(function($) {

	//var pasteEventName = ($.browser.msie ? 'paste' : 'input') + ".mask";
	var iPhone = (window.orientation != undefined);

	$.mask = {
		//Predefined character definitions
		definitions: {
			'9': "[0-9]",
			'a': "[A-Za-z]",
			'*': "[A-Za-z0-9]"
		},
		dataName:"rawMaskFn"
	};

	$.fn.extend({
		//Helper Function for Caret positioning
		caret: function(begin, end) {
			if (this.length == 0) return;
			if (typeof begin == 'number') {
				end = (typeof end == 'number') ? end : begin;
				return this.each(function() {
					if (this.setSelectionRange) {
						this.setSelectionRange(begin, end);
					} else if (this.createTextRange) {
						var range = this.createTextRange();
						range.collapse(true);
						range.moveEnd('character', end);
						range.moveStart('character', begin);
						range.select();
					}
				});
			} else {
				if (this[0].setSelectionRange) {
					begin = this[0].selectionStart;
					end = this[0].selectionEnd;
				} else if (document.selection && document.selection.createRange) {
					var range = document.selection.createRange();
					begin = 0 - range.duplicate().moveStart('character', -100000);
					end = begin + range.text.length;
				}
				return { begin: begin, end: end };
			}
		},
		unmask: function() { return this.trigger("unmask"); },
		mask: function(mask, settings) {
			if (!mask && this.length > 0) {
				var input = $(this[0]);
				return input.data($.mask.dataName)();
			}
			settings = $.extend({
				placeholder: "_",
				completed: null
			}, settings);

			var defs = $.mask.definitions;
			var tests = [];
			var partialPosition = mask.length;
			var firstNonMaskPos = null;
			var len = mask.length;

			$.each(mask.split(""), function(i, c) {
				if (c == '?') {
					len--;
					partialPosition = i;
				} else if (defs[c]) {
					tests.push(new RegExp(defs[c]));
					if(firstNonMaskPos==null)
						firstNonMaskPos =  tests.length - 1;
				} else {
					tests.push(null);
				}
			});

			return this.trigger("unmask").each(function() {
				var input = $(this);
				var buffer = $.map(mask.split(""), function(c, i) { if (c != '?') return defs[c] ? settings.placeholder : c });
				var focusText = input.val();

				function seekNext(pos) {
					while (++pos <= len && !tests[pos]);
					return pos;
				};
				function seekPrev(pos) {
					while (--pos >= 0 && !tests[pos]);
					return pos;
				};

				function shiftL(begin,end) {
					if(begin<0)
					   return;
					for (var i = begin,j = seekNext(end); i < len; i++) {
						if (tests[i]) {
							if (j < len && tests[i].test(buffer[j])) {
								buffer[i] = buffer[j];
								buffer[j] = settings.placeholder;
							} else
								break;
							j = seekNext(j);
						}
					}
					writeBuffer();
					input.caret(Math.max(firstNonMaskPos, begin));
				};

				function shiftR(pos) {
					for (var i = pos, c = settings.placeholder; i < len; i++) {
						if (tests[i]) {
							var j = seekNext(i);
							var t = buffer[i];
							buffer[i] = c;
							if (j < len && tests[j].test(t))
								c = t;
							else
								break;
						}
					}
				};

				function keydownEvent(e) {
					var k=e.which;

					//backspace, delete, and escape get special treatment
					if(k == 8 || k == 46 || (iPhone && k == 127)){
						var pos = input.caret(),
							begin = pos.begin,
							end = pos.end;
						
						if(end-begin==0){
							begin=k!=46?seekPrev(begin):(end=seekNext(begin-1));
							end=k==46?seekNext(end):end;
						}
						clearBuffer(begin, end);
						shiftL(begin,end-1);

						return false;
					} else if (k == 27) {//escape
						input.val(focusText);
						input.caret(0, checkVal());
						return false;
					}
				};

				function keypressEvent(e) {
					var k = e.which,
						pos = input.caret();
					if (e.ctrlKey || e.altKey || e.metaKey || k<32) {//Ignore
						return true;
					} else if (k) {
						if(pos.end-pos.begin!=0){
							clearBuffer(pos.begin, pos.end);
							shiftL(pos.begin, pos.end-1);
						}

						var p = seekNext(pos.begin - 1);
						if (p < len) {
							var c = String.fromCharCode(k);
							if (tests[p].test(c)) {
								shiftR(p);
								buffer[p] = c;
								writeBuffer();
								var next = seekNext(p);
								input.caret(next);
								if (settings.completed && next >= len)
									settings.completed.call(input);
							}
						}
						return false;
					}
				};

				function clearBuffer(start, end) {
					for (var i = start; i < end && i < len; i++) {
						if (tests[i])
							buffer[i] = settings.placeholder;
					}
				};

				function writeBuffer() { return input.val(buffer.join('')).val(); };

				function checkVal(allow) {
					//try to place characters where they belong
					var test = input.val();
					var lastMatch = -1;
					for (var i = 0, pos = 0; i < len; i++) {
						if (tests[i]) {
							buffer[i] = settings.placeholder;
							while (pos++ < test.length) {
								var c = test.charAt(pos - 1);
								if (tests[i].test(c)) {
									buffer[i] = c;
									lastMatch = i;
									break;
								}
							}
							if (pos > test.length)
								break;
						} else if (buffer[i] == test.charAt(pos) && i!=partialPosition) {
							pos++;
							lastMatch = i;
						}
					}
					if (!allow && lastMatch + 1 < partialPosition) {
						input.val("");
						clearBuffer(0, len);
					} else if (allow || lastMatch + 1 >= partialPosition) {
						writeBuffer();
						if (!allow) input.val(input.val().substring(0, lastMatch + 1));
					}
					return (partialPosition ? i : firstNonMaskPos);
				};

				input.data($.mask.dataName,function(){
					return $.map(buffer, function(c, i) {
						return tests[i]&&c!=settings.placeholder ? c : null;
					}).join('');
				})

				if (!input.attr("readonly"))
					input
					.one("unmask", function() {
						input
							.unbind(".mask")
							.removeData($.mask.dataName);
					})
					.bind("focus.mask", function() {
						focusText = input.val();
						var pos = checkVal();
						writeBuffer();
						var moveCaret=function(){
							if (pos == mask.length)
								input.caret(0, pos);
							else
								input.caret(pos);
						};
						($.browser.msie ? moveCaret:function(){setTimeout(moveCaret,0)})();
					})
					.bind("blur.mask", function() {
						checkVal();
						if (input.val() != focusText)
							input.change();
					})
					.bind("keydown.mask", keydownEvent)
					.bind("keypress.mask", keypressEvent)
//					.bind(pasteEventName, function() {
//						setTimeout(function() { input.caret(checkVal(true)); }, 0);
//					});

				checkVal(); //Perform initial check for existing values
			});
		}
	});
})(jQuery);

(function ($) {
    $.fn.coremovedivhint = function (options) {
        var elem = this;
        var settings = $.extend({}, $.fn.coremovedivhint.defaults, options);
        if (settings.slideup == true) {
            $('#' + settings.nomediv).slideUp('fast').show();
        }
        else {
			$('#' + settings.nomediv).remove();
        }
    }
    $.fn.coremovedivhint.defaults = {
        nomediv: "divHint",
        slideup : false
    }
})(jQuery);

(function ($) {
    $.fn.comessagebox = function (options) {
        var elem = this;
        var settings = $.extend({}, $.fn.comessagebox.defaults, options);
		var divelem = '#'+settings.nomediv;
        var lDiv = "<div id='"+settings.nomediv+"'></div>";
        var ExisteDiv = $('body').find('#'+settings.nomediv).length > 0 ? true : false;

        if (!ExisteDiv) { $('body').append(lDiv); }

        if ( settings.closebutton == true ) {
            var dHint = "<table border='0' width='100%'><tr><td width='97%'>" + settings.mensagem + "</td><td width='3%' ><img  style='float:right;border-radius:2px;padding: 1px;margin: 0 auto;position: relative;top: "+settings.topclosebutton+";' align='left' id='imgcomessagebox' src="+settings.closebuttonimage+" /></td></tr></table>";
        }
        else {
            var dHint = "<table border='0' width='100%'><tr><td width='97%'>" + settings.mensagem + "</td><td width='3%'></td></tr></table>";
        }

        if ( settings.hintcomponta == true ) {
            var dHint = "<img  style='float:left;border:none;margin:-19px -20px 0 0;margin-left:5px;background:transparent;' align='left' id='imgcomessagebox' src="+settings.pontaimage+" /><table border='0' width='100%'><tr><td width='97%'>" + settings.mensagem + "</td><td width='3%'></td></tr></table>";
        }
        
		var lcomessagebox =
        {
            close: function () {
                if (settings.effect == 'slidedown') {
                    $(divelem).slideUp(settings.speed).show();
                } else if (settings.effect == 'fadeout') {
                    $(divelem).fadeOut('slow');
                } else {
    	            $(divelem).remove();
                }

		        if (settings.closewindowclick == true) {  $(window).unbind('click'); }
        		if (settings.closemouseleave == true) { $(elem).unbind('click');  }

            },
            createcomessagebox: function () {

                $(divelem).html(dHint);

                if ( settings.closebutton == true ) {
                    $(divelem).find("#imgcomessagebox").css({ cursor: "pointer" });
                    $(divelem).find('#imgcomessagebox').click(function (event) {
                        lcomessagebox.close();
                    });
                }

                $(divelem).css('position', 'absolute');
                $(divelem).css('z-index', '9999');
                $(divelem).css('display', 'none');
                $(divelem).css({ top: settings.posicaoY });
                $(divelem).css({ left: settings.posicaoX });
                $(divelem).css({ width: settings.width });
                $(divelem).css({ height: settings.height });
                $(divelem).css('font-family', settings.font);
                $(divelem).css('font-size', settings.fontsize);
                $(divelem).css({ color: settings.colorfont });
                $(divelem).css('font-weight',settings.fontweight);
                $(divelem).css({ padding: settings.padding });
                $(divelem).css('background-color', settings.backgroundcolor);

                if (settings.background != "")
                    $(divelem).css('background',settings.background);

                if (settings.borderradius != "")
                    $(divelem).css('border-radius',settings.borderradius);

                $(divelem).css('text-align',settings.textalign);

                if (settings.boxshadow != "")
                    $(divelem).css('box-shadow', settings.boxshadow);

                if (settings.border != "")
                    $(divelem).css({ border: settings.border });

            },
            closewindowclick: function (){
                $(window).click(function (e) {
                    lcomessagebox.close();
                });
            },
            closemouseleave: function (){
               $(elem).mouseleave(function (e) {
                    lcomessagebox.close();
               });
            }
        }	

        lcomessagebox.createcomessagebox();

        if ((settings.temporario == false) && (settings.effect != 'slidedown'))
            $(divelem).fadeIn(10);

        if ((settings.temporario == true) || (settings.effect == 'slidedown')) {
            if (settings.effect == 'slidedown') {
                $(divelem).slideDown(settings.speed).show();
            }else {
                $(divelem).fadeIn('slow').delay(settings.delay).fadeOut('slow');
           }
        }
		if (settings.closewindowclick == true) {  lcomessagebox.closewindowclick(); }
		if (settings.closemouseleave == true) { lcomessagebox.closemouseleave();   }
        
        return lcomessagebox;

    }
    $.fn.comessagebox.defaults = {
        posicaoX: 0,
        posicaoY: 0,
        nomediv: "divHint",
        mensagem: "",
        width: "169px",
        height: "70px",
		font: "verdana",
		fontsize: "10px",
		colorfont: "#333",
		fontweight: "bold",
		padding: "5px 0 5px 1px",
		backgroundcolor: "#d7e7f6",
		borderradius: "4px",
		textalign: "center",
        border : "1px solid #8091a1",
		boxshadow: "1px 1px 13px  #696969",
        temporario: false,
        transparencia: 1,
        closebutton : false,
        closebuttonimage : "./admin_static/Theme/Image/acremove.png", 
        pontaimage : "./admin_static/Theme/Image/icon-ponta.png", 
        topclosebutton : '-7px',
        delay : 4000,
        effect : 'nomal',
        speed : 'fast',
        closewindowclick : false,
        closemouseleave : false,
        background : "",
        hintcomponta : false
    }
})(jQuery);

//Replace All --> varstring.ReplaceAll(".","")
String.prototype.ReplaceAll = function (stringToFind, stringToReplace) {
        var temp = this;
        var index = temp.indexOf(stringToFind);
        while (index != -1) {
            temp = temp.replace(stringToFind, stringToReplace);
            index = temp.indexOf(stringToFind);
        }
        return temp;
    };

(function ($) {
    $.fn.coerrorbox = function (options) {
        var elem = this;
        var settings = $.extend({}, $.fn.coerrorbox.defaults, options);

        var dError = "<article class='box-error'><div class='validation-summary-errors'><span><p>" + settings.Mensagem + "</p></span></div></article>";
        
        var ExisteDiv = $('body').find('.box-error').length > 0 ? true : false;
        if (!ExisteDiv)
        {
            $("." + settings.classe ).prepend(dError);                        
        }
        else
        {
            $(".box-error").remove();
            $("." + settings.classe ).prepend(dError);
        }
		        
        $(".box-error").css('margin', 10,0,0,0);
        		                        
        $(".box-error").fadeIn("slow", 10);

        $(".box-error").fadeTo("slow", 10); 
        
        $(".box-error").fadeIn('slow').delay(3000).fadeOut('slow');

    }
    $.fn.coerrorbox.defaults = {
        classe: "nopError",
        Mensagem: ""
    }
})(jQuery);

(function ($) {    
    $.fn.coinformationbox = function (options) {
        var elem = this;
        var settings = $.extend({}, $.fn.coinformationbox.defaults, options);        
        var dError = "<article class='box-information'><div class='validation-summary-information'><span>" + settings.Mensagem + "</span></div></article>";
        
        var ExisteDiv = $('body').find('.box-information').length > 0 ? true : false;
        if (!ExisteDiv)
        {
            $("." + settings.classe ).prepend(dError);                        
        }
        else
        {
            $(".box-information").remove();
            $("." + settings.classe ).prepend(dError);
        }
		        
        $(".box-information").css('margin', 10,0,0,0);
        		                        
        $(".box-information").fadeIn("slow", 10);

        $(".box-information").fadeTo("slow", 10); 
        
        $(".box-information").fadeIn('slow').delay(3000).fadeOut('slow');        

    }
    $.fn.coinformationbox.defaults = {
        classe: "nopInformation",
        Mensagem: ""
    }
})(jQuery);

(function ($) {    
    $.fn.cowarningbox = function (options) {
        var elem = this;
        var settings = $.extend({}, $.fn.cowarningbox.defaults, options);

        var dError = "<article class='box-warning'><div class='validation-summary-warning'><span>" + settings.Mensagem + "</span></div></article>";
        
        var ExisteDiv = $('body').find('.box-warning').length > 0 ? true : false;
        if (!ExisteDiv)
        {
            $("." + settings.classe ).prepend(dError);                        
        }
        else
        {
            $(".box-warning").remove();
            $("." + settings.classe ).prepend(dError);
        }

        $(".box-warning").css('margin', 10,0,0,0);
        		                        
        $(".box-warning").fadeIn("slow", 10);

        $(".box-warning").fadeTo("slow", 10); 
        
        $(".box-warning").fadeIn('slow').delay(3000).fadeOut('slow');

    }
    $.fn.cowarningbox.defaults = {
        classe: "nopWarning",
        Mensagem: ""
    }
})(jQuery);

(function ($) {
    $.fn.cosplashscreenboxold = function (options) {
        var elem = this;
		var Sessao = "";
        var settings = $.extend({}, $.fn.cosplashscreenboxold.defaults, options);

        var ExisteDiv = $('body').find('#'+settings.nomediv).length > 0 ? true : false;
        if (!ExisteDiv)
        {
            var lDiv = "<div id='"+settings.nomediv+"'></div>";
            $('body').append(lDiv);
        }

		Sessao += "<section id='idSplashScreenModal' class='box-dialog' title='Splash Screen' style='display:none;height:auto;width:auto' class='corpo-dialog'>"
		Sessao += "<div id='div-idSplashScreenModal' width='10' style='border:1px solid #84a2a6;background-color:#84a2a6;box-shadow:4px 4px 13px  #696969;border-radius:4px'>"
		Sessao += "<section class='idSplashScreenModal2' id='idSplashScreenModal2' width='10' style='height:auto;'>"
		Sessao += "<table width='100%' border='0' cellspacing='0' cellpadding='0'>"
		Sessao += "<tr>"
		Sessao += "<td width='20%' align='left' valign='top'>"
		Sessao += "<i class='fa fa-circle-o-notch fa-spin fa-3x fa-fw margin-bottom'></i>"
		Sessao += "</td>"
		Sessao += "<td>"
		Sessao += "<label style='color:#F5F5F5;'>&nbsp;&nbsp;Waite...</label></td>"
		Sessao += "</tr>"
		Sessao += "</table>"
		Sessao += "</section>"
		Sessao += "</div>"
		Sessao += "</section>"

		var divelem = '#'+settings.nomediv;
        
        $(divelem).html(Sessao);
		
		$("#idSplashScreenModal").dialog({
			modal: true,
			width: 10,
			closeOnEscape: false,
			dialogClass: 'dialog-transparent',
			resizable: false
		});

		$(".ui-dialog-titlebar").hide();
			
    }
    $.fn.cosplashscreenboxold.defaults = {
        nomediv: "divCoSplashScreenBox"
    }
})(jQuery);

(function ($) {
    $.fn.coremovesplashscreenboxold = function (options) {
        var elem = this;
        var settings = $.extend({}, $.fn.coremovesplashscreenboxold.defaults, options);
        
        if (!settings.modal)
        {
            $(".ui-dialog-titlebar").show();        
        }

        var ExisteDiv = $('body').find('#'+settings.nomediv).length > 0 ? true : false;
        if (ExisteDiv)
        {
            $("#idSplashScreenModal").dialog("close");
			$('#' + settings.nomediv).remove();
        }
    }
    $.fn.coremovesplashscreenboxold.defaults = {
        nomediv: "divCoSplashScreenBox",
        modal: false
    }
})(jQuery);

(function ($) {
    $.fn.somentenumeros = function (options) {
        var elem = this;
        var settings = $.extend({}, $.fn.somentenumeros.defaults, options);
        
		$(elem).bind("keypress", function (event) {

			setTimeout(function () {
				var text = $(elem).val();
				elem.val( text.replace(/\D/g, '') );
			}, 100);
		
		});
					
    }
    $.fn.somentenumeros.defaults = {
    }
})(jQuery);

(function ($) {
    $.fn.cosplashscreenbox = function (options) {
        var elem = this;
        var settings = $.extend({}, $.fn.cosplashscreenbox.defaults, options);
		var lSection = settings.wsName;
        
        var DivMask = "divMask"+settings.wsName;
		var ExisteDivMask = $('body').find("#"+DivMask).length > 0 ? true : false;
        if ( (!ExisteDivMask) ) {
			var lDiv = "<div id='"+DivMask+"' style='position:absolute;left:0;top:0;background-color:#000;display:none;'></div>";
            $('body').append(lDiv);
            $('#'+DivMask).css({ 'z-index': 999998 });

            $(window).resize(function() {
                if ( $('body').find("#"+DivMask).length > 0) {
                    $('#'+DivMask).css({ 'width': $(window).width(), 'height': $(document).height() });
                }
                lws.centraliza();
            });
        }
        $('#'+DivMask).css({ 'width': $(window).width(), 'height': $(document).height() });
		
		var ExisteDiv = $('body').find("#"+lSection).length > 0 ? true : false;

        if ( (!ExisteDiv) ) {
			var lDiv = "<section id='"+lSection+"'  style='display:none' class='corpo-dialog'></section>";
			$('body').append(lDiv);
        }
		
		var Sessao = "";
		Sessao += "<div width='auto' style='border:1px solid #84a2a6;background-color:#84a2a6;box-shadow:4px 4px 13px #696969;border-radius:4px'>";
		Sessao += "	<section class='idSplashScreenModal2' id='idSplashScreenModal2' width='10' style='height:auto;'>";
		Sessao += "		<table width='100%' border='0' cellspacing='0' cellpadding='0'>";
		Sessao += "			<tr>";
		Sessao += "				<td width='20%' align='left' valign='top'>";
		Sessao += "				<i class='fa fa-circle-o-notch fa-spin fa-3x fa-fw margin-bottom'></i>";
		Sessao += "			</td>";
		Sessao += "			<td>";
		Sessao += "				<label style='color:#F5F5F5;'>&nbsp;&nbsp;Aguarde...</label></td>";
		Sessao += "			</tr>";
		Sessao += "		</table>";
		Sessao += "	</section>";
		Sessao += "</div>";
		
        var lws =
        {
            centraliza: function () {
                $('#'+lSection).css({ 'top': 0, 'left': 0 });

		        var winH = $(window).height();
		        var winW = $(window).width();

		        var posicionY = (winH / 2 - $('#'+lSection).height() / 2);
		        var posicionX = (winW / 2 - $('#'+lSection).width() / 2);

                posicionY = posicionY+$(window).scrollTop();
                posicionX = posicionX+$(window).scrollLeft();

                if (posicionY < 0) { posicionY=10; }
                if (posicionX < 0) { posicionX=10; }

                $('#'+lSection).css({ 'top': posicionY, 'left': posicionX });

            }
        }

		$("#"+lSection).append(Sessao);
        $('#'+lSection).css({ 'z-index': 999999 });
        $('#'+lSection).css({ 'position': 'absolute' });
        $('#'+lSection).css({ 'display': 'none' });
        $('#'+lSection).css({ 'padding': '0px' });
		lws.centraliza();
        $('#'+DivMask).fadeTo("fast", 0.2);
        $("#"+lSection).show();
    }
    $.fn.cosplashscreenbox.defaults = {
		wsName: "formsplashscreen"
    }
})(jQuery);

(function ($) {
    $.fn.coremovesplashscreenbox = function (options) {
        var elem = this;
        var settings = $.extend({}, $.fn.coremovesplashscreenbox.defaults, options);
        
        var DivMask = "divMask"+settings.wsName;
		var lSection = settings.wsName;

        var ExisteDiv = $('body').find('#'+DivMask).length > 0 ? true : false;
        if (ExisteDiv) { $('#' + DivMask).remove(); }

        var ExisteDiv = $('body').find('#'+lSection).length > 0 ? true : false;
        if (ExisteDiv) { $('#' + lSection).remove(); }
    }
    $.fn.coremovesplashscreenbox.defaults = {
		wsName: "formsplashscreen"
    }
})(jQuery);

(function ($) {
    $.fn.cogettwiter = function (options) {
        var elem = this;
        var settings = $.extend({}, $.fn.cogettwiter.defaults, options);

        var cogettwiter_ =
        {
             aTwiteres: "",
             callService : function () {
                $.ajax({
                    type: "GET",
                    url: "http://api.twitter.com/1/statuses/user_timeline/"+settings.conta+".json",
                    data: "callback=twitterCallback2&count="+settings.count+"&exclude_replies=true",
                    dataType: "jsonp",
                    traditional: true,
                    success: function (data) {
                        settings.atwiter = data;
				        cogettwiter_.retorndata(data);
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
				        //alert("Erro");
                    }
                });
            },
            retorndata : function (data) {
                settings.cbhSucess(settings.atwiter[0]);
            },
            nexttwitter : function () {
                settings.postition = settings.postition + 1;
                if ( parseInt(settings.postition)  >= parseInt(settings.atwiter.length)) { settings.postition = settings.atwiter.length-1; }
                settings.cbhSucess(settings.atwiter[settings.postition]);
            },
            oldtwitter : function () {
                settings.postition = settings.postition - 1;
                if ( parseInt(settings.postition)  < 1) { settings.postition = 0 }
                settings.cbhSucess(settings.atwiter[settings.postition]);
            }
        }
        
        cogettwiter_.callService();

        return cogettwiter_;
	}

    $.fn.cogettwiter.defaults = {
		cbhSucess: "cbhSucess",
        conta: "embraport",
        count: 50,
        postition: 0,
        atwiter: ""
    }
})(jQuery);

// Function to find a link within a string
function URLInString(inputText) {
    var replacedText, replacePattern1, replacePattern2, replacePattern3;

    //URLs starting with http://, https://, or ftp://
    replacePattern1 = /(\b(https?|ftp):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/gim;
    replacedText = inputText.replace(replacePattern1, '<a href="$1" target="_blank">$1</a>');

    //URLs starting with "www." (without // before it, or it'd re-link the ones done above).
    replacePattern2 = /(^|[^\/])(www\.[\S]+(\b|$))/gim;
    replacedText = replacedText.replace(replacePattern2, '$1<a href="http://$2" target="_blank">$2</a>');

    //Change email addresses to mailto:: links.
    replacePattern3 = /(\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,6})/gim;
    replacedText = replacedText.replace(replacePattern3, '<a href="mailto:$1">$1</a>');

    return replacedText;
}

// Function to sort one Json for a given field
function Order(prData, prPor)
{        
    var i, j = 0;
    var lLength = prData.length;
        
    for(i = 0; i < lLength; i++)
    {
        for(j = 0; j < lLength; j++)
        {
            if(prData[i][prPor] < prData[j][prPor])
            {
                temp = prData[i];
                prData[i] = prData[j];
                prData[j] = temp;
            }
        }
    }        
    return prData;
}

(function($) {
    $.fn.coupload_old = function(options) {
        var settings = {
          params: {},
          action: '',
          beforeUpload: null,
          afterUpload: null,
          onCancel: null,
          cbhError: null,
          clsMensagemInformation: null,
          clsMensagem: null,
          validate_extensions : true,
          valid_extensions : ['gif','png','jpg','jpeg','pdf','csv'],
          max_size: 5000, //Em KB (def = 5 Mb)
          qtd_limite_upload: 1,
          splashscreen: true,
          submit_button : null
        };

        var uploading_file = false;
        var div_id = 'div_' + Math.round(new Date().getTime() / 1000)
        var frame_id = 'coupload_old-iframe-' + Math.round(new Date().getTime() / 1000);

        if (settings.submit_button == null) {
            $(this).click();
        }

        if ( options ) { 
          $.extend( settings, options );
        }
        
        return this.each(function() {
          var $element = $(this);
                              
          if($element.data('coupload_old-setup') === true) return;

          $element.change(function()
          {
            uploading_file = false;            
            if (settings.submit_button == null)
            {
              upload_file();
            }
          });
          
          if (settings.submit_button != null){            
            settings.submit_button.click(function()
            {
              if (!uploading_file)
              {
                 upload_file();
              }
            });
          }

          var upload_file = function()
          {

            if (settings.splashscreen == true)
                $("").cosplashscreenbox({});

            var execute = true;        
            var lMessage = null;
            // Validates that has file to send
            if($element.val() == '') 
            {
                if (settings.splashscreen){
                    $("").coremovesplashscreenbox();
                }
                execute = false;
                lMessage = "The upload file was not found.";
            }
            
            if (lMessage == null)
            {
                var lId = $element.attr('id');
                var inp = document.getElementById(lId);            

                if (settings.qtd_limite_upload != null && inp.files.length > settings.qtd_limite_upload)
                {
                    lMessage = "The amount of files selected is invalid. The maximum amount is [ " + settings.qtd_limite_upload + " ]";
                }
                else
                {
                    for (var i = 0; i < inp.files.length; ++i) {
                        var lnome = inp.files.item(i).name;
                        var ltamanho = inp.files.item(i).size;
                        var lextensao = inp.files.item(i).name.split('.').pop().toLowerCase();      
                    
                        if (settings.max_size != null){
                            if (ltamanho > (settings.max_size * 1000)) { //Tamanho em bytes
                                lMessage = "The File [" + lnome + "] is greater than the allowable limit [" + settings.max_size + " Kb]";
                            }
                        }
                    
                        if (lMessage == null && settings.validate_extensions && $.inArray(lextensao, settings.valid_extensions) == -1)
                        {                              
                            var lMessage = "The file extension [" + lnome + "] is not valid. Valid extensions: [" + settings.valid_extensions.join(', ') + "].";                           
                        }     
                    
                        if (lMessage != null) break;                
                    }
                }
            }

            if (lMessage != null){
                execute = false;
                
                if (settings.clsMensagem != null){
                    $("").coerrorbox({
                            Mensagem: lMessage,
                            classe: settings.clsMensagemInformation
                        });
                }
                else{
                    alert(lMessage);
                }
                
                if (settings.onCancel != null){
                    return settings.onCancel.apply($element, [settings.params]);
                }

                return null;
            }            
        
            if (execute){
              uploading_file = true;
              montarFrame($element);
                            
              if (settings.beforeUpload != null){
                execute = settings.beforeUpload.apply($element, [settings.params]);
              }
                            
              if(execute !== false){
                $element.parent('form').submit(function(e) { e.stopPropagation(); }).submit();
              }
            }
            else{
                if (settings.splashscreen){
                    $("").coremovesplashscreenbox();
                }
            }
          };

          $element.data('coupload_old-setup', true);

          var handleResponse = function(loadedFrame, element) {
                var response, responseStr = loadedFrame.contentWindow.document.body.innerHTML;
                var ldocument = loadedFrame.contentWindow.document;
                var lpreTag = $(ldocument).find("pre");
                
                if (lpreTag[0] != null && lpreTag[0] != undefined){
                    response = lpreTag[0].innerHTML;
                }

                element.siblings().remove();
                element.unwrap();

                uploading_file = false;

                var lJsonResult = JSON.parse(response);
                                
                var erro = false;
                var message = "";
                var information = "";
                var warning = "";
                                
                $.map(lJsonResult, function (item) {
                    if(item!=null && item.astType!=undefined && item.astType!=null)
                    {
                        if (!erro){
                            erro = (item.astType==KDCoMessage_Type.kdError);
                        }
                                                                       
                        message += (item.astType==KDCoMessage_Type.kdError) ? item.astMessage + "</br>": "";
                        information += (item.astType==KDCoMessage_Type.kdInformation) ? item.astMessage + "</br>": "";
                        warning += (item.astType==KDCoMessage_Type.kdWarning)? item.astMessage + "</br>": "";
                    }
                    
                    return {};
                });
                
                if (erro){
                    if (settings.cbhError == null)
                        if (settings.clsMensagemRetorno == null){
                            alert(message.ReplaceAll("</br>", "\n"));
                        }
                        else{    
                            $("").coerrorbox({
                                Mensagem: message,
                                classe: settings.clsMensagemRetorno
                            });
                        }
                    else
                        settings.cbhError(lJsonResult);
                }
                else if (information != ""){
                    if (settings.clsMensagemInformation == null){
                            alert(information.ReplaceAll("</br>", "\n"));
                        }
                    else {
                        $("").coinformationbox({
                                Mensagem: information,
                                classe: settings.clsMensagemInformation
                            });
                    }
                }

                if (settings.splashscreen){
                    $("").coremovesplashscreenbox();
                }

                var lFrame = document.getElementById(frame_id);
                if (lFrame != null)
                    lFrame.remove();

                element.val(null);
                                    
                if (settings.afterUpload != null){
                    settings.afterUpload.call(element, response);
                }
              };
          
          var montarFrame = function(element) {
            $('body').after('<iframe width="0" height="0" style="display:none;" name="'+frame_id+'" id="'+frame_id+'"/>');
            $('#'+frame_id).load(function() {
              handleResponse(this, element);
            });

            element.wrap(function() {
              return '<form action="' + settings.action + '" method="POST" enctype="multipart/form-data" target="'+frame_id+'" />'
            })

            .before(function() {
              var key, html = '';
              for(key in settings.params) {
                var paramVal = settings.params[key];
                if (typeof paramVal === 'function') {
                  paramVal = paramVal();
                }
                html += '<input type="hidden" name="' + key + '" value="' + paramVal + '" />';
              }
              return html;
            });
          }
        });
      }
})( jQuery );

//TESTE  
(function ($) {
    $.fn.cowaitscreen = function (options) {
        var elem = this;
        var settings = $.extend({}, $.fn.cowaitscreen.defaults, options);

		var aLoadingForm = null;

		var Sessao = "";
		Sessao += "<section  id='"+settings.nomediv+"'>";
		Sessao += "    <i class='fa fa-circle-o-notch fa-spin fa-3x fa-fw margin-bottom'></i>";
 		Sessao += "</section>";
		
        var ExisteDiv = $('body').find('#'+settings.nomediv).length > 0 ? true : false;
        if (!ExisteDiv)
        {
            $('body').append(Sessao);

            $('#'+settings.nomediv).css("display","none"); 
            $('#'+settings.nomediv).css("width","auto"); 
            $('#'+settings.nomediv).css("height","auto");
            $('#'+settings.nomediv).css("border","1px solid #009F93");
            $('#'+settings.nomediv).css("background-color","#009F93");
            $('#'+settings.nomediv).css("box-shadow","4px 4px 13px #696969");
            $('#'+settings.nomediv).css("border-radius","4px");
            $('#'+settings.nomediv).css("opacity","0.9");

        }


        var lsplashscreenform =
        {
            close: function () {
                aLoadingForm.close();
            },
            show: function () {

				aLoadingForm = $("").coloadmodalform({
					form: settings.nomediv,
					center: settings.center,
					modal: settings.modal,
                    zindex: 999999,
					load: false
				});	
            }
        }

        return lsplashscreenform;

    }
    $.fn.cowaitscreen.defaults = {
		nomediv: "FormcoWaitScreen",
        modal: false,
        center: true
    }
})(jQuery);
