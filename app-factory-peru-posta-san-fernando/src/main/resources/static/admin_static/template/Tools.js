﻿var globalFunctions = (function () {

    //# INI - Globals Enumerartions
    var KDOperation = {
        INSERT: 0,
        UPDATE: 1,
        DELETE: 2,
        SEARCH: 3
    }

    var KDUser = {
        External: 0,
        Internal: 1
    }

    var KDLogical = {
        NO: 0,
        YES: 1
    }
    //# END - Globals Enumerartions

    function extendKORequiredString(prKo) {

        prKo.extenders.required = function (target, overrideMessage) {
            target.hasError = ko.observable();
            target.validationMessage = ko.observable();

            //validates
            function validate(newValue) {
                target.hasError(newValue ? false : true);
                target.validationMessage(newValue ? "" : overrideMessage || "Obligatorio");
            }
            validate(target()); //validation Home
            target.subscribe(validate); //Validate whenever the value changes

            return target;
        };
    };

    function extendKORequiredCombo(prKo) {

        prKo.extenders.requiredCombo = function (target, overrideMessage) {
            target.hasError = ko.observable();
            target.validationMessage = ko.observable();

            //validates
            function validate(newValue) {
                target.hasError(newValue != "-1" ? false : true);
                target.validationMessage(newValue != "-1" ? "" : overrideMessage || "Obligatorio");
            }
            validate(target()); //validation Home
            target.subscribe(validate); //Validate whenever the value changes

            return target;
        };
    };

    function extendKORequiredUnderAge(prKo) {

        prKo.extenders.requiredUnderAge = function (target, overrideMessage) {
            target.hasError = ko.observable();
            target.validationMessage = ko.observable();

            function validate(newValue) {
                var lAge = getAge(newValue);
                target.hasError(lAge >= 18 ? false : true);
                target.validationMessage(lAge >= 18 ? "" : overrideMessage || "Obligatorio");
            }
            validate(target());
            target.subscribe(validate);

            return target;
        };
    };


    /*  
   it is possible to enter the other extenders within this method below, 
   eliminating the other methods. review .....
    */
    function configTools(prKo) {

        prKo.extenders.onlyNumbers = function (target) {
            //validates
            function validate(newValue) {
                if (newValue != "" && newValue != undefined && newValue != null) {
                    target(newValue.replace(/[^0-9]+/g, ''));
                }
            };

            validate(target());
            target.subscribe(validate);

            return target;

        };

        prKo.extenders.onlyLetters = function (target) {
            //validates
            function validate(newValue) {
                if (newValue != "" && newValue != undefined && newValue != null) {
                    target(newValue.replace(/[^A-Za-z\s]+/g, ''));
                }
            };

            validate(target());
            target.subscribe(validate);

            return target;

        };

        prKo.extenders.onlyLettersAndCharSpecial = function (target) {
            //validates
            function validate(newValue) {
                if (newValue != "" && newValue != undefined && newValue != null) {
                    target(newValue.replace(/[^A-Za-z\sáéíóúÁÉÍÓÚñÑüÜ]/g, ''));
                }
            };

            validate(target());
            target.subscribe(validate);

            return target;

        };

    };

    return {
        KDOperation: KDOperation,
        KDLogical: KDLogical,
        KDUser: KDUser,
        extendKORequiredString: extendKORequiredString,
        extendKORequiredCombo: extendKORequiredCombo,
        extendKORequiredUnderAge: extendKORequiredUnderAge,
        configTools: configTools
    };
})();


