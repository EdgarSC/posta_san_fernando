﻿
//** teste // 
(function ($) {
    $.fn.coloadmodalform = function (options) {
        var aCoLoadModalElemOrigem = this;
        var settings = $.extend({}, $.fn.coloadmodalform.defaults, options);
        var aLoading = null;

        if (settings.waitscreen == true) {
            aLoading = $("").cowaitscreen({ modal: true }); //carregando o loading;
        }

        var lZIndex = $(this).css("z-index");
        if ((lZIndex != 'auto') && (lZIndex != null)) {
            settings.zindex = parseInt(lZIndex) + 10;
        }

        var lloadmodalform =
        {
            close: function () {
                if (settings.afterclose != null) { settings.afterclose(); }

                if (settings.modal == true) { var DivMask = "#divMask" + settings.form; }
                var DivModalForm = "#divModalForm" + settings.form;

                if (settings.modal == true) { $(DivMask).remove(); }
                $(DivModalForm).remove();

                if (settings.load == false) { $('#' + DivFom).css({ 'display': 'none' }); }
            },
            getzindex: function () {
                return settings.zindex;
            },
            buttoncloseform: function (divremove, divdisplaynone) {
                $('#' + settings.buttonclose).click(function (e) {
                    e.preventDefault();
                    lloadmodalform.close();
                    //caso se defina uma funcao para quando a tela fechar
                    if (settings.functionclose != null)
                        settings.functionclose();
                });
            },
            atualizecss: function (divatualizecss) {
                if (settings.width != null) {
                    $('#' + divatualizecss).css({ 'width': settings.width });
                }
                if (settings.height != null) {
                    $('#' + divatualizecss).css({ 'height': settings.height });
                }

                if (settings.center == true) {
                    lloadmodalform.centralizamodal();
                }
                else {
                    lloadmodalform.posicaofixamodal();
                }

                $('#' + divatualizecss).css({ 'z-index': settings.zindex + 20 });
                $('#' + divatualizecss).css({ 'position': 'absolute' });
                $('#' + divatualizecss).css({ 'display': 'none' });
                $('#' + divatualizecss).css({ 'padding': '0px' });


                if (settings.draggable == true)
                    $('#' + DivFom).draggable();
                if (settings.resizable == true)
                    $('#' + DivFom).resizable();

            },
            posicaofixamodal: function () {
                var posicionX = settings.positionX;
                var posicionY = settings.positionY;
                if (posicionY < 0) { posicionY = 10; }
                if (posicionX < 0) { posicionX = 10; }

                $("#" + settings.form).css({ 'top': posicionY, 'left': posicionX });
            },
            centralizamodal: function () {

                if (settings.center == true) {

                    //if (aCoLoadModalElemOrigem.height() == null && aCoLoadModalElemOrigem.width() == null) {
                    if ((settings.load == true) || ((aCoLoadModalElemOrigem.height() == null && aCoLoadModalElemOrigem.width() == null))) {

                        var winH = $(window.parent).height();
                        var winW = $(window.parent).width();

                        var posicionY = (winH / 2 - $("#" + settings.form).height() / 2);
                        var posicionX = (winW / 2 - $("#" + settings.form).width() / 2);

                        posicionY = posicionY + $(window.parent).scrollTop();
                        posicionX = posicionX + $(window.parent).scrollLeft();
                    }
                    else {
                        var difH = ($('body').height() - aCoLoadModalElemOrigem.height()) / 2;
                        var difW = ($('body').width() - aCoLoadModalElemOrigem.width()) / 2;

                        var winH = ($(window.parent).height() / 2);
                        var winW = ($(window.parent).width() / 2);

                        var posicionY = ((winH - ($("#" + settings.form).height() / 2))) - difH;
                        var posicionX = ((winW - ($("#" + settings.form).width() / 2))) - difW;

                        posicionY = posicionY + $(window.parent).scrollTop();
                        posicionX = posicionX + $(window.parent).scrollLeft();
                    }

                    if (posicionY < 0) { posicionY = 10; }
                    if (posicionX < 0) { posicionX = 10; }
                    $("#" + settings.form).css({ 'top': posicionY, 'left': posicionX });
                }

            },
            formtomodal: function () {
                var DivModalForm = "#divModalForm" + settings.form;
                $(DivModalForm).find('.nopModal').show();
                $(DivModalForm).find("#" + settings.form).css({ 'margin': '0 auto' });
                $(DivModalForm).find("#" + settings.form).css({ 'border': 'solid 1px #8F9EB6' });
                $(DivModalForm).find("#" + settings.form).css({ 'border-radius': '5px' });
                $(DivModalForm).find("#" + settings.form).css({ 'margin': '0 auto' });
                $(DivModalForm).find("#" + settings.form).css({ 'padding': '10px' });
                $(DivModalForm).find("#" + settings.form).css({ 'box-shadow': '5px 5px 10px #ccc' });
                $(DivModalForm).find("#" + settings.form).css({ 'background': 'White' });
                $("#" + settings.form).css({ 'top': 0, 'left': 0 });

                if (settings.width != null) {
                    $("#" + settings.form).css({ 'width': settings.width });
                }
                if (settings.height != null) {
                    $("#" + settings.form).css({ 'height': settings.height });
                }

                if (settings.center == true) {
                    lloadmodalform.centralizamodal();
                }
                else {
                    lloadmodalform.posicaofixamodal();
                }
            }
        }

        if (settings.modal == true) { var DivMask = "divMask" + settings.form; }
        var DivModalForm = "divModalForm" + settings.form;
        var DivFom = settings.form;

        var ExisteDiv = $('body').find("#" + DivMask).length > 0 ? true : false;
        if ((!ExisteDiv) && (settings.modal == true)) {
            var lDiv = "<div id='" + DivMask + "' style='position:absolute;left:0;top:0;background-color:#000;display:none;'></div>";
            $('body').append(lDiv);
            $('#' + DivMask).css({ 'z-index': settings.zindex + 10 });

            $(window).scroll(function () {
                if ($('body').find("#" + DivMask).length > 0) {
                    $('#' + DivMask).css({ 'width': $(window).width(), 'height': $(document).height() });
                }
            });

            $(window).resize(function () {
                if ($('body').find("#" + DivMask).length > 0) {
                    $('#' + DivMask).css({ 'width': $(window).width(), 'height': $(document).height() });
                }
            });

        }

        $(window).resize(function () {
            //quando diminuir a tela tem que centralizar
            lloadmodalform.centralizamodal();
        });

        var ExistDivModalForm = $('body').find("#" + DivModalForm).length > 0 ? true : false;
        if (!ExistDivModalForm) {
            var lDivModalForm = "<section id='" + DivModalForm + "'></section>";
            $('body').append(lDivModalForm);
        }

        if (settings.modal == true) { $('#' + DivMask).css({ 'width': $(window).width(), 'height': $(document).height() }); }

        //var aLoading = $("").cowaitscreen(); //carregando o loading;
        //if (aLoading != null) aLoading.show(); //mostrando o loading;

        if (settings.load == true) {
            if (aLoading != null) aLoading.show(); //mostrando o loading;

            $("#" + DivModalForm).load(settings.action, { jsonparametros: settings.jsonparametros }, function (response, status, xhr) {

                if (aLoading != null) aLoading.close(); //mostrando o loading;

                if (status == "success") {

                    if (settings.modal == true) { $('#' + DivMask).fadeTo("fast", 0.2); }

                    lloadmodalform.atualizecss(DivFom);
                    $("#" + DivModalForm).find("#" + DivFom).show();
                    if (settings.afterload != null)
                        settings.afterload(lloadmodalform);

                    if (settings.formtomodal == true)
                        lloadmodalform.formtomodal();

                    lloadmodalform.buttoncloseform(DivModalForm, null);
                    //if (aLoading != null) aLoading.close(); //mostrando o loading;

                }
            });
        }
        else {

            if (settings.modal == true) { $('#' + DivMask).fadeTo("fast", 0.2); }

            lloadmodalform.atualizecss(DivFom);
            $("#" + DivFom).show();

            if (settings.afterload != null)
                settings.afterload();

            lloadmodalform.buttoncloseform(DivModalForm, DivFom);
        }

        if (settings.buttonconfirm != "") {
            var lBotaoConfirm = $('#' + settings.buttonconfirm);
            lBotaoConfirm.unbind("click");
            lBotaoConfirm.click(function (e) {
                settings.functionconfirm(lloadmodalform);
            });

        }

        if (settings.modal == true) {
            $('#' + DivMask).css({ 'width': $(window).width(), 'height': $(document).height() });
            $('#' + DivMask).fadeTo("fast", 0.2);
        }

        if (settings.draggable == true)
            $('#' + DivFom).draggable();
        if (settings.resizable == true)
            $('#' + DivFom).resizable();

        return lloadmodalform;

    }
    $.fn.coloadmodalform.defaults = {
        form: "dialog",
        load: true,
        modal: true,
        html: null,
        action: null,
        positionX: 10,
        positionY: 10,
        center: false,
        buttonclose: "",
        buttonconfirm: "",
        functionconfirm: null,
        functionclose: null,
        zindex: 9000,
        afterload: null,
        width: null,
        height: null,
        formtomodal: false,
        draggable: false,
        resizable: false,
        afterclose: null,
        jsonparametros: "",
        waitscreen: true
    }
})(jQuery);



(function ($) {
    $.fn.coserver = function (options) {
        var settings = $.extend({}, $.fn.coserver.defaults, options);

        if (settings.takeresult > 0) {
            $("#" + settings.btnext).unbind("click");
            $("#" + settings.btprev).unbind("click");

            $("#" + settings.btlast).unbind("click");
            $("#" + settings.btfirst).unbind("click");

            $("#" + settings.btnext).click(function () {
                $(window).scrollLeft(0);
                $(window).scrollTop(0);
                coserver_.callService(coserver_.parameters, 'next');
            });

            $("#" + settings.btlast).click(function () {
                $(window).scrollLeft(0);
                $(window).scrollTop(0);
                coserver_.callService(coserver_.parameters, 'last');
            });

            $("#" + settings.btprev).click(function () {
                $(window).scrollLeft(0);
                $(window).scrollTop(0);
                coserver_.callService(coserver_.parameters, 'prev');
            });

            $("#" + settings.btfirst).click(function () {
                $(window).scrollLeft(0);
                $(window).scrollTop(0);
                coserver_.callService(coserver_.parameters, 'first');

            });
        }

        var coserver_ =
        {
            totalRegistro: 0,
            skipResult: 0,
            ornav: "",
            qtdResult: 0,
            navNextPrev: 0, //Load
            parameters: "",
            errorHandling: function (dado) {
                var lerro = "";
                if (dado != null && dado.astType != undefined && dado.astType != null)
                    lerro = dado.astMessage;
                else {
                    $.map(dado, function (item) {
                        lerro += (item.astType == KDCoMessage_Type.kdError) ? item.astMessage + "</br>" : "";
                        return {};
                    });
                }


                if (dialogSessionExpired(dado) == false)
                {
                    if ((settings.cbhError == null) && (settings.cbhErrorWithObject == null)) {

                        if (settings.clsMensagemRetorno == null) {
                            alert(lerro.ReplaceAll("</br>", "\n"));
                        }
                        else {
                            $("").coerrorbox({
                                Mensagem: lerro,
                                classe: settings.clsMensagemRetorno
                            });
                        }
                    }
                    else {
                        if (settings.cbhError != null) {
                            settings.cbhError(dado);
                        }

                        if (settings.cbhErrorWithObject != null)
                            settings.cbhErrorWithObject(dado, settings.cbhObject);
                    }
                }
            },

            callService: function (parameters, nav) {
                var aLoading = $("").cowaitscreen({modal: true}); //loading;

                if (settings.showload == true)
                    $(".loading").show();
                if (settings.splashscreen == true)
                {
                    if (aLoading != null) aLoading.show(); //show the loading;
                }

                coserver_.parameters = parameters;
                if (settings.takeresult > 0) {
                    switch (nav) {
                        case 'next':
                            coserver_.skipResult += coserver_.qtdResult;
                            break;
                        case 'last':
                            coserver_.skipResult = -1;
                            break;
                        case 'prev':
                            if (coserver_.qtdResult == 0)
                                coserver_.qtdResult = settings.takeresult;

                            coserver_.skipResult = (coserver_.skipResult - settings.takeresult);
                            if (coserver_.skipResult < 0)
                                coserver_.skipResult = 0;

                            break;
                        case 'first':
                            coserver_.skipResult = 0;
                            break;
                    }

                    if (parameters != "" && parameters != null)
                        parameters = parameters + "&skipresult=" + coserver_.skipResult + "&takeresult=" + settings.takeresult;
                    else
                        parameters = "skipresult=" + coserver_.skipResult + "&takeresult=" + settings.takeresult;
                }

               // parameters = parameters + "&sessionID=" + getSessionID() + "&TopologySessionID=" + getTopologySessionID();

                var urlAction = settings.action.indexOf("?") > 0
                    ? settings.action + "&sessionID=" + getSessionID() + "&TopologySessionID=" + getTopologySessionID()
                    : settings.action + "?sessionID=" + getSessionID() + "&TopologySessionID=" + getTopologySessionID();

                if (settings.DownloadFileResult)
                {
                    //$("#ifrDownloadMain").attr("src", settings.action + "?" + parameters);
					$("#ifrDownloadMain").attr("src", urlAction + "&" + parameters);
                    $(".loading").hide();
                    if (settings.splashscreen) {
                        if (aLoading != null)
                            aLoading.close(); //show the loading;
                    }
                }
                else
                {
                    $.ajax({
                        type: settings.type,
                        //url: settings.action,
                        url: urlAction,
                        data: parameters,
                        dataType: "json",
                        contentType: settings.contentType,
                        traditional: true,
                        success: function (dado) {
                            var erro = false;
                            var information = "";
                            var warning = "";
                            var message = "";

                            coserver_.qtdResult = 0;

                            $(".loading").hide();

                            if (settings.splashscreen) {
                                if (aLoading != null) aLoading.close(); //show the loading;
                            }

                            if (dado != null && dado.astType != undefined && dado.astType != null)
                                erro = (dado.astType == KDCoMessage_Type.kdError) ? true : false;

                            if (!erro) {
                                $.map(dado, function (item) {
                                    if (item != null && item.astType != undefined && item.astType != null) {
                                        if (!erro) {
                                            erro = (item.astType == KDCoMessage_Type.kdError) ? true : false;
                                        }

                                        message += (item.astType == KDCoMessage_Type.kdError) ? item.astMessage : "";
                                        information += (item.astType == KDCoMessage_Type.kdInformation) ? item.astMessage : "";
                                        warning += (item.astType == KDCoMessage_Type.kdWarning) ? item.astMessage : "";

                                    }
                                    else
                                        coserver_.qtdResult++;

                                    return {};
                                });

                                if (!erro) {
                                    if (settings.takeresult > 0) {
                                        if (coserver_.qtdResult > 0) {
                                            if ((coserver_.qtdResult % settings.takeresult) == 0) {
                                                if (coserver_.totalRegistro == (coserver_.skipResult + settings.takeresult)) {
                                                    $("#" + settings.btnext).hide();
                                                    $("#" + settings.btlast).hide();
                                                }
                                                else {
                                                    $("#" + settings.btnext).show();
                                                    $("#" + settings.btlast).show();
                                                }
                                                if (coserver_.skipResult == 0) {
                                                    $("#" + settings.btprev).hide();
                                                    $("#" + settings.btfirst).hide();
                                                }
                                                else {
                                                    $("#" + settings.btprev).show();
                                                    $("#" + settings.btfirst).show();
                                                }

                                            }
                                            else {
                                                $("#" + settings.btnext).hide();
                                                $("#" + settings.btlast).hide();
                                                if (coserver_.skipResult < settings.takeresult) {
                                                    $("#" + settings.btprev).hide();
                                                    $("#" + settings.btfirst).hide();
                                                }
                                                else {
                                                    $("#" + settings.btprev).show();
                                                    $("#" + settings.btfirst).show();
                                                }
                                            }
                                        }
                                        else if (coserver_.qtdResult == 0) {
                                            if (coserver_.skipResult == 0) {
                                                $("#" + settings.btnext).hide();
                                                $("#" + settings.btlast).hide();

                                                $("#" + settings.btprev).hide();
                                                $("#" + settings.btfirst).hide();
                                            }
                                            else {
                                                $("#" + settings.btnext).hide();
                                                $("#" + settings.btlast).hide();

                                                $("#" + settings.btprev).show();
                                                $("#" + settings.btfirst).show();
                                            }
                                        }

                                    }
                                }
                            }

                            if (erro) {
                                coserver_.errorHandling(dado);
                            }
                            else {
                                if (settings.cbhSucess != null)
                                    if (coserver_.skipResult == -1) {
                                        coserver_.totalRegistro = parseInt(information.replace("</br>", ""));
                                        coserver_.skipResult = coserver_.totalRegistro - settings.takeresult;
                                        if (coserver_.skipResult < 0)
                                            coserver_.skipResult = 0;

                                        coserver_.callService(coserver_.parameters, 'next');
                                    } else
                                        settings.cbhSucess(dado);

                                if ((settings.cbhSucessWithObject != null) && (settings.cbhObject != null)) {
                                    if (coserver_.skipResult == -1) {
                                        coserver_.totalRegistro = parseInt(information.replace("</br>", ""));
                                        coserver_.skipResult = coserver_.totalRegistro - settings.takeresult;
                                        if (coserver_.skipResult < 0)
                                            coserver_.skipResult = 0;

                                        coserver_.callService(coserver_.parameters, 'next');
                                    } else {
                                        settings.cbhSucessWithObject(dado, settings.cbhObject);
                                    }
                                }

                            }

                            if (information != "" && settings.clsMensagemInformation != null) {

                                $("").coinformationbox({
                                    Mensagem: information,
                                    classe: settings.clsMensagemInformation
                                });
                            } else {
                                if (information != "" && settings.cbhSucess == null && settings.cbhSucessWithObject == null) {
                                    alert(information);
                                }
                            }

                            if (warning != "" && settings.clsMensagemWarning != null) {
                                $("").cowarningbox({
                                    Mensagem: warning,
                                    classe: settings.clsMensagemWarning
                                });
                            } else {
                                if (warning != "" && settings.cbhSucess == null && settings.cbhSucessWithObject == null) {
                                    alert(warning);
                                }
                            }
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            $(".loading").hide();

                            if (settings.splashscreen) {
                                if (aLoading != null) aLoading.close(); //show the loading;
                            }

                            if (settings.cbhError != null) {
                                settings.cbhError(xhr, ajaxOptions, thrownError);

                            }
                            else {
                                if (coenv != KDCoEnv.Producao)
                                    alert("Alerta: coloadbox s=" + xhr.status + " te=" + thrownError + " rt=" + xhr.responseText);
                            }
                        }
                    });
                }
            }
        }

        return coserver_;
    }

    $.fn.coserver.defaults = {
        type: "POST",
        action: "",
        datatype: "json",
        contentType: "application/x-www-form-urlencoded",// application/json
        cbhSucess: null,
        cbhSucessWithObject: null,
        cbhObject: null,
        cbhError: null,
        cbhErrorWithObject: null,
        clsMensagemRetorno: null,
        clsMensagemWarning: null,
        clsMensagemInformation: null,
        takeresult: 0,
        btnext: "",
        btprev: "",
        btlast: "",
        btfirst: "",
        splashscreen: false,
        modal: false,
        showload: true,
        DownloadFileResult: false
    }

})(jQuery);


String.prototype.ReplaceCharSpecial = function () {
    var temp = this;
    var SpecialChars = ["&quot;", "&#227;", "&#231;", "&#237;", "&#199;", "&#225;", "&#211;", "&#218;", "&#233;", "&#195;", "&#205;", "&#193;", "&#250;", "&#249;", "&#243;", "&#180;", "&#234;", "&#202;", "&#201;", "&#213;", "&#245;", "&#194;", "&#160;", "&#212;", "&#244;"  ];
    var NewChars =     ['"'     , "ã"     , "ç"     , "í"     , "Ç"     , "á"     , "Ó"     , "Ú"     , "é"     , "Ã"     , "Í"     , "Á"     , "ú"     , "ù"     ,"ó"      , "´"     , "ê"     , "Ê",      "É"     , "Õ"     , "õ"     , "Â"     , " "     , "Ô"     , "ô"       ];

    while (FindArrayInString(temp, SpecialChars) == true) {
        for (index = 0; index < SpecialChars.length; ++index) {
            if ((temp.indexOf(SpecialChars[index]) != -1))
                temp = temp.replace(SpecialChars[index], NewChars[index]);
        }
    }
    return temp;
};


function FindArrayInString(prString, prArray) {
    var lRetorno = false;

    for (myIndex = 0; myIndex < prArray.length; ++myIndex) {
        if ((prString.indexOf(prArray[myIndex]) != -1)) {
            lRetorno = true; ;
            break;
        }
    }
    return lRetorno;
}


String.prototype.DateToJson = function () {
    var temp = this;

    var _DateToJson = temp.split("/");
    var lDateToJson = new Date(_DateToJson[2], _DateToJson[1] - 1, _DateToJson[0], 0, 0, 0, 0)

    return lDateToJson;
};



String.prototype.ToMoney = function () {
    var temp = this;
    var lToMoney = parseFloat(temp.replace(/\./g, '').replace(',', '.'));
    return lToMoney;
};



(function ($) {
    $.fn.coalertbox = function (options) {
        var elemOri = this;
        
        var settings = $.extend({}, $.fn.coalertbox.defaults, options);
        var lDate = new Date();
        var lNameElement = settings.nomediv +settings.tipo+ lDate.getMilliseconds();

        var divelem = '#' + settings.nomediv;
        var lDiv = "<div id='" + settings.nomediv + "'></div>";
        var ExisteDiv = $('body').find('#' + settings.nomediv).length > 0 ? true : false;

        if (!ExisteDiv) { $('body').append(lDiv); }


        var lZIndex = settings.zindex;

        if ((elemOri.css("z-index") != undefined) && (lZIndex != 'auto')) {
                lZIndex = elemOri.css("z-index") + 10;
        }

        var lcoalertbox =
        {
            close: function () {
                if (settings.effect == 'slidedown') {
                    $("#" + lNameElement).slideUp(settings.speed).show();
                } else if (settings.effect == 'fadeout') {
                    $("#" + lNameElement).fadeOut('slow');
                } else {
                    $("#" + lNameElement).remove();
                }

                if (settings.closewindowclick == true) { $(window).unbind('click'); }
                if (settings.closemouseleave == true) { $(elem).unbind('click'); }

            },
            createcoalertbox: function () {
                var dHint = $('<div id="' + lNameElement + '"></div>');
                dHint.css('box-shadow', "1px 1px 13px  #696969");

                if (settings.tipo == "danger") {
                    dHint.attr('class', 'alert alert-danger');
                }
                if (settings.tipo == "info") {
                    dHint.attr('class', 'alert alert-info');
                }
                if (settings.tipo == "warning") {
                    dHint.attr('class', 'alert alert-warning');
                }

                if (settings.tipo == "success") {
                    dHint.attr('class', 'alert alert-success');
                }

                dHint.attr('role', 'alert');


                var lImagem = settings.imagem;
                if (settings.imagem == "")
                {
                    if (settings.mostrarimagem == true)
                    {
                        if (settings.tipo == "danger") {
                            lImagem = '<i class="fa fa-exclamation-triangle"></i>';
                        }
                        if (settings.tipo == "info") {
                            lImagem = '<i class="fa fa-info"></i>';
                        }
                        if (settings.tipo == "warning") {
                            lImagem = '<i class="fa fa-check"></i>';
                        }
                        if (settings.tipo == "success") {
                            lImagem = '<i class="fa fa-exclamation-circle"></i>';
                        }
                    }
                }

                var lMensagem = ((lImagem != ""?lImagem+'&nbsp;&nbsp;':"")+settings.mensagem);
                $('<span>'+lMensagem +'</span>').appendTo(dHint);

                var lDivMensagemButton = $('<button>');
                lDivMensagemButton.attr('type', 'button');
                lDivMensagemButton.attr('class', 'close');
                lDivMensagemButton.attr('title', 'Fechar');
                lDivMensagemButton.attr('onclick', '$("#' + lNameElement + '").remove()');

                var lDivMensagemSpan = $('<span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span>');

                lDivMensagemSpan.appendTo(lDivMensagemButton);
                lDivMensagemButton.appendTo(dHint);

                dHint.appendTo(divelem);

                $(divelem).css({ width: settings.width });
                $(divelem).css({ height: settings.height });

                if (settings.center != null) {

                    var lPosicaoX = settings.posicaoY + $(window).scrollTop();
                    var lPosicaoY = settings.posicaoX + $(window).scrollLeft();
                    if (settings.center == true) {
                        //START: Find the center of the window
                        var winH = $(window.parent).height();
                        var winW = $(window.parent).width();

                        var lPosicaoY = (winH / 2 - $("#" + settings.nomediv).height() / 2);
                        var lPosicaoX = (winW / 2 - $("#" + settings.nomediv).width() / 2);

                        lPosicaoY = lPosicaoY + $(window.parent).scrollTop();
                        lPosicaoX = lPosicaoX + $(window.parent).scrollLeft();

                        //END: Find the center of the window
                    }

                }

                $(divelem).css('position', 'absolute');
                $(divelem).css('z-index', lZIndex);
                $(divelem).css('display', 'inline');
                $(divelem).css({ top: lPosicaoY });
                $(divelem).css({ left: lPosicaoX });
                $(divelem).css({ padding: settings.padding });

            },
            closewindowclick: function () {
                $(window).click(function (e) {
                    lcoalertbox.close();
                });
            },
            closemouseleave: function () {
                $(elem).mouseleave(function (e) {
                    lcoalertbox.close();
                });
            }
        }

        lcoalertbox.createcoalertbox();

        if ((settings.temporario == false) && (settings.effect != 'slidedown'))
            $("#" + lNameElement).fadeIn(10);

        if ((settings.temporario == true) || (settings.effect == 'slidedown')) {
            if (settings.effect == 'slidedown') {
                $("#" + lNameElement).slideDown(settings.speed).show();
            } else {
                $("#" + lNameElement).fadeIn('slow').delay(settings.delay).fadeOut('slow');
            }
        }
        if (settings.closewindowclick == true) { lcoalertbox.closewindowclick(); }
        if (settings.closemouseleave == true) { lcoalertbox.closemouseleave(); }

        return lcoalertbox;

    }
    $.fn.coalertbox.defaults = {
        posicaoX: 10,
        posicaoY: 10,
        center: true,
        nomediv: "divHintcoalertbox",
        tipo: "info",
        mensagem: "",
        width: "450px",
        height: "auto",
        padding: "5px 0 5px 1px",
        temporario: false,
        delay: 4000,
        effect: 'nomal',
        speed: 'fast',
        zindex: '99999',
        shadow: true,
        imagem: '',
        mostrarimagem: true,
        closewindowclick: false,
        closemouseleave: false
    }
})(jQuery);


var validarCPFImpl = function (strCPF) {
    var Soma;
    var Resto;
    Soma = 0;
    if (strCPF == "00000000000" || strCPF == "11111111111" || strCPF == "22222222222"
            || strCPF == "33333333333" || strCPF == "44444444444" || strCPF == "55555555555"
            || strCPF == "66666666666" || strCPF == "77777777777" || strCPF == "88888888888"
            || strCPF == "99999999999" || strCPF == "12345678909")
        return false;

    for (i = 1; i <= 9; i++) Soma = Soma + parseInt(strCPF.substring(i - 1, i)) * (11 - i);
    Resto = (Soma * 10) % 11;
    if ((Resto == 10) || (Resto == 11))
        Resto = 0;

    if (Resto != parseInt(strCPF.substring(9, 10)))
        return false;

    Soma = 0;
    for (i = 1; i <= 10; i++) Soma = Soma + parseInt(strCPF.substring(i - 1, i)) * (12 - i);
    Resto = (Soma * 10) % 11;

    if ((Resto == 10) || (Resto == 11)) Resto = 0;

    if (Resto != parseInt(strCPF.substring(10, 11)))
        return false;

    return true;
}


function validarCNPJ(cnpj) {

    cnpj = cnpj.replace(/[^\d]+/g, '');

    if (cnpj == '') return false;

    if (cnpj.length != 14)
        return false;

    if (cnpj == "00000000000000" ||
        cnpj == "11111111111111" ||
        cnpj == "22222222222222" ||
        cnpj == "33333333333333" ||
        cnpj == "44444444444444" ||
        cnpj == "55555555555555" ||
        cnpj == "66666666666666" ||
        cnpj == "77777777777777" ||
        cnpj == "88888888888888" ||
        cnpj == "99999999999999")
        return false;

    
    tamanho = cnpj.length - 2
    numeros = cnpj.substring(0, tamanho);
    digitos = cnpj.substring(tamanho);
    soma = 0;
    pos = tamanho - 7;
    for (i = tamanho; i >= 1; i--) {
        soma += numeros.charAt(tamanho - i) * pos--;
        if (pos < 2)
            pos = 9;
    }
    resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
    if (resultado != digitos.charAt(o))
        return false;

    tamanho = tamanho + 1;
    numeros = cnpj.substring(0, tamanho);
    soma = 0;
    pos = tamanho - 7;
    for (i = tamanho; i >= 1; i--) {
        soma += numeros.charAt(tamanho - i) * pos--;
        if (pos < 2)
            pos = 9;
    }
    resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
    if (resultado != digitos.charAt(1))
        return false;

    return true;

}

//Mask

(function ($) {
    
    var iPhone = (window.orientation != undefined);

    $.mask = {
        //Predefined character definitions
        definitions: {
            '9': "[0-9]",
            'a': "[A-Za-z]",
            '*': "[A-Za-z0-9]"
        },
        dataName: "rawMaskFn"
    };

    $.fn.extend({
        //Helper Function for Caret positioning
        caret: function (begin, end) {
            if (this.length == 0) return;
            if (typeof begin == 'number') {
                end = (typeof end == 'number') ? end : begin;
                return this.each(function () {
                    if (this.setSelectionRange) {
                        this.setSelectionRange(begin, end);
                    } else if (this.createTextRange) {
                        var range = this.createTextRange();
                        range.collapse(true);
                        range.moveEnd('character', end);
                        range.moveStart('character', begin);
                        range.select();
                    }
                });
            } else {
                if (this[0].setSelectionRange) {
                    begin = this[0].selectionStart;
                    end = this[0].selectionEnd;
                } else if (document.selection && document.selection.createRange) {
                    var range = document.selection.createRange();
                    begin = 0 - range.duplicate().moveStart('character', -100000);
                    end = begin + range.text.length;
                }
                return { begin: begin, end: end };
            }
        },
        unmask: function () { return this.trigger("unmask"); },
        mask: function (mask, settings) {
            if (!mask && this.length > 0) {
                var input = $(this[0]);
                return input.data($.mask.dataName)();
            }
            settings = $.extend({
                placeholder: "_",
                completed: null
            }, settings);

            var defs = $.mask.definitions;
            var tests = [];
            var partialPosition = mask.length;
            var firstNonMaskPos = null;
            var len = mask.length;

            $.each(mask.split(""), function (i, c) {
                if (c == '?') {
                    len--;
                    partialPosition = i;
                } else if (defs[c]) {
                    tests.push(new RegExp(defs[c]));
                    if (firstNonMaskPos == null)
                        firstNonMaskPos = tests.length - 1;
                } else {
                    tests.push(null);
                }
            });

            return this.trigger("unmask").each(function () {
                var input = $(this);
                var buffer = $.map(mask.split(""), function (c, i) { if (c != '?') return defs[c] ? settings.placeholder : c });
                var focusText = input.val();

                function seekNext(pos) {
                    while (++pos <= len && !tests[pos]);
                    return pos;
                };
                function seekPrev(pos) {
                    while (--pos >= 0 && !tests[pos]);
                    return pos;
                };

                function shiftL(begin, end) {
                    if (begin < 0)
                        return;
                    for (var i = begin, j = seekNext(end); i < len; i++) {
                        if (tests[i]) {
                            if (j < len && tests[i].test(buffer[j])) {
                                buffer[i] = buffer[j];
                                buffer[j] = settings.placeholder;
                            } else
                                break;
                            j = seekNext(j);
                        }
                    }
                    writeBuffer();
                    input.caret(Math.max(firstNonMaskPos, begin));
                };

                function shiftR(pos) {
                    for (var i = pos, c = settings.placeholder; i < len; i++) {
                        if (tests[i]) {
                            var j = seekNext(i);
                            var t = buffer[i];
                            buffer[i] = c;
                            if (j < len && tests[j].test(t))
                                c = t;
                            else
                                break;
                        }
                    }
                };

                function keydownEvent(e) {
                    var k = e.which;

                    //backspace, delete, and escape get special treatment
                    if (k == 8 || k == 46 || (iPhone && k == 127)) {
                        var pos = input.caret(),
							begin = pos.begin,
							end = pos.end;

                        if (end - begin == 0) {
                            begin = k != 46 ? seekPrev(begin) : (end = seekNext(begin - 1));
                            end = k == 46 ? seekNext(end) : end;
                        }
                        clearBuffer(begin, end);
                        shiftL(begin, end - 1);

                        return false;
                    } else if (k == 27) {//escape
                        input.val(focusText);
                        input.caret(0, checkVal());
                        return false;
                    }
                };

                function keypressEvent(e) {
                    var k = e.which,
						pos = input.caret();
                    if (e.ctrlKey || e.altKey || e.metaKey || k < 32) {//Ignore
                        return true;
                    } else if (k) {
                        if (pos.end - pos.begin != 0) {
                            clearBuffer(pos.begin, pos.end);
                            shiftL(pos.begin, pos.end - 1);
                        }

                        var p = seekNext(pos.begin - 1);
                        if (p < len) {
                            var c = String.fromCharCode(k);
                            if (tests[p].test(c)) {
                                shiftR(p);
                                buffer[p] = c;
                                writeBuffer();
                                var next = seekNext(p);
                                input.caret(next);
                                if (settings.completed && next >= len)
                                    settings.completed.call(input);
                            }
                        }
                        return false;
                    }
                };

                function clearBuffer(start, end) {
                    for (var i = start; i < end && i < len; i++) {
                        if (tests[i])
                            buffer[i] = settings.placeholder;
                    }
                };

                function writeBuffer() { return input.val(buffer.join('')).val(); };

                function checkVal(allow) {
                    //try to place characters where they belong
                    var test = input.val();
                    var lastMatch = -1;
                    for (var i = 0, pos = 0; i < len; i++) {
                        if (tests[i]) {
                            buffer[i] = settings.placeholder;
                            while (pos++ < test.length) {
                                var c = test.charAt(pos - 1);
                                if (tests[i].test(c)) {
                                    buffer[i] = c;
                                    lastMatch = i;
                                    break;
                                }
                            }
                            if (pos > test.length)
                                break;
                        } else if (buffer[i] == test.charAt(pos) && i != partialPosition) {
                            pos++;
                            lastMatch = i;
                        }
                    }
                    if (!allow && lastMatch + 1 < partialPosition) {
                        input.val("");
                        clearBuffer(0, len);
                    } else if (allow || lastMatch + 1 >= partialPosition) {
                        writeBuffer();
                        if (!allow) input.val(input.val().substring(0, lastMatch + 1));
                    }
                    return (partialPosition ? i : firstNonMaskPos);
                };

                input.data($.mask.dataName, function () {
                    return $.map(buffer, function (c, i) {
                        return tests[i] && c != settings.placeholder ? c : null;
                    }).join('');
                })

                if (!input.attr("readonly"))
                    input
					.one("unmask", function () {
					    input
							.unbind(".mask")
							.removeData($.mask.dataName);
					})
					.bind("focus.mask", function () {
					    focusText = input.val();
					    var pos = checkVal();
					    writeBuffer();
					    var moveCaret = function () {
					        if (pos == mask.length)
					            input.caret(0, pos);
					        else
					            input.caret(pos);
					    };
					    ($.browser.msie ? moveCaret : function () { setTimeout(moveCaret, 0) })();
					})
					.bind("blur.mask", function () {
					    checkVal();
					    if (input.val() != focusText)
					        input.change();
					})
					.bind("keydown.mask", keydownEvent)
					.bind("keypress.mask", keypressEvent)

                checkVal(); //Perform initial check for existing values
            });
        }
    });
})(jQuery);



///MaskMoney

/*
* maskMoney plugin for jQuery
* http://plentz.github.com/jquery-maskmoney/
* version: 2.1.2
* Licensed under the MIT license
*/
; (function ($) {
    if (!$.browser) {
        $.browser = {};
        $.browser.mozilla = /mozilla/.test(navigator.userAgent.toLowerCase()) && !/webkit/.test(navigator.userAgent.toLowerCase());
        $.browser.webkit = /webkit/.test(navigator.userAgent.toLowerCase());
        $.browser.opera = /opera/.test(navigator.userAgent.toLowerCase());
        $.browser.msie = /msie/.test(navigator.userAgent.toLowerCase());
    }

    var methods = {
        destroy: function () {
            var input = $(this);
            input.unbind('.maskMoney');

            if ($.browser.msie) {
                this.onpaste = null;
            }
            return this;
        },

        mask: function () {
            return this.trigger('mask');
        },

        init: function (settings) {
            settings = $.extend({
                symbol: '',
                symbolStay: false,
                thousands: ',',
                decimal: '.',
                precision: 2,
                defaultZero: true,
                allowZero: false,
                allowNegative: false
            }, settings);

            return this.each(function () {
                var input = $(this);
                var dirty = false;

                function markAsDirty() {
                    dirty = true;
                }

                function clearDirt() {
                    dirty = false;
                }

                function keypressEvent(e) {
                    e = e || window.event;
                    var k = e.which || e.charCode || e.keyCode;
                    if (k == undefined) return false; //needed to handle an IE "special" event
                    if (k < 48 || k > 57) { // any key except the numbers 0-9
                        if (k == 45) { // -(minus) key
                            markAsDirty();
                            input.val(changeSign(input));
                            return false;
                        } else if (k == 43) { // +(plus) key
                            markAsDirty();
                            input.val(input.val().replace('-', ''));
                            return false;
                        } else if (k == 13 || k == 9) { // enter key or tab key
                            if (dirty) {
                                clearDirt();
                                $(this).change();
                            }
                            return true;
                        } else if ($.browser.mozilla && (k == 37 || k == 39) && e.charCode == 0) {
                            // needed for left arrow key or right arrow key with firefox
                            // the charCode part is to avoid allowing '%'(e.charCode 0, e.keyCode 37)
                            return true;
                        } else { // any other key with keycode less than 48 and greater than 57
                            preventDefault(e);
                            return true;
                        }
                    } else if (canInputMoreNumbers(input)) {
                        return false;
                    } else {
                        preventDefault(e);

                        var key = String.fromCharCode(k);
                        var x = input.get(0);
                        var selection = getInputSelection(x);
                        var startPos = selection.start;
                        var endPos = selection.end;
                        x.value = x.value.substring(0, startPos) + key + x.value.substring(endPos, x.value.length);
                        maskAndPosition(x, startPos + 1);
                        markAsDirty();
                        return false;
                    }
                }

                function canInputMoreNumbers(element) {
                    var reachedMaxLenght = (element.val().length >= element.attr('maxlength') && element.attr('maxlength') >= 0);
                    var selection = getInputSelection(element.get(0));
                    var start = selection.start;
                    var end = selection.end;
                    var hasNumberSelected = (selection.start != selection.end && element.val().substring(start, end).match(/\d/)) ? true : false;
                    return reachedMaxLenght && !hasNumberSelected;
                }

                function keydownEvent(e) {
                    e = e || window.event;
                    var k = e.which || e.charCode || e.keyCode;
                    if (k == undefined) return false; //needed to handle an IE "special" event

                    var x = input.get(0);
                    var selection = getInputSelection(x);
                    var startPos = selection.start;
                    var endPos = selection.end;

                    if (k == 8) { // backspace key
                        preventDefault(e);

                        if (startPos == endPos) {
                            // Remove single character
                            x.value = x.value.substring(0, startPos - 1) + x.value.substring(endPos, x.value.length);
                            startPos = startPos - 1;
                        } else {
                            // Remove multiple characters
                            x.value = x.value.substring(0, startPos) + x.value.substring(endPos, x.value.length);
                        }
                        maskAndPosition(x, startPos);
                        markAsDirty();
                        return false;
                    } else if (k == 9) { // tab key
                        if (dirty) {
                            $(this).change();
                            clearDirt();
                        }
                        return true;
                    } else if (k == 46 || k == 63272) { // delete key (with special case for safari)
                        preventDefault(e);
                        if (x.selectionStart == x.selectionEnd) {
                            // Remove single character
                            x.value = x.value.substring(0, startPos) + x.value.substring(endPos + 1, x.value.length);
                        } else {
                            //Remove multiple characters
                            x.value = x.value.substring(0, startPos) + x.value.substring(endPos, x.value.length);
                        }
                        maskAndPosition(x, startPos);
                        markAsDirty();
                        return false;
                    } else { // any other key
                        return true;
                    }
                }

                function focusEvent(e) {
                    var mask = getDefaultMask();
                    if (input.val() == mask) {
                        input.val('');
                    } else if (input.val() == '' && settings.defaultZero) {
                        input.val(setSymbol(mask));
                    } else {
                        input.val(setSymbol(input.val()));
                    }
                    if (this.createTextRange) {
                        var textRange = this.createTextRange();
                        textRange.collapse(false); // set the cursor at the end of the input
                        textRange.select();
                    }
                }

                function blurEvent(e) {
                    if ($.browser.msie) {
                        keypressEvent(e);
                    }

                    if (input.val() == '' || input.val() == setSymbol(getDefaultMask()) || input.val() == settings.symbol) {
                        if (!settings.allowZero) {
                            input.val('');
                        } else if (!settings.symbolStay) {
                            input.val(getDefaultMask());
                        } else {
                            input.val(setSymbol(getDefaultMask()));
                        }
                    } else {
                        if (!settings.symbolStay) {
                            input.val(input.val().replace(settings.symbol, ''));
                        } else if (settings.symbolStay && input.val() == settings.symbol) {
                            input.val(setSymbol(getDefaultMask()));
                        }
                    }
                }

                function preventDefault(e) {
                    if (e.preventDefault) { //standard browsers
                        e.preventDefault();
                    } else { // old internet explorer
                        e.returnValue = false
                    }
                }

                function maskAndPosition(x, startPos) {
                    var originalLen = input.val().length;
                    input.val(maskValue(x.value));
                    var newLen = input.val().length;
                    startPos = startPos - (originalLen - newLen);
                    setCursorPosition(input, startPos);
                }

                function mask() {
                    var value = input.val();
                    input.val(maskValue(value));
                }

                function maskValue(v) {
                    v = v.replace(settings.symbol, '');

                    var strCheck = '0123456789';
                    var len = v.length;
                    var a = '', t = '', neg = '';

                    if (len != 0 && v.charAt(0) == '-') {
                        v = v.replace('-', '');
                        if (settings.allowNegative) {
                            neg = '-';
                        }
                    }

                    if (len == 0) {
                        if (!settings.defaultZero) return t;
                        t = '0.00';
                    }

                    for (var i = 0; i < len; i++) {
                        if ((v.charAt(i) != '0') && (v.charAt(i) != settings.decimal)) break;
                    }

                    for (; i < len; i++) {
                        if (strCheck.indexOf(v.charAt(i)) != -1) a += v.charAt(i);
                    }
                    var n = parseFloat(a);

                    n = isNaN(n) ? 0 : n / Math.pow(10, settings.precision);
                    t = n.toFixed(settings.precision);

                    i = settings.precision == 0 ? 0 : 1;
                    var p, d = (t = t.split('.'))[i].substr(0, settings.precision);
                    for (p = (t = t[0]).length; (p -= 3) >= 1; ) {
                        t = t.substr(0, p) + settings.thousands + t.substr(p);
                    }

                    return (settings.precision > 0)
					? setSymbol(neg + t + settings.decimal + d + Array((settings.precision + 1) - d.length).join(0))
					: setSymbol(neg + t);
                }

                function getDefaultMask() {
                    var n = parseFloat('0') / Math.pow(10, settings.precision);
                    return (n.toFixed(settings.precision)).replace(new RegExp('\\.', 'g'), settings.decimal);
                }

                function setSymbol(value) {
                    if (settings.symbol != '') {
                        var operator = '';
                        if (value.length != 0 && value.charAt(0) == '-') {
                            value = value.replace('-', '');
                            operator = '-';
                        }

                        if (value.substr(0, settings.symbol.length) != settings.symbol) {
                            value = operator + settings.symbol + value;
                        }
                    }
                    return value;
                }

                function changeSign(input) {
                    var inputValue = input.val();
                    if (settings.allowNegative) {
                        if (inputValue != '' && inputValue.charAt(0) == '-') {
                            return inputValue.replace('-', '');
                        } else {
                            return '-' + inputValue;
                        }
                    } else {
                        return inputValue;
                    }
                }

                function setCursorPosition(input, pos) {
                    // I'm not sure if we need to jqueryfy input
                    $(input).each(function (index, elem) {
                        if (elem.setSelectionRange) {
                            elem.focus();
                            elem.setSelectionRange(pos, pos);
                        } else if (elem.createTextRange) {
                            var range = elem.createTextRange();
                            range.collapse(true);
                            range.moveEnd('character', pos);
                            range.moveStart('character', pos);
                            range.select();
                        }
                    });
                    return this;
                };

                function getInputSelection(el) {
                    var start = 0, end = 0, normalizedValue, range, textInputRange, len, endRange;

                    if (typeof el.selectionStart == "number" && typeof el.selectionEnd == "number") {
                        start = el.selectionStart;
                        end = el.selectionEnd;
                    } else {
                        range = document.selection.createRange();

                        if (range && range.parentElement() == el) {
                            len = el.value.length;
                            normalizedValue = el.value.replace(/\r\n/g, "\n");

                            // Create a working TextRange that lives only in the input
                            textInputRange = el.createTextRange();
                            textInputRange.moveToBookmark(range.getBookmark());

                            // Check if the start and end of the selection are at the very end
                            // of the input, since moveStart/moveEnd doesn't return what we want
                            // in those cases
                            endRange = el.createTextRange();
                            endRange.collapse(false);

                            if (textInputRange.compareEndPoints("StartToEnd", endRange) > -1) {
                                start = end = len;
                            } else {
                                start = -textInputRange.moveStart("character", -len);
                                start += normalizedValue.slice(0, start).split("\n").length - 1;

                                if (textInputRange.compareEndPoints("EndToEnd", endRange) > -1) {
                                    end = len;
                                } else {
                                    end = -textInputRange.moveEnd("character", -len);
                                    end += normalizedValue.slice(0, end).split("\n").length - 1;
                                }
                            }
                        }
                    }

                    return {
                        start: start,
                        end: end
                    };
                } // getInputSelection

                if (!input.attr("readonly")) {
                    input.unbind('.maskMoney');
                    input.bind('keypress.maskMoney', keypressEvent);
                    input.bind('keydown.maskMoney', keydownEvent);
                    input.bind('blur.maskMoney', blurEvent);
                    input.bind('focus.maskMoney', focusEvent);
                    input.bind('mask.maskMoney', mask);
                }
            })
        }
    }

    $.fn.maskMoney = function (method) {
        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Method ' + method + ' does not exist on jQuery.maskMoney');
        }
    };
})(jQuery);
