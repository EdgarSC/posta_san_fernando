$(document).ready(function(){
	var objRequest = {};
    posta.get(
        'obtener_rol_agente_logueado',
        objRequest,
        function(response) {
        	ROL_AGENTE_ADMIN = response.ROL_AGENTE_ADMIN;
        	ROL_AGENTE_SUPER = response.ROL_AGENTE_SUPER;
        	ROL_AGENTE_CON = response.ROL_AGENTE_CON;
        	ROL_ADMINISTRADOR = response.ROL_PERMITIDO[0];        
        	ROL_SUPERVISOR = response.ROL_PERMITIDO[1];        
        	ROL_CONSULTOR = response.ROL_PERMITIDO[2];
        	/*$("#side_usuario").addClass("hidden");
        	$("#side_configuraciones").addClass("hidden");
        	$("#side_paciente").addClass("hidden");
        	$("#side_consultas").addClass("hidden");*/
        	if(ROL_AGENTE_ADMIN == ROL_ADMINISTRADOR){
        		$("#side_usuario").removeClass("hidden");
        		$("#side_configuraciones").removeClass("hidden");
        		$("#side_paciente").removeClass("hidden");
        		$("#side_consultas").removeClass("hidden");
        		$("#side_citas").removeClass("hidden");
        	}else if(ROL_AGENTE_CON == ROL_CONSULTOR){
        		$("#side_paciente").removeClass("hidden");
        		$("#side_consultas").removeClass("hidden");
        		$("#side_citas").removeClass("hidden");
        	}
        	/*else if(ROL_AGENTE_SUPER == ROL_SUPERVISOR){
        		
        	}*/
        }
     );
});