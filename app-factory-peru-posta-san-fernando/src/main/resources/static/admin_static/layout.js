//$('.sidebar-menu').tree();
//$('#usuarios').click(function() {
//	window.location = window.location.pathname;
//});
//document.addEventListener('DOMContentLoaded', function(){
//
//	var list_inputs = Array.from(document.querySelector('#form_agregar_servicio input'));
//	list_input.map(input => {
//		input.addEventListener('invalid', function(e){
//	    	e.preventDefault();
//		});
//	});
//});

// Métodos Generales
function messageSuccess(message){
	$.growl.notice(null, message);
}

function volverArriba(){
	$('html, body').animate({scrollTop : 0},600);
}

function messageError(message){
	$.growl.error(null, 'Error', message);
}

var posta = {
	// Method POST
	post: function(url, data, function_exec){
		var aLoading = $("").cowaitscreen({modal: true});
		aLoading.show();
		
		return $.ajax({ url : ip + url, data,
			cache : false,
			method : 'POST',
		}).done(function(response){
			function_exec(response);
		}).always(function(){
			aLoading.close();
		});
	},
	// Method GET
	get: function(url, data, function_exec){
		var aLoading = $("").cowaitscreen({modal: true});
		aLoading.show();
		
		return $.ajax({ url : ip + url, data,
			cache : false,
			method : 'GET',
		}).done(function(response){
			function_exec(response);
		}).always(function(){
			aLoading.close();
		});
	}
	
};

/*En la vista debe crearse un div asi: 
	<div class="col-sm-12" id="id_contenedor" style="display: none;">&nbsp;</div><br>
	
para que la alerta funcione. 
*/
function mostrarMensajeTemporal(id_contenedor, color_mensaje, texto_mensaje, segundos){
	var color_alerta = "";
	if(color_mensaje == "verde")
		color_alerta = "alert-success";
	else if(color_mensaje == "amarillo")
		color_alerta = "alert-warning";
	else if(color_mensaje == "rojo")
		color_alerta = "alert-danger";
	
	var milisegundos = parseFloat(segundos * 1000);
		
	$("#"+id_contenedor).empty().append('<div class="alert '+color_alerta+'" role="alert">'+texto_mensaje+'</div>').fadeIn('medium');
	setTimeout(
		function(){
			$("#"+id_contenedor).fadeOut('medium');
	  	}, milisegundos);
}