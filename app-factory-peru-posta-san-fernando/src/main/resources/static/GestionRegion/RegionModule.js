
function initDepartamentos(){
	listarDepartamentos();
}

function listarDepartamentos(){
	return posta.get('regiones/listar_departamentos', {} , function(departamentos){
		$('.select_departamento').empty().append('<option value="-1">[seleccione]</option>');
		departamentos.forEach(departamento => $('.select_departamento').append(`<option value="${departamento.regionNumero}">${departamento.nombre}</option>`));
		$('.select_departamento').change(function(){
			listarProvincia(this.value);
		});
	});
}

function listarProvincia(departamento){
	return posta.get('regiones/listar_provincias', { departamento }, function(provincias){
		$('.select_provincia').empty().append('<option value="-1">[seleccione]</option>');
		provincias.forEach(provincia => $('.select_provincia').append(`<option value="${provincia.regionNumero}">${provincia.nombre}</option>`));
		$('.select_provincia').change(function(){
			listarDistrito(this.value);
		});
	});
}

function listarDistrito(provincia){
	return posta.get('regiones/listar_distritos', { provincia }, function(distritos){
		$('.select_distrito').empty().append('<option value="-1">[seleccione]</option>');
		distritos.forEach(distrito => $('.select_distrito').append(`<option value="${distrito.regionId}">${distrito.nombre}</option>`));
	});
}

function setSelectRegiones(form, departamento, provincia, distrito){
	form.querySelector('.select_departamento').value = departamento;
	listarProvincia(departamento).then(function(){
		form.querySelector('.select_provincia').value = provincia;
		listarDistrito(provincia).then(function(){
			form.querySelector('.select_distrito').value = distrito;
		});
	});
}

	