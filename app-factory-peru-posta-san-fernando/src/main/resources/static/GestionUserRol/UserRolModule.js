var table_userRol;
var actualizar_id_userRol = -1;
function init() {
    listarUserRol();
    listar_usuarios("#nw_ur_usuario");
    listar_roles("#nw_ur_rol");
    listar_usuarios("#upd_ur_usuario");
    listar_roles("#upd_ur_rol");
}

function listarUserRol() {
    table_userRol = $("#table_userRol").DataTable({
        responsive: true,
        "ajax": {
            "url": ip + "rolUser/listar_userRol",
            "dataSrc": ""
        },
        "columns": [
            { data: "usuario.usuario" },
            { data: "rol.name" },
            {
                data: null,
                className: "dt-center",
                render: function(data) {
                    var id_userRol = data.userRolId;
                    var estado_userRol = data.estado;
                    var rol_userRol = data.rol.name;
                    var usuario_userRol = data.usuario.usuario;
                    if (estado_userRol == 1) {
                        return '<a href="#" onclick="cambiar_estado_usuario_roles(`'+id_userRol+'`,`'+estado_userRol+'`,`'+usuario_userRol+'`,`'+rol_userRol+'`)"><i class="fa fa-toggle-on green font-md"></i></a>';
                    } else {
                        return '<a href="#" onclick="cambiar_estado_usuario_roles(`'+id_userRol+'`,`'+estado_userRol+'`,`'+usuario_userRol+'`,`'+rol_userRol+'`)"><i class="fa fa-toggle-off red font-md"></i></a>';
                    }
                }
            },
            { data: null,
              className: "dt-center",
              render: function(data){
              	var id_userRol = data.userRolId;
              	var usuarioId_userRol = data.usuario.usuarioId;
              	var rolId_userRol = data.rol.rolId;
              	return '<button type="button" class="btn btn-sm" onclick="ver_actualizar_userRol(`'+id_userRol+'`,`'+usuarioId_userRol+'`,`'+rolId_userRol+'`)"><i class="fa fa-edit style="font-size:80px" "></i></button>';
              }
            }
        ],
        language
    });
}

function cambiar_estado_usuario_roles(id_userRol, estado_userRol, usuario, rol) {
    var objRequest = {
        id_userRol: id_userRol,
        estado: estado_userRol
    };
    posta.post(
        'rolUser/cambiar_estado_rolUser',
        objRequest,
        function(response) {
            if (response == "Listo") {
                table_userRol.ajax.reload();
                var estado = estado_userRol == 1 ? "deshabilitó " : "habilitó ";
                mostrarMensajeTemporal("contenedor_mensaje_userRol", "verde", "Se "+estado+" el rol "+ rol+" al usuario "+usuario+" con éxito!", 6);
            } else {
               mostrarMensajeTemporal("contenedor_mensaje_userRol", "rojo", "Ocurrió un error, contacte con el administrador.", 6);
            }
        }
    );
}

function listar_usuarios(_usuarios) {
    posta.get(
        'usuario/listar_usuarios_activos',
        objRequest = {},
        function(response) {
            if (response.length >0) {
               for(var i=0; i<response.length;i++ ){
               		$(_usuarios).append('<option value="'+response[i].usuarioId+'">'+response[i].nombre+'</option>');
               }
            } else {
               mostrarMensajeTemporal("contenedor_mensaje_add_userRol", "rojo", "Ocurrió un error, contacte con el administrador.", 6);
            }
        }
    );
}

function listar_roles(_roles) {
    posta.get(
        'rol/listar_roles_activos',
        objRequest = {},
        function(response) {
            if (response.length >0) {
               for(var i=0; i<response.length;i++ ){
               		$(_roles).append('<option value="'+response[i].rolId+'">'+response[i].name+'</option>');
               }
            } else {
               mostrarMensajeTemporal("contenedor_mensaje_add_userRol", "rojo", "Ocurrió un error, contacte con el administrador.", 6);
            }
        }
    );
}

function agregarRolUser() {
    if ($("#nw_ur_usuario").val() == "-1") {
        mostrarMensajeTemporal("contenedor_mensaje_add_userRol", "amarillo", "Seleccione Usuario.", 3);
        return;
    }
    if ($("#nw_ur_rol").val() == "-1") {
        mostrarMensajeTemporal("contenedor_mensaje_add_userRol", "amarillo", "Seleccione Rol.", 3);
        return;
    }
    posta.post(
        'rolUser/guardar_rolUser',
        data= {
	        id_user: $("#nw_ur_usuario").val(),
	        id_rol: $("#nw_ur_rol").val()
	    },
        function(response) {
        	if(response == "Listo"){               		 	
	            table_userRol.ajax.reload();
	            $("#nw_ur_usuario").val("");
	            $("#nw_ur_rol").val("");
	            mostrarMensajeTemporal("contenedor_mensaje_userRol", "verde", "Se registró con éxito!", 4);
	        }else{
	     		mostrarMensajeTemporal("contenedor_mensaje_userRol", "rojo", "Ocurrió un error, contacte con el administrador.", 6);	
	        }
        }
    );
}

function ver_actualizar_userRol(id, usuario, rol){
	$("#modal_actualizar_userRol").modal('show');
	$("#upd_ur_usuario").val(usuario);
	$("#upd_ur_rol").val(rol);
	actualizar_id_userRol = id;
}

function actualizar_userRol(){
	posta.post(
        'rolUser/actualizar_rolUser',
        data = {
        	id_userRol: actualizar_id_userRol,
	        id_user: $("#upd_ur_usuario").val(),
	        id_rol: $("#upd_ur_rol").val()
        },
        function(response) {
        	if(response == "Listo"){        
        		table_userRol.ajax.reload();
        		$("#modal_actualizar_userRol").modal('hide');
	            mostrarMensajeTemporal("contenedor_mensaje_userRol", "verde", "Se actualizó con éxito!", 4);
	        }else{
	     		mostrarMensajeTemporal("contenedor_mensaje_userRol", "rojo", "Ocurrió un error, contacte con el administrador.", 6);	
	        }
        }
    );
}