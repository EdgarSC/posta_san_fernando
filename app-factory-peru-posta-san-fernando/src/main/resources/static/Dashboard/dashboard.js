var table_citas_pendientes;
$(document).ready(function(){
	vistarXroles();
	listarCitasNoAtendidas();
});

function vistarXroles(){

	var objRequest = {};
    posta.get(
        'cantidad_total_usuarios',
        objRequest,
        function(response) {
	        var users = response.usuarios_totales;
	        var pacientes = response.pacientes_totales;
	        var consultas = response.consultas_totales;
	        var citas = response.citas_totales;
	        var user_admin = 0;
	        var user_consultor = 0;
	        var user_total = 0;
	        for(var i=0; i< users.length; i++){
	        	user_admin = users[i][0];
	        	user_consultor = users[i][1];
	        	user_total = users[i][0] + users[i][1];
	        }
	        $("#total_usuarios").append(user_total);
	        $("#total_admin").append(user_admin);
	        $("#total_consult").append(user_consultor);	   
	        $("#total_pacientes").append(pacientes);
	        $("#total_consultas").append(consultas);
	         $("#total_citas").append(citas);
        }
    );
}

function listarCitasNoAtendidas(){
    table_citas_pendientes = $("#table_citas_pendientes").DataTable({
        responsive : true,
        ajax:{
            url: ip + "citas/listar_citas_no_atendidas",
            type: "GET",
		    "dataSrc": ""
        },
        columns: [
        	{ data: 'paciente.numero_documento'},
        	{ data: null,
			   render: function ( data, type, row ) {
			     return row.paciente.nombre + ' ' + row.paciente.apellido_paterno + ' ' + row.paciente.apellido_materno;
		    }},		    
        	{ data: null,
			   render: function ( data, type, row ) {
			     return row.fecha_cita + ' ' + row.hora_cita;
		    }},
        	{ data: null,
			   render: function ( data, type, row ) {
			     return row.celular_paciente;
		    }},
        	{ data: null,
			   render: function ( data, type, row ) {
			     return row.interconsulta ? 'Si' : 'No'; 
		    }},
		    { data: 'servicio.nombre'},
        	{ data: null,
			   render: function ( data, type, row ) {
			   		var id_cita = row.id_cita;
                    var atendido_cita = row.atentido;
			     if (atendido_cita == 1) 
                    return '<label type="button" class="btn btn-sm" onclick="cambiar_atendido_cita(`'+id_cita+'`,`'+atendido_cita+'`)"><i class="fa fa-toggle-on green font-md"></i></label>';
                else 
                    return '<label type="button" class="btn btn-sm" onclick="cambiar_atendido_cita(`'+id_cita+'`,`'+atendido_cita+'`)"><i class="fa fa-toggle-off red font-md"></i></label>';
		    }}
        ],
        language
    });
}

function cambiar_atendido_cita(id_cita, atendido_cita) {
    var objRequest = {
        id_cita: id_cita,
        atendido_cita: atendido_cita
    };
    posta.post(
        'citas/cambiar_atendido_cita',
        objRequest,
        function(response) {
            if (response == "Listo") {
                table_citas_pendientes.ajax.reload();
                mostrarMensajeTemporal("contenedor_cita_pendiente", "verde", "Se pasó atender la cita.", 4);
            } else {
               mostrarMensajeTemporal("contenedor_cita_pendiente", "rojo", "Ocurrió un error, contacte con el administrador.", 6);
            }
        }
    );
}