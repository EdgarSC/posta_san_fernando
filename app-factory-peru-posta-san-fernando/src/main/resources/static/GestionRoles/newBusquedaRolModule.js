
function init(){
	listarRoles();
}

function listarRoles(){
	console.log("ROLES")
	var prRolDTO = {
		estado : $('#id_rol_estado').val(),
		name: $('#id_rol_nombre').val(),
		descripcion : $('#id_rol_descripcion').val()	
	}
	
	posta.post('rol/rolList', 
		prRolDTO, 
		function(response){
		$('#table_rol  tbody').empty();
		response.forEach(rol => {
			var btn_status = (rol.estado == 1) 
							? '<span style="cursor:pointer" class="badge badge-Active">' + rol.estadoDescripcion + '</span>' 
							: '<span style="cursor:pointer" class="badge badge-Inactive">' + rol.estadoDescripcion + '</span>';
							
			$('#table_rol tbody').append(`
			<tr>
				<td>${rol.name}</td>
				<td>${rol.descripcion}</td>
				<td>${rol.createdDescripcion}</td>
				<td class="text-center">
					<button class="btn" onclick="abrirModalEditarRol(rol)"><i class="fa fa-edit"></i></button>
				</td>
			</tr>`);
		});
	})
}

function abrirModalEditarRol(rol){
	$('#modal_name_editar_rol').val(rol.name);
	$('#modal_descripcion_editar_rol').val(rol.descripcion);
	$('#modal_editar_rol').modal('show');
}

function limpiarCampos(){
	
}

function agregarRol(){
	var prRolDTO = {
		name: $('#modal_name_agregar_rol').val(),
		descripcion : $('#modal_descripcion_agregar_rol').val()
	}
	
	posta.post('rol/rolAdd',
		{ prRolDTO : JSON.stringify(prRolDTO)},
		function(response){
			if(response){
				$('#modal_agregar_rol').modal('hide');
				messageSuccess('El rol fue registrado con éxito.');
				listarRoles();
			}
	});
}
