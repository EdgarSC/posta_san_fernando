var BusquedaRolModule = (function () {
	var obj = this;
	globalFunctions.extendKORequiredString(ko);
    globalFunctions.configTools(ko);
    
    var config = function (prSearchRoles) {
        ko.applyBindings(obj, document.getElementById(prSearchRoles));
        $("").showDiv({ section: prSearchRoles, divId: 'RolesSearchMain' });
    };
    
    var KDStatus = {
    		Inactivo: 0,
    		Activo: 1
    }
    
    var aBusquedaRolEntityCollection =  ko.observableArray([]);
    
    var FilterRolDTO = function(){
    	
    	var _estado = ko.observable(-1);
    	var _name = ko.observable("");
    	var _descripcion = ko.observable("");
    	
    	return{
    		estado: _estado,
    		name: _name,
    		descripcion: _descripcion
    	}
    }
    
    var aFilterRolDTO = new FilterRolDTO();
    
    var bto_searchRoles = function (){
    	searchRoles();
    }
    
    var bto_newRol = function (){
    	newRol();
    }
    
    var bto_limpiar = function(){
    	clearFields();
    }
    
    var bto_showRol = function(prIndex, data){
    	ShowRol(prIndex, data);
    }
    
    var btn_ActivateDesactivate_Click = function (prIndex, data) { activateOrDesactivateRol(prIndex, data); }
    
    var ShowRol = function (prIndex, prData) {

        var onSucessLoad = function() {
            $("").showDiv({ section: 'secBusquedaRol', divId: 'divCallRol' });
            aRolModule.configView("divRolMain");
            aRolModule.setRol(prData, prIndex);

        };

        $("").coLoadDiv({
            action: "/rol/rolView",
            cbhSucess: onSucessLoad,
            splashscreen: true,
            elementID: "#divCallRol"
        }).callService();
    };
    
    var searchRoles = function (){
    	
    	var cbhSucess = function (data) {
    		aBusquedaRolEntityCollection([]);
    		aBusquedaRolEntityCollection(data);
        }

        var cbhError = function (data) {
            $.growl.error(null, 'Error', data.responseText);
        }

        var lParameter = "prRolDTO="+ko.mapping.toJSON(aFilterRolDTO);
        
    	 $("").coserver({
             action: "/rol/rolList",
             splashscreen: true,
             cbhSucess: cbhSucess,
             cbhError: cbhError
         }).callService(lParameter);
    }
    
    var newRol = function(){
    	
    	var onSucessLoad = function(){
             $("").showDiv({ section: 'secBusquedaRol', divId: 'divCallRol' });
             aRolModule.configView("divRolMain");
    	}
    	
    	 $("").coLoadDiv({
             action: "/rol/rolView",
             cbhSucess: onSucessLoad,
             splashscreen: true,
             elementID: "#divCallRol"
         }).callService();
    }
    
    var activateOrDesactivateRol = function (prIndex, data) {

    	var setSuccess = function (dataRol) {
    		searchRoles();
            $.growl.notice(null, 'Éxito', 'Rol actualizado con éxito.');
        }

        var lParametros = "prRolId=" + data.rolId;
        $("").coserver({
            action: "/rol/activateOrDesactivateRol", splashscreen: true, cbhSucess: setSuccess, cbhError: onErrorRol
        }).callService(lParametros);
    }
    
    var onErrorRol = function (data) {
        $.growl.error(null, 'Error', (data[0] == null ? data.responseText : data[0].responseText));
    };
    
    var clearFields = function(){
    	aFilterRolDTO.estado(-1);

    }
	
	return {
		 config: config,
		 bto_searchRoles: bto_searchRoles,
		 bto_newRol:bto_newRol,
		 KDStatus:KDStatus,
		 aBusquedaRolEntityCollection:aBusquedaRolEntityCollection,
		 aFilterRolDTO: aFilterRolDTO,
		 bto_limpiar:bto_limpiar,
		 bto_showRol:bto_showRol,
		 btn_ActivateDesactivate_Click:btn_ActivateDesactivate_Click
	}
	
});