var LoginModule = (function() {
	var obj = this;

	globalFunctions.extendKORequiredString(ko);
	globalFunctions.configTools(ko);

    var config = function (prLoginMain) {
        ko.applyBindings(obj, document.getElementById(prLoginMain));
        //$("").showDiv({ section: prSearchUsuarios, divId: 'UsersSearchMain' });
    };
    
    var UsuarioDTO = function() {

		var _usuarioId = ko.observable(0);
		var _usuario = ko.observable("");
		var _contrasena = ko.observable("");
		var _confirmacionContrasena = ko.observable("");
		var _nombre = ko.observable("");
		var _apellidoPaterno = ko.observable("");
		var _apellidoMaterno = ko.observable("");
		var _tipoDocumento = ko.observable(-1);
		var _numeroDocumento = ko.observable("");
		var _fechaNacimientoDescripcion = ko.observable("");
		var _correo = ko.observable("");
		var _telefono = ko.observable("");
		var _celular = ko.observable("");
		var _estado = ko.observable(1);
		var _creator = ko.observable("");
		var _createdDescripcion = ko.observable("");

		return {
			usuarioId : _usuarioId,
			usuario : _usuario,
			contrasena : _contrasena,
			confirmacionContrasena : _confirmacionContrasena,
			nombre : _nombre,
			apellidoPaterno : _apellidoPaterno,
			apellidoMaterno : _apellidoMaterno,
			tipoDocumento : _tipoDocumento,
			numeroDocumento : _numeroDocumento,
			fechaNacimientoDescripcion : _fechaNacimientoDescripcion,
			correo : _correo,
			telefono : _telefono,
			celular : _celular,
			creator : _creator,
			estado : _estado,
			createdDescripcion : _createdDescripcion
		}
	}
    
    var aUsuarioDTO = new UsuarioDTO();
    
    var bto_login = function (){ login ();} 
    
    var login = function (){
    	alert("usuario: " + aUsuarioDTO.usuario());
    }
    
    return {
    	config: config,
    	aUsuarioDTO: aUsuarioDTO,
    	bto_login: bto_login
    }
});