var table_consultas;
var obj_paci = null;
var posicion_tabla_receta = 1;
var posicion_tabla_diagnostico = 1;

var listado_servicios = null;
var ultima_consulta_paciente = null;

var diagnosticos_c = null, medicamentos_c = null;

function iniciarConsultas(){
	
	/*Fecha actual*/
	var today = new Date();
	var dd = String(today.getDate()).padStart(2, '0');
	var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
	var yyyy = today.getFullYear();
	today = mm + '/' + dd + '/' + yyyy;
	$("#lbl_fecha_actual_receta").empty().append("Fecha de prescripci\u{F3}n: "+today);
	
	/*Generación de inputs tabla receta y diagnostico*/
	for (var i = 0; i < 3; i++) {
		llenarInputTablaReceta();
		
		if(i<2)
			llenarInputTablaDiagnostico();
	}
	
	/*Segundo contenedor desactivado*/
	$("#contenedor_info_paciente_nueva_consulta *").attr("disabled", "disabled").off('click');
	
	/*LLenando listado de servicios y consultas */
	listarServicios();
    listarConsultas("##vacio##");

	/*Botones gestión*/
	$("#btn_activar_buscador_nueva_consulta").on("click",function(){
		if($("#contenedor_busqueda_dni").is(":visible")){
			mostrarContenedor(false, "contenedor_busqueda_dni");
		}else{
			mostrarContenedor(true, "contenedor_busqueda_dni");
		}
		listarServicios();
	})
	$("#busqueda_btn_dni").on("click",function(){
		obtenerPaciente();
	})
	
	$("#btn_nueva_consulta_paciente").on("click",function(){
		mostrarInfoFiliacionBasica();
	})
	
	$("#btn_cancelar_registro_nueva_consulta").on("click",function(){
		obj_paci = null;
		listarConsultas("##vacio##");
		$("#busqueda_input_dni").val('');
		$("#contenedor_registro_consulta").fadeOut('medium');
		
		llenarInfoPrimerPanelPaciente();
		llenarInfoSegundoPanelPaciente();
		mostrarContenedor(true, "contenedor_listar_consulta");
		mostrarContenedor(false, "contenedor_info_nueva_consulta");
		
		limpiarCamposModalReceta();
	})
	
	/* Posteriormente limpiar la info registrada en este modal... */	
	$("#btn_adicionar_info_receta").on("click",function(){
		if(obj_paci == null){
			mostrarMensajeTemporal("contenedor_nueva_consulta_mensaje", "amarillo", "Error al obtener los datos del paciente.", 4);
		}else{
			var html_datos_paciente = "Paciente: <b>"+(obj_paci.nombre == null ? "" : obj_paci.nombre) + " " + (obj_paci.apellido_paterno == null ? "" : obj_paci.apellido_paterno) + " " + (obj_paci.apellido_materno == null ? "" : obj_paci.apellido_materno)+"</b> — "+
			 							"Edad: <b>"+obj_paci.edad+" a\u{F1}os</b> — Peso: <b>"+obj_paci.peso+" kgs</b> — Talla: <b>"+obj_paci.talla+" cms.</b>";
			$("#lbl_titulo_receta_paciente").empty().append(html_datos_paciente);
			
			$("#modal_grabar_receta_consulta").modal('show');
		}
	})
	$("#btn_adicionar_fila_receta").on("click",function(){
		llenarInputTablaReceta();
	})
	$("#btn_adicionar_fila_diagnostico").on("click",function(){
		llenarInputTablaDiagnostico();
	})
	$("#btn_limpiar_modal_receta").on("click",function(){
		limpiarCamposModalReceta();
	})
	$("#btn_guardar_nueva_consulta").on("click",function(){
		grabarConsultaPaciente();
	})
	
	$("#btn_grabar_receta").on("click",function(){
		var panel_03 = verificarInfo03Receta();
		if(panel_03 != ''){
			mostrarMensajeTemporal("contenedor_receta_mensaje", "amarillo", panel_03, 4);
		}else{
			$("#modal_grabar_receta_consulta").modal('hide');
		}
	})
	
	
	$("#consulta_talla").add($("#consulta_peso")).on("keyup",function(){
		calculandoIMC();
	})
	
	$("#id_limpiar_seleccion_servicios").on("click",function(){
		llenarSelectServicios();
	})
}

function calculandoIMC(){
	var imc_val = 0;
	var peso_pac = $("#consulta_peso").val().trim();
	var talla_pac = $("#consulta_talla").val().trim();
	
	if(talla_pac != '' && peso_pac != '' && talla_pac > 0 && peso_pac > 0){
		/*IMC = [peso(Kg)/ (talla(m)^2)] (peso en kilogramos entre el cuadrado de la talla en metros).*/
		imc_val = parseFloat(peso_pac) / (parseFloat(talla_pac) * parseFloat(talla_pac));
	}
	$("#consulta_imc").val(imc_val);
}

function grabarConsultaPaciente(){
//	var panel_01 = verificarInfo01Personales();
	var panel_02 = verificarInfo02Consulta();
	var panel_03 = verificarInfo03Receta();
//	if(panel_01 != ''){
//		volverArriba();
//		mostrarMensajeTemporal("contenedor_nueva_consulta_mensaje", "amarillo", panel_01 + " (Panel <b>PERSONALES GINECOLOGICOS</b>)", 4); return;
//	}
	if(panel_02 != ''){
		volverArriba();
		mostrarMensajeTemporal("contenedor_nueva_consulta_mensaje", "amarillo", panel_02 + " (Panel <b>CONSULTA</b>)", 4); return;
	}
	if(panel_03 != ''){
		volverArriba();
		mostrarMensajeTemporal("contenedor_nueva_consulta_mensaje", "amarillo", panel_03 + " (Opci\u{F3}n <b>Recetas</b>)", 4); return;
	}
	
	var diagnosticos = [];
	for (var i = 0; i <= posicion_tabla_diagnostico; i++) {
		if($('#rc_diagnostico_'+i).length && $('#rc_diagnostico_'+i).val().trim() != ''){
			var diag_consulta = $('#rc_diagnostico_'+i).val().trim();
			var cie_consulta = ($('#rc_cie_'+i).val().trim() == "" ? "-" : $('#rc_cie_'+i).val().trim());
			diagnosticos.push({diag_consulta, cie_consulta});
		}
	}
	
	var recetas = [];
	for (var i = 0; i <= posicion_tabla_receta; i++) {
		if($('#input_'+i+'_medicamento').length && $('#input_'+i+'_medicamento').val().trim() != ''){
			var medicamento = $('#input_'+i+'_medicamento').val().trim();
			var concentraci = $('#input_'+i+'_concent').val().trim();
			var ff = $('#input_'+i+'_ff').val().trim();
			var cantidad = $('#input_'+i+'_cantidad').val().trim();
			var dosis = $('#input_'+i+'_dosis').val().trim();
			var via = $('#input_'+i+'_via').val().trim();
			var frecuencia = $('#input_'+i+'_frecuencia').val().trim();
			var duracion = $('#input_'+i+'_duracion').val().trim();
			
			recetas.push({medicamento, concentraci, ff, cantidad, dosis, via, frecuencia, duracion,});
		}
	}
	
	var ids_seleccionados_servicios = "";
	var values = $('#consulta_interconsulta').val();
	if(values != null){
		for (var i = 0; i < values.length; i++) {
			ids_seleccionados_servicios += values[i] + ";";
		}
	}
	
	var objRequest = {
			js_diagnosticos: JSON.stringify(diagnosticos),
			js_recetas: JSON.stringify(recetas),
			
			id_pac: obj_paci.id_paciente,
			talla_pac: $('#consulta_talla').val(),
			peso_pac: $('#consulta_peso').val().trim(),
			imc_pac: $('#consulta_imc').val().trim(),
			pres_art_pac: $('#consulta_pres_arterial').val().trim(),
			
			frec_card_pac: $('#consulta_frec_cardiaca').val().trim(),
			temp_pac: $('#consulta_temperatura').val().trim(),
			frec_resp_pac: $('#consulta_frec_respiratoria').val().trim(),
			satur_pac: $('#consulta_saturacion').val().trim(),
			ids_servicios: ids_seleccionados_servicios,
			
			motivo_consulta_pac: $('#consulta_motivo').val().trim(),
			plan_trabajo_pac: $('#consulta_plan_trabajo').val().trim(),
			menarquia_pac: $('#nw_menarquia').val().trim(),
			gestaciones_pac: $('#nw_numero_embarazos').val().trim(),
			partos_pac: $('#nw_cantidad_hijos').val().trim(),
			
			reg_catam_pac: $('#nw_regimen_cetamenial').val().trim(),
			met_anticon_pac: $('#nw_metodo_anti').val().trim(),
			fecha_ult_reg_pac: $('#nw_fecha_ult_reg').val().trim(),
			//orient_sex_pac: $('#nw_orientacion_sexual').val().trim(),
			nro_parejas_sex_pac: $('#nw_nro_parejas_sexuales').val().trim(),
			
			fecha_expiracion_receta: $('#rc_fecha_expiracion').val().trim()
		};
	posta.post(
			'consultas/grabar_consulta', 
			objRequest,
			function(response) {
				if(response.startsWith("(a)")){
					mostrarMensajeTemporal("contenedor_nueva_consulta_mensaje", "amarillo", response+"", 5); return;
				}else{
					mostrarMensajeTemporal("contenedor_nueva_consulta_mensaje", "verde", response+"", 5);
					$('#contenedor_registro_de_consulta_01').empty();
					$('#contenedor_registro_de_consulta_02').empty();
					$('#contenedor_registro_de_consulta_03').empty();
					$('#contenedor_registro_de_consulta_04').empty();
					setTimeout(
						function(){
							location.reload();
					  	}, (2*1000));
					
				}
			}
		);
}

function verificarInfo01Personales(){
	if(obj_paci != null && obj_paci.sexo == 1){
		if($('#nw_menarquia').val().trim() == '')
			return "Ingrese menarqu\u{ED}a.";
		if($('#nw_numero_embarazos').val().trim() == '')
			return "Ingrese n\u{FA}mero de embarazos.";
		if($('#nw_cantidad_hijos').val().trim() == '')
			return "Ingrese cantidad de hijos.";
		if($('#nw_regimen_cetamenial').val().trim() == '')
			return "Ingrese r\u{E9}gimen certamenial.";
		if($('#nw_metodo_anti').val().trim() == '')
			return "Ingrese m\u{E9}todo anticonceptivo.";
		if($('#nw_fecha_ult_reg').val().trim() == '')
			return "Seleccione fecha \u{FA}ltima regla.";
		if($('#nw_orientacion_sexual').val().trim() == '-1')
			return "Seleccione orientaci\u{F3}n sexual.";
		if($('#nw_nro_parejas_sexuales').val().trim() == '')
			return "Ingrese n\u{FA}mero de parejas sexuales.";
	}
	return "";
}

function verificarInfo02Consulta(){
	if(obj_paci != null){
		if($('#consulta_talla').val().trim() == '')
			return "Ingrese talla paciente.";
		
		if($('#consulta_peso').val().trim() == '')
			return "Ingrese peso paciente.";
		
		if($('#consulta_imc').val().trim() == '')
			return "Ingrese IMC paciente.";
		
//		if($('#consulta_pres_arterial').val().trim() == '')
//			return "Ingrese presi\u{F3}n arterial.";
//		
//		if($('#consulta_frec_cardiaca').val().trim() == '')
//			return "Ingrese frecuencia cardiaca.";
//		
//		if($('#consulta_temperatura').val().trim() == '')
//			return "Ingrese temperatura..";
//		
//		if($('#consulta_frec_respiratoria').val().trim() == '')
//			return "Ingrese frecuencia respiratoria.";
//		
//		if($('#consulta_saturacion').val().trim() == '')
//			return "Ingrese saturaci\u{F3}n.";
//		
//		if($('#consulta_interconsulta').val().trim() == '-1')
//			return "Seleccione interconsulta.";
		
		if($('#consulta_motivo').val().trim() == '')
			return "Ingrese motivo consulta.";
			
		if($('#consulta_plan_trabajo').val().trim() == '')
			return "Ingrese plan de trabajo.";
	}
	return "";
}

function verificarInfo03Receta(){
	if(obj_paci != null){
		if($('#rc_fecha_expiracion').val().trim() == '')
			return "Seleccione fecha de expiraci\u{F3}n.";
		
		/*Validando info diagnostico*/
		if($('#rc_diagnostico_1').val().trim() == '')
			return "Debe ingresar al menos un diagnostico.";
			
//		for (var i = 0; i <= posicion_tabla_diagnostico; i++) {
//			if($('#rc_diagnostico_'+i).length && $('#rc_diagnostico_'+i).val().trim() != '' && $('#rc_cie_'+i).val().trim() == '')
//				return "Ingrese CIE-10 del diagnostico " + i + ".";
//		}
		
		/*Validando info receta*/
		if($('#input_1_medicamento').val().trim() == '')
			return "Debe ingresar al menos un medicamento.";
		
		for (var i = 0; i <= posicion_tabla_receta; i++) {
			if($('#input_'+i+'_medicamento').length && $('#input_'+i+'_medicamento').val().trim() != ''){
				if($('#input_'+i+'_concent').val().trim() == '')
					return "Ingrese concetraci\u{F3}n del medicamento " + i + ".";
				
				if($('#input_'+i+'_ff').val().trim() == '')
					return "Ingrese FF del medicamento " + i + ".";
					
				if($('#input_'+i+'_cantidad').val().trim() == '')
					return "Ingrese cantidad del medicamento " + i + ".";
					
				if($('#input_'+i+'_dosis').val().trim() == '')
					return "Ingrese dosis del medicamento " + i + ".";
					
				if($('#input_'+i+'_via').val().trim() == '')
					return "Ingrese v\u{ED}a del medicamento " + i + ".";
					
				if($('#input_'+i+'_frecuencia').val().trim() == '')
					return "Ingrese frecuencia del medicamento " + i + ".";
					
				if($('#input_'+i+'_duracion').val().trim() == '')
					return "Ingrese duraci\u{F3}n del medicamento " + i + ".";
			}
		}
	}
	return "";
}

function limpiarCamposModalReceta(){
	/*Limpiar modal receta*/
		$('#tbl_listado_receta tbody').empty();
		$('#tbl_listado_diagnostico tbody').empty();
		$("#rc_fecha_expiracion").val('');
		posicion_tabla_receta = 1;
		posicion_tabla_diagnostico = 1;

		for (var i = 0; i < 3; i++) {
			llenarInputTablaReceta();
			
			if(i<2)
				llenarInputTablaDiagnostico();
		}
}

function llenarInputTablaDiagnostico(){
	var html_boton_eliminar = (posicion_tabla_diagnostico == 1 ? "&nbsp;" : "<i onclick='eliminarFilaTablaDiagnostico(`"+posicion_tabla_diagnostico+"`);' class='fa fa-minus-circle' title='Eliminar fila..."+posicion_tabla_diagnostico+"'></i>");
	var html="<tr id='tr_diagnostico_"+posicion_tabla_diagnostico+"'>"+
				"<td>"+
					"<div class='form-row'>"+
						"<div class='col-sm-9'>"+
							"<input id='rc_diagnostico_"+posicion_tabla_diagnostico+"' type='text' class='form-control font-weight-bold border-bottom' aria-describedby='basic-addon1'>"+
						"</div>"+
						"<div class='col-sm-1'><h6>CIE-10:</h6></div>"+
						"<div class='col-sm-2'>"+
							"<input id='rc_cie_"+posicion_tabla_diagnostico+"' type='text' class='form-control font-weight-bold border-bottom' aria-describedby='basic-addon1'>"+
						"</div>"+
					"</div>"+
				"</td><td>"+ 
					html_boton_eliminar+
				"</td>"+
			"</tr>";
	
	posicion_tabla_diagnostico++;
	$("#tbl_listado_diagnostico tbody").append(html);
}

function llenarInputTablaReceta(){
	var html_boton_eliminar = (posicion_tabla_receta == 1 ? "&nbsp;" : "<i onclick='eliminarFilaTablaReceta(`"+posicion_tabla_receta+"`);' class='fa fa-minus-circle' title='Eliminar fila..."+posicion_tabla_receta+"'></i>");
	var html="<tr id='tr_receta_"+posicion_tabla_receta+"'>"+
				"<td>"+
					"<input id='input_"+posicion_tabla_receta+"_medicamento' type='text' class='form-control font-weight-bold border-bottom' aria-describedby='basic-addon1'>"+
				"</td><td>"+
					"<input id='input_"+posicion_tabla_receta+"_concent' type='text' class='form-control font-weight-bold border-bottom' aria-describedby='basic-addon1'>"+
				"</td><td>"+
					"<input id='input_"+posicion_tabla_receta+"_ff' type='text' class='form-control font-weight-bold border-bottom' aria-describedby='basic-addon1'>"+
				"</td><td>"+
					"<input id='input_"+posicion_tabla_receta+"_cantidad' type='text' class='form-control font-weight-bold border-bottom' aria-describedby='basic-addon1'>"+
				"</td><td>"+
					"<input id='input_"+posicion_tabla_receta+"_dosis' type='text' class='form-control font-weight-bold border-bottom' aria-describedby='basic-addon1'>"+
				"</td><td>"+
					"<input id='input_"+posicion_tabla_receta+"_via' type='text' class='form-control font-weight-bold border-bottom' aria-describedby='basic-addon1'>"+
				"</td><td>"+
					"<input id='input_"+posicion_tabla_receta+"_frecuencia' type='text' class='form-control font-weight-bold border-bottom' aria-describedby='basic-addon1'>"+
				"</td><td>"+
					"<input id='input_"+posicion_tabla_receta+"_duracion' type='text' class='form-control font-weight-bold border-bottom' aria-describedby='basic-addon1'>"+
				"</td><td>"+
					html_boton_eliminar +
				"</td>"+
			"</tr>";
	posicion_tabla_receta++;
	$("#tbl_listado_receta tbody").append(html);
}

function eliminarFilaTablaDiagnostico(numero_fila){
	$("#tr_diagnostico_"+numero_fila).remove();
}

function eliminarFilaTablaReceta(numero_fila){
	$("#tr_receta_"+numero_fila).remove();
}

function listarServicios(){
	//"dataSrc": ""
	var objRequest = { info: "" };
	posta.get(  
			'servicios/listar_servicios_activos', 
			objRequest,
			function(response) {
				listado_servicios = response;
				llenarSelectServicios();
			}
		);
}

function llenarSelectServicios(){
	var html_servicios = "";
	if(listado_servicios != null){
		for (var i = 0; i < listado_servicios.length; i++) {
			html_servicios += "<option value='"+listado_servicios[i].servicioId+"' >"+listado_servicios[i].nombre+"</option>";
		}
	}
	$("#consulta_interconsulta").empty().append(html_servicios);
}

function mostrarInfoFiliacionBasica(){
	if(obj_paci == null){
		mostrarMensajeTemporal("contenedor_nueva_consulta_mensaje", "amarillo", "Error en la obtención de datos del paciente.", 4);
		return;
	}
	obtenerUltimaConsultaPaciente(obj_paci.numero_documento == null ? "-1" : obj_paci.numero_documento);
	
	$("#consulta_peso").val(obj_paci.peso == null ? 0 : obj_paci.peso);
	$("#consulta_talla").val(obj_paci.talla == null ? 0 : obj_paci.talla);
	calculandoIMC();
	
	$("#nav-filiacion-tab").click();
	mostrarContenedor(false, "contenedor_listar_consulta");
	mostrarContenedor(true, "contenedor_info_nueva_consulta");
	
	llenarInfoPrimerPanelPaciente();
	llenarInfoSegundoPanelPaciente();
}

function llenarInfoSegundoPanelPaciente(){
	var activar_segundo_panel = (obj_paci == null ? false : (obj_paci.sexo == null ? false : (obj_paci.sexo == 1 ? true : false)));
	mostrarContenedor(activar_segundo_panel, "nav-personales-tab");
	mostrarContenedor(activar_segundo_panel, "contenedor_nav_personales");
}

function llenarInfoPrimerPanelPaciente(){
	
	$("#lbl_paciente_seleccionado").empty();
	if(obj_paci != null){
		$("#lbl_paciente_seleccionado").append("Nueva consulta: <b>" + (obj_paci.nombre == null ? "" : obj_paci.nombre) + " " + (obj_paci.apellido_paterno == null ? "" : obj_paci.apellido_paterno) 
			+ " " + (obj_paci.apellido_materno == null ? "" : obj_paci.apellido_materno)+"</b>");
	}
	if(obj_paci != null){
		$("#nw_nombres").val(obj_paci.nombre == null ? "-" : obj_paci.nombre);
		$("#nw_ape_paterno").val(obj_paci.apellido_paterno == null ? "-" : obj_paci.apellido_paterno);
		$("#nw_ape_materno").val(obj_paci.apellido_materno == null ? "-" : obj_paci.apellido_materno);
		$("#nw_nro_documento").val(obj_paci.numero_documento == null ? "-" : obj_paci.numero_documento);
		$("#nw_sexo").val(obj_paci.sexo == null ? "-" : (obj_paci.sexo == 1 ? "Femenino" : "Masculino"));
		
		$("#nw_fecha_nacimiento").val(obj_paci.fecha_nacimiento == null ? "-" : obj_paci.fecha_nacimiento);
		$("#nw_telefono").val(obj_paci.telefono == null ? "-" : obj_paci.telefono);
		$("#nw_celular").val(obj_paci.celular == null ? "-" : obj_paci.celular);
		$("#nw_email").val(obj_paci.correo == null ? "-" : obj_paci.correo);
		$("#nw_edad").val(obj_paci.edad == null ? "-" : obj_paci.edad);
		
		$("#nw_peso").val(obj_paci.peso == null ? "-" : obj_paci.peso);
		$("#nw_talla").val(obj_paci.talla == null ? "-" : obj_paci.talla);
		$("#nw_raza").val(obj_paci.raza == null ? "-" : obj_paci.raza);
		$("#nw_religion").val(obj_paci.religion == null ? "-" : obj_paci.religion);
		$("#nw_nacionalidad").val(obj_paci.nacionalidad == null ? "-" : obj_paci.nacionalidad);
		
		$("#nw_direccion").val(obj_paci.direccion_actual == null ? "-" : obj_paci.direccion_actual);
		$("#nw_lugar_nacimiento").val(obj_paci.lugar_nacimiento == null ? "-" : obj_paci.lugar_nacimiento);
		var grado_int = "-"
		if(obj_paci.grado_instruccion != null){
			if(obj_paci.grado_instruccion == 0) grado_int = "Analfabeto";
			if(obj_paci.grado_instruccion == 1) grado_int = "Pre Escolar";
			if(obj_paci.grado_instruccion == 2) grado_int = "Primaria Incompleta";
			if(obj_paci.grado_instruccion == 3) grado_int = "Primaria Completa";
			if(obj_paci.grado_instruccion == 4) grado_int = "Secundaria Incompleta";
			if(obj_paci.grado_instruccion == 5) grado_int = "Secundaria Completa";
			if(obj_paci.grado_instruccion == 6) grado_int = "Técnico Superior Incompleta";
			if(obj_paci.grado_instruccion == 7) grado_int = "Técnico Superior Completa";
			if(obj_paci.grado_instruccion == 8) grado_int = "Universidad Incompleta";
			if(obj_paci.grado_instruccion == 9) grado_int = "Universidad Completa";			
		}
		$("#nw_grado_instruccion").val(grado_int);
		$("#nw_ocupacion").val(obj_paci.ocupacion == null ? "-" : obj_paci.ocupacion);
		var est_civil = "-";
		if(obj_paci.estado_civil != null){
			if(obj_paci.estado_civil == 0) est_civil = "Soltero";
			if(obj_paci.estado_civil == 1) est_civil = "Conviviente";
			if(obj_paci.estado_civil == 2) est_civil = "Casado";
			if(obj_paci.estado_civil == 3) est_civil = "Viudo";
			if(obj_paci.estado_civil == 4) est_civil = "Divorciado";
		}
		$("#nw_estado_civil").val(est_civil);
		
		$("#nw_distrito").val(obj_paci.nombre_distrito);
		$("#nw_provincia").val(obj_paci.nombre_provincia);
		$("#nw_departamento").val(obj_paci.nombre_departamento);
		
		$("#nw_nombre_contacto").val(obj_paci.nombre_contacto == null ? "-" : obj_paci.nombre_contacto);
		$("#nw_telefono_contacto").val(obj_paci.telefono_contacto == null ? "-" : obj_paci.telefono_contacto);
		$("#nw_email_contacto").val(obj_paci.correo_contacto == null ? "-" : obj_paci.correo_contacto);
	}else{
		 $('.primer_panel_nueva_consulta').find('input:text').val('');
	}
}

function mostrarContenedor(activar_contenedor, id_contenedor){
	if(activar_contenedor){
		$("#"+id_contenedor).fadeIn('medium');
	}else{
		$("#"+id_contenedor).fadeOut('medium');  
	}
}

function obtenerPaciente(){
	obj_paci = null;
	var dni_paciente_input = $("#busqueda_input_dni").val().trim();
	var objRequest = { dni_paciente: dni_paciente_input };
	posta.get(
			'consultas/obtener_paciente_dni', 
			objRequest,
			function(response) {
				if(response+""==""){
					$("#input_dni_paciente").val("");
					$("#input_nombre_paciente").val("");
					$("#input_apellidos_paciente").val("");
					
					listarConsultas("##vacio##");
					$("#contenedor_registro_consulta").fadeOut('medium');
					if($("#busqueda_input_dni").val().trim() != "")
						mostrarMensajeTemporal("contenedor_mensaje", "amarillo", "El paciente no existe, por favor registre uno en la Gestión de Pacientes. Mostrando la totalidad de consultas registradas en el sistema:", 6);
				}else{
					obj_paci = response;
					$("#input_dni_paciente").val(response.numero_documento);
					$("#input_nombre_paciente").val(response.nombre);
					$("#input_apellidos_paciente").val(response.apellido_paterno + " " + response.apellido_materno);
					
					listarConsultas(response.numero_documento+"");
					$("#contenedor_registro_consulta").fadeIn('medium');
					mostrarMensajeTemporal("contenedor_mensaje", "verde", "Paciente encontrado. Mostrando sus consultas registradas:", 6);
				}
			}
		);
}

/* Busqueda del paciente por dni, si existe, listar solo sus consultas sino mostrará todas las consultas existentes. */
function listarConsultas(dni_ingresado){
		
	if($.fn.DataTable.isDataTable('#tabla_consultas')) {
		$("#tabla_consultas").DataTable().destroy();
	}
    table_consultas = $("#tabla_consultas").DataTable({
        responsive : true,
        ajax:{
            url: ip + "consultas/listar_consultas_paciente",
            type: "GET",
            data: {
            		dni_paciente : dni_ingresado
                },
		    "dataSrc": ""
        },
        columns: [
        	{ data: 'id_consulta' },
        	{ data: 'created'},
        	{ data: 'paciente.numero_documento'},
        	{ data: 'paciente.nombre'},
        	{ data: 'paciente.apellido_paterno'},
        	{ data: 'paciente.celular'},
			{ 
				data: null,
				className: "dt-center",
				render: function(data) {
                    var id_consulta = data.id_consulta;
                    return '<a href="#" onclick="descargarPDF(`'+id_consulta+'`,`receta`)" title="Descargar PDF receta..."><i class="fa fa-download"></i></a>';
                }
			}
        ],
        language
    });
}

function descargarPDF(id_consulta_seleccionada, tipo_pdf){
	diagnosticos_c = null;
	medicamentos_c = null;
	if(tipo_pdf == 'receta'){
		
		
		/*Diagnosticos y recetas de la consulta seleccionada...*/
		
		obtenerDiagnosticos(id_consulta_seleccionada)
			.then(obtenerMedicamentos.bind(null, id_consulta_seleccionada))
			.then(generarPDF);
		
	}
}

function generarPDF(){
	
	
	if(diagnosticos_c.length > 0 && medicamentos_c.length > 0){
		
		var consulta_c = medicamentos_c[0].consulta;
		var fecha_prescripcion_c = medicamentos_c[0].consulta.created;
		var fecha_expiracion_c = medicamentos_c[0].fecha_expiracion;
		
		var paciente_c = medicamentos_c[0].consulta.paciente;
		var apes_y_nom_paciente_c = paciente_c.apellido_paterno + " " + paciente_c.apellido_materno + ", " +paciente_c.nombre;
		var peso_c = consulta_c.peso;
		var edad_c = paciente_c.edad;
		var talla_c = consulta_c.talla;
				
		/*INFO GENERAL*/
		var html_cont = "<div class='col-sm-12'><p>"+
			"Fecha de inscripci\u{F3}n: <strong>"+fecha_prescripcion_c+"</strong><br>"+
			"Fecha de expiraci\u{F3}n: <strong>"+ fecha_expiracion_c + "</strong><br>"+
			"Apellidos y nombres: "+apes_y_nom_paciente_c + "<br>"+
			"Edad: " + edad_c + " a\u{F1}os.<br>"+
			"Peso: " + peso_c + " kgs.<br>"+
			"Talla: " + talla_c + " metros.</p>";
		
		/*DIAGNOSTICOS*/
		html_cont += "<p>";
		for (var i = 0; i < diagnosticos_c.length; i++) {
			html_cont += "<b>Diagnostico "+(i+1)+"</b>: <ins>"+diagnosticos_c[i].diagnostico+"</ins>. CIE10: <ins>"+diagnosticos_c[i].cie10+"</ins><br>"; 
		}
		html_cont +="</p></div>";
		
		/*MEDICAMENTOS*/
		html_cont += '<div class="col-sm-12">'+
						'<div class="row">'
		for (var i = 0; i < medicamentos_c.length; i++) {
			html_cont +='<div class="col"><b>Medicamento (DCI) o insumo</b>: '+ medicamentos_c[i].medicamento +'</div>'+
						'<div class="col"><b>Concentraci\u{F3}n</b>: '+ medicamentos_c[i].concentracion +'</div>'+
						'<div class="col"><b>F.F.</b>: '+ medicamentos_c[i].ff +'</div>'+
						'<div class="col"><b>Cantidad</b>: '+ medicamentos_c[i].cantidad +'</div>'+
						'<div class="col"><b>Dosis</b>: '+ medicamentos_c[i].dosis +'</div>'+
						'<div class="col"><b>V\u{ED}a</b>: '+ medicamentos_c[i].via +'</div>'+
						'<div class="col"><b>Frecuencia</b>: '+ medicamentos_c[i].frecuencia +'</div>'+
						'<div class="col"><b>Duraci\u{F3}n</b>: '+ medicamentos_c[i].duracion +'</div>'+
						'<div class="col-12" style="text-align: center;">&nbsp;&nbsp;-&nbsp;</div>'+
						'<div class="col-12" style="text-align: center;"><hr></div>';
		}
		
		
		
		
		html_cont +="</div></div>";
		$('#contenedor_pdf').empty().append(html_cont);
		
		var doc = new jsPDF();
		var elementHTML = $('#contenedor_pdf').html(); 
		var specialElementHandlers = {
		    '#elementH': function (element, renderer) {
		        return true;
		    }
		};
		doc.setFont("courier");
		doc.setFontType("normal");
		doc.fromHTML(elementHTML, 15, 15, {
		    'width': 250,
		    'elementHandlers': specialElementHandlers
		});
		
		// Save the PDF
		doc.save('Consulta_'+consulta_c.id_consulta+'.pdf');
		
		
	}
}



function obtenerDiagnosticos(id_consulta_seleccionada){
	return $.ajax({
			url: ip + "consultas/listar_diagnosticos",
			data : {
				id_consulta: id_consulta_seleccionada
			},
			type: 'get',
			cache: false
		}).done(function(response){
			if(response)
				diagnosticos_c = response;
		})
}

function obtenerMedicamentos(id_consulta_seleccionada){
	return $.ajax({
			url: ip + "consultas/listar_medicamentos",
			data : {
				id_consulta: id_consulta_seleccionada
			},
			type: 'get',
			cache: false
		}).done(function(response){
			if(response)
				medicamentos_c = response;
		})
}

function obtenerUltimaConsultaPaciente(dni_ingresado){
	return $.ajax({
			url: ip + "consultas/obtener_ultima_consulta",
			data : {
				dni_paciente: dni_ingresado
			},
			type: 'get',
			cache: false
		}).done(function(response){
			$('#nw_menarquia').val('');
			$('#nw_numero_embarazos').val('');
			$('#nw_cantidad_hijos').val('');
			$('#nw_regimen_cetamenial').val('');
			$('#nw_metodo_anti').val('');
			$('#nw_fecha_ult_reg').val('');
			$('#nw_orientacion_sexual').val('');
			$('#nw_nro_parejas_sexuales').val('');
			if(response){
				if(response.fur!= null){
					var form_fur = response.fur;
					var day_fur = form_fur.split("/")[0];
					var month_fur = form_fur.split("/")[1];
					var year_fur = form_fur.split("/")[2];
					var fecha_nuevo_formato = year_fur+"-"+month_fur+"-"+day_fur;
					$('#nw_fecha_ult_reg').val(fecha_nuevo_formato);
				}
				
				$('#nw_menarquia').val(response.menarquia.trim().length > 0 && response.menarquia.trim() != "-" ? response.menarquia : "");
				$('#nw_numero_embarazos').val(response.gestaciones.trim().length > 0 && response.gestaciones.trim() != "-" ? response.gestaciones : "");
				$('#nw_cantidad_hijos').val(response.partos.trim().length > 0 && response.partos.trim() != "-" ? response.partos : "");
				$('#nw_regimen_cetamenial').val(response.regimen_catamenial.trim().length > 0 && response.regimen_catamenial.trim() != "-" ? response.regimen_catamenial : "");
				$('#nw_metodo_anti').val(response.metconceptivo.trim().length > 0 && response.metconceptivo.trim() != "-" ? response.metconceptivo : "");
				$('#nw_orientacion_sexual').val(response.orientacion_sexual.trim().length > 0 && response.orientacion_sexual.trim() != "-" ? response.orientacion_sexual : "");
				$('#nw_nro_parejas_sexuales').val(response.nro_parejas_sexuales.trim().length > 0 && response.nro_parejas_sexuales.trim() != "-" ? response.nro_parejas_sexuales : "");
			}
		})
}
















