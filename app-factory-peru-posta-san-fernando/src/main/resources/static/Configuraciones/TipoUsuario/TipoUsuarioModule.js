var table_tipoUsuario;
var actualizar_id_tipoUsuario = -1;
function init() {
    listarServicios();
}

function listarServicios() {
    table_tipoUsuario = $("#table_tipoUsuario").DataTable({
        responsive: true,
        "ajax": {
            "url": ip + "tipoUsuario/listar_tipoUsuarios",
            "dataSrc": ""
        },
        "columns": [
            { data: "nombre" },
            { data: "descripcion" },
            { data: "created" },
            {
                data: null,
                className: "dt-center",
                render: function(data) {
                    var id_tipoUser = data.tipoUsuarioId;
                    var estado_tipoUser = data.estado;
                    var nom_tipoUser = data.nombre;
                    if (estado_tipoUser == 1) {
                        return '<a href="#" onclick="cambiar_estado_tipoUser(`'+id_tipoUser+'`,`'+estado_tipoUser+'`,`'+nom_tipoUser+'`)"><i class="fa fa-toggle-on green font-md"></i></a>';
                    } else {
                        return '<a href="#" onclick="cambiar_estado_tipoUser(`'+id_tipoUser+'`,`'+estado_tipoUser+'`,`'+nom_tipoUser+'`)"><i class="fa fa-toggle-off red font-md"></i></a>';
                    }
                }
            },
            { data: null,
              className: "dt-center",
              render: function(data){
              	var id_tipoUser = data.tipoUsuarioId;
              	var nom_tipoUser = data.nombre;
              	var desc_tipoUser = data.descripcion;
              	return '<button type="button" class="btn btn-sm" onclick="ver_actualizar_tipoUser(`'+id_tipoUser+'`,`'+nom_tipoUser+'`,`'+desc_tipoUser+'`)"><i class="fa fa-edit style="font-size:80px" "></i></button>';
              }
            }
        ],
        language
    });
}

function cambiar_estado_tipoUser(id_tipoUser, estado_tipoUser, nom_tipoUser) {
    var objRequest = {
        id_tipoUser: id_tipoUser,
        estado: estado_tipoUser
    };
    posta.post(
        'tipoUsuario/cambiar_estado_tipoUser',
        objRequest,
        function(response) {
            if (response == "Listo") {
                table_tipoUsuario.ajax.reload();
                var estado = estado_tipoUser == 1 ? "deshabilitó " : "habilitó ";
                mostrarMensajeTemporal("contenedor_mensaje_tipoUsuario", "verde", "Se "+estado+ nom_tipoUser+" con éxito!", 4);
            } else {
               mostrarMensajeTemporal("contenedor_mensaje_tipoUsuario", "rojo", "Ocurrió un error, contacte con el administrador.", 6);
            }
        }
    );
}

function agregarTipoUsuario() {
    if ($("#nombre_tipoUser").val() == "") {
        mostrarMensajeTemporal("contenedor_mensaje_add_tipoUser", "amarillo", "Ingrese nombre del tipo de usuario.", 3);
        return;
    }
    posta.post(
        'tipoUsuario/guardar_tipoUsuarios',
        data= {
	        nombre: $("#nombre_tipoUser").val(),
	        descripcion: $("#descripcion_tipoUser").val()
	    },
        function(response) {
        	if(response == "Listo"){               		 	
	            table_tipoUsuario.ajax.reload();
	            $("#nombre_tipoUser").val("");
	            $("#descripcion_tipoUser").val("");
	            mostrarMensajeTemporal("contenedor_mensaje_add_tipoUser", "verde", "Se registró con éxito!", 4);
	        }else{
	     		mostrarMensajeTemporal("contenedor_mensaje_add_tipoUser", "rojo", "Ocurrió un error, contacte con el administrador.", 6);	
	        }
        }
    );
}

function ver_actualizar_tipoUser(id, nombre, descripcion){
	$("#modal_actualizar_tipoUsuario").modal('show');
	$("#update_nombreTipoUser").val(nombre);
	$("#update_descripcionTipoUser").val(descripcion);
	actualizar_id_tipoUsuario = id;
}

function actualizarTipoUsuario(){
	posta.post(
        'tipoUsuario/actualizar_tipoUsuarios',
        data = {
        	id_tipoUser: actualizar_id_tipoUsuario,
	        new_nombre: $("#update_nombreTipoUser").val(),
	        new_descripcion: $("#update_descripcionTipoUser").val()
        },
        function(response) {
        	if(response == "Listo"){        
        		table_tipoUsuario.ajax.reload();
        		$("#modal_actualizar_tipoUsuario").modal('hide');
	            mostrarMensajeTemporal("contenedor_mensaje_tipoUsuario", "verde", "Se actualizó con éxito!", 4);
	        }else{
	     		mostrarMensajeTemporal("contenedor_mensaje_tipoUsuario", "rojo", "Ocurrió un error, contacte con el administrador.", 6);	
	        }
        }
    );
}