var table_paciente;
 
function init(){
	listarPacientes();
	$('#form_agregar_paciente').submit(function(e){
		e.preventDefault();
		agregarPaciente(this);
	}); 
	$('#form_editar_paciente').submit(function(e){
		e.preventDefault();
		editarPaciente(this);
	}); 
}

function agregarPaciente(form_paciente){
	if ($("#nw_pac_nombre").val() == "") {
        mostrarMensajeTemporal("contenedor_mensaje_add_paciente", "amarillo", "Por favor, ingrese nombres del paciente.", 4);
        return;
    }
    if ($("#nw_pac_ape_paterno").val() == "") {
        mostrarMensajeTemporal("contenedor_mensaje_add_paciente", "amarillo", "Por favor, ingrese apellido paterno del paciente.", 4);
        return;
    }
    if ($("#nw_pac_dni").val() == "") {
        mostrarMensajeTemporal("contenedor_mensaje_add_paciente", "amarillo", "Por favor, ingrese N° documento del paciente.", 4);
        return;
    }
    if ($("#nw_pac_sexo").val() == -1) {
        mostrarMensajeTemporal("contenedor_mensaje_add_paciente", "amarillo", "Por favor, ingrese sexo del paciente.", 4);
        return;
    }
    if ($("#nw_pac_fechaNacimiento").val() == "") {
        mostrarMensajeTemporal("contenedor_mensaje_add_paciente", "amarillo", "Por favor, ingrese fecha de nacimiento del paciente.", 4);
        return;
    }
    if ($("#nw_pac_distrito").val() == -1) {
        mostrarMensajeTemporal("contenedor_mensaje_add_paciente", "amarillo", "Por favor, ingrese distrito del paciente.", 4);
        return;
    }
	var object = {};
	var formData = new FormData(form_paciente);
	formData.forEach((value, key) => object[key] = value);
	
	posta.post('pacientes/guardar_paciente', object, function(response){
		if(response.startsWith(':')){
			mostrarMensajeTemporal('contenedor_mensaje_add_paciente', 'amarillo', response.slice(1), 6);
		} else if (response == 'Listo'){
			form_paciente.reset();
			mostrarMensajeTemporal('contenedor_mensaje_add_paciente', 'verde', 'Paciente registrado con éxito.', 5);			
			table_paciente.ajax.reload();
		} else{
			mostrarMensajeTemporal('contenedor_mensaje_add_paciente', 'rojo', 'Ocurrió un error, contacte con el administrador.', 6);
		}
	});
}

function editarPaciente(form_paciente){
	if ($("#up_pac_nombre").val() == "") {
        mostrarMensajeTemporal("contenedor_mensaje_edit_paciente", "amarillo", "Por favor, ingrese nombres del paciente.", 4);
        return;
    }
    if ($("#up_pac_ape_paterno").val() == "") {
        mostrarMensajeTemporal("contenedor_mensaje_edit_paciente", "amarillo", "Por favor, ingrese apellido paterno del paciente.", 4);
        return;
    }
    if ($("#up_pac_dni").val() == "") {
        mostrarMensajeTemporal("contenedor_mensaje_edit_paciente", "amarillo", "Por favor, ingrese N° documento del paciente.", 4);
        return;
    }
    if ($("#up_pac_sexo").val() == -1) {
        mostrarMensajeTemporal("contenedor_mensaje_edit_paciente", "amarillo", "Por favor, ingrese sexo del paciente.", 4);
        return;
    }
    if ($("#up_pac_fechaNacimiento").val() == "") {
        mostrarMensajeTemporal("contenedor_mensaje_edit_paciente", "amarillo", "Por favor, ingrese fecha de nacimiento del paciente.", 4);
        return;
    }
    if ($("#up_pac_distrito").val() == -1) {
        mostrarMensajeTemporal("contenedor_mensaje_edit_paciente", "amarillo", "Por favor, ingrese distrito del paciente.", 4);
        return;
    }
	var object = {};
	var formData = new FormData(form_paciente);
	formData.forEach((value, key) => object[key] = value);
	
	posta.post('pacientes/editar_paciente', object, function(response){
		if(response.startsWith(':')){
			mostrarMensajeTemporal('contenedor_mensaje_edit_paciente', 'amarillo', response.slice(1), 6);
		} else if (response == 'Listo'){
			table_paciente.ajax.reload();
			form_paciente.reset();
			$('#modal_editar_paciente').modal('hide');
			mostrarMensajeTemporal("contenedor_mensaje_pacientes", "verde", "Se actualizó con éxito!", 4);
		} else{
			mostrarMensajeTemporal('contenedor_mensaje_edit_paciente', 'rojo', 'Ocurrió un error, contacte con el administrador.', 6);
		}
	})
}

function mostrarModalPaciente(id_paciente, nombre, apellido_paterno, apellido_materno, numero_documento, sexo, fechaNacimiento, telefono, celular, correo, lugar_nacimiento, 
						peso, talla, raza, religion, nacionalidad, direccion_actual, grado_instruccion, ocupacion, estado_civil, departamento, provincia, distrito, nombre_contacto,
						telefono_contacto, correo_contacto){
	$('#modal_editar_paciente').modal('show');
	var form = document.getElementById('form_editar_paciente');
	form.id_paciente.value = id_paciente;
	form.nombre.value = nombre;
	form.apellidoPaterno.value = apellido_paterno;
	form.apellidoMaterno.value = apellido_materno;
	form.numeroDocumento.value = numero_documento;
	form.sexo.value = sexo;
	form.fechaNacimiento.value = fechaNacimiento;
	form.telefono.value = telefono;
	form.celular.value = celular;
	form.email.value = correo;
	form.lugarNacimiento.value = lugar_nacimiento;
	form.peso.value = peso;
	form.talla.value = talla;
	form.raza.value = raza;
	form.religion.value = religion;
	form.nacionalidad.value = nacionalidad;
	form.direccionDomiciliaria.value = direccion_actual;
	form.gradoInstruccion.value = grado_instruccion;
	form.ocupacion.value = ocupacion;
	form.estadoCivil.value = estado_civil;
	form.nombreContacto.value = nombre_contacto;
	form.telefonoContacto.value = telefono_contacto;
	form.emailContacto.value = correo_contacto;
	
	setSelectRegiones(form, departamento, provincia, distrito);
}


function listarPacientes(){
	table_paciente = $("#table_paciente").DataTable({
		responsive : true,
		"ajax":{
			"url": ip + "pacientes/listar_pacientes",
			"dataSrc": ""
		},
		"columns":[
			{data: "numero_documento"},
			{data: "nombre"},
			{ data: null,
			   render: function ( data, type, row ) {
			     return row.apellido_paterno + ' ' + row.apellido_materno;
		     }
			},			
			{data: "telefono"},
			{data: "celular"},
			{data: "region.nombre"},
			{data: "direccion_actual"},
			/*{
				data: null,
                className: "dt-center",
                render: function(data) {
                    var id_paciente = data.id_paciente;
                    var estado_paciente = data.estado;
                    if (estado_paciente == 1) {
                        return '<button type="button" class="btn btn-sm" onclick="cambiar_estado_paciente(`'+id_paciente+'`,`'+estado_paciente+'`)"><i class="fa fa-toggle-on green font-md"></i></button>';
                    } else {
                        return '<button type="button" class="btn btn-sm" onclick="cambiar_estado_paciente(`'+id_paciente+'`,`'+estado_paciente+'`)"><i class="fa fa-toggle-off red font-md"></i></button>';
                    }
                }
			},*/
			{data: null, 
				render: function(data , type, row){
				return `<button class="btn btn-sm" type="button"
					onclick="mostrarModalPaciente(${row.id_paciente}, '${row.nombre}', '${row.apellido_paterno}', '${row.apellido_materno}', '${row.numero_documento}', 
							${row.sexo}, '${row.fecha_nacimiento}', '${row.telefono}', '${row.celular}', '${row.correo}', '${row.lugar_nacimiento}', '${row.peso}', '${row.talla}', '${row.raza}', 
							'${row.religion}', '${row.nacionalidad}', '${row.direccion_actual}', ${row.grado_instruccion}, '${row.ocupacion}', ${row.estado_civil},
							${row.provincia.departamento}, ${row.provincia.regionNumero}, ${row.region.regionId}, '${row.nombre_contacto}', 
							'${row.telefono_contacto}', '${row.correo_contacto}')"><i class="fa fa-edit style=" font-size:80px"></i>
					</button>`;
			}}
		],
		language
	});
}


function cambiar_estado_paciente(id_paciente, estado_paciente) {
    posta.post(
        'pacientes/cambiar_estado_paciente',
        { id_paciente , estado_paciente } ,
        function(response) {
            if (response == "Listo") {
                table_paciente.ajax.reload();
                var estado = estado_paciente == 1 ? "deshabilitó " : "habilitó ";
                mostrarMensajeTemporal("contenedor_mensaje_pacientes", "verde", "Se realizaron los cambios con éxito!", 4);
            } else {
               mostrarMensajeTemporal("contenedor_mensaje_pacientes", "rojo", "Ocurrió un error, contacte con el administrador.", 6);
            }
        }
    );
}
