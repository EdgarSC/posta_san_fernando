var table_citas;
var obj_paci = null;
var dni_paciente_seleccionado = '';

function iniciarCitas(){
	listar_servicios("#nw_cita_servicio");
    listar_servicios("#up_cita_servicio");
    
	listarCitas('');
	
	$("#btn_activar_buscador_nueva_consulta").on("click",function(){
		if($("#contenedor_busqueda_dni").is(":visible")){
			mostrarContenedor(false, "contenedor_busqueda_dni");
		}else{
			mostrarContenedor(true, "contenedor_busqueda_dni");
		}
	})
	
	$('#form_agregar_cita').submit(function(e){
		e.preventDefault();
		agregarCita(this);
	});
	
	$('#form_editar_cita').submit(function(e){
		e.preventDefault();
		editarCita(this);
	});
}

function listar_servicios(_servicio) {
    posta.get(
        'servicios/listar_servicios_activos',
        objRequest = {},
        function(response) {
            if (response.length >0) {
               for(var i=0; i<response.length;i++ ){
               		$(_servicio).append('<option value="'+response[i].servicioId+'">'+response[i].nombre+'</option>');
               }
            } else {
               mostrarMensajeTemporal("contenedor_mensaje_usuarios", "rojo", "Ocurrió un error, contacte con el administrador.", 6);
            }
        }
    );
}

function agregarCita(form){
	var formData = new FormData(form);
	var object = {};
	formData.forEach((value, key) => object[key] = value);
	
	if(!objectValid(object)){
		mostrarMensajeTemporal('contenedor_mensaje_add_cita', 'amarillo', 'Tiene que completar todos los campos', 3);
		return;
	}
	
	posta.post('citas/guardar_cita', object, function(response){
		if(response == 'Listo'){
			mostrarMensajeTemporal("contenedor_mensaje", "verde", "Se agregó con éxito!", 4);
			$('#modal_agregar_cita').modal('hide');
			listarCitas(dni_paciente_seleccionado);
			form.reset();
		}else{
			mostrarMensajeTemporal("contenedor_mensaje_add_cita", "rojo", 'Ocurrió un error, contacte con el administrador.', 6);
		}	
	});
}

function editarCita(form){
	var formData = new FormData(form);
	var object = {};
	formData.forEach((value, key) => object[key] = value);
	
	if(!objectValid(object)){
		mostrarMensajeTemporal('contenedor_mensaje_edit_paciente', 'amarillo', 'Tiene que completar todos los campos', 3);
		return;
	}
	
	posta.post('citas/editar_cita', object, function(response){
		if(response == 'Listo'){
			mostrarMensajeTemporal("contenedor_mensaje", "verde", "Se modificó con éxito!", 4);
			$('#modal_editar_cita').modal('hide');
			listarCitas(dni_paciente_seleccionado);
			form.reset();
		}else{
			mostrarMensajeTemporal("contenedor_mensaje_edit_paciente", "rojo", 'Ocurrió un error, contacte con el administrador.', 6);
		}	
	});
}

function eliminarCita(id_cita){
	posta.post('citas/eliminar_cita', { id_cita } , function(response){
		if(response == 'Listo'){
			mostrarMensajeTemporal("contenedor_mensaje", "verde", "Se eliminó con éxito!", 4);
			$('#modal_agregar_cita').modal('hide');
			listarCitas(dni_paciente_seleccionado);
		}else{
			mostrarMensajeTemporal("contenedor_mensaje", "rojo", 'Ocurrió un error, contacte con el administrador.', 6);
		}	
	});
}

function listarCitas(dni_paciente){
	if($.fn.DataTable.isDataTable('#table_citas')) {
		$("#table_citas").DataTable().destroy();
	}
    table_citas = $("#table_citas").DataTable({
        responsive : true,
        ajax:{
            url: ip + "citas/listar_citas_paciente",
            type: "GET",
            data: { dni_paciente },
		    "dataSrc": ""
        },
        columns: [
        	{ data: 'paciente.numero_documento'},
        	{ data: null,
			   render: function ( data, type, row ) {
			     return row.paciente.nombre + ' ' + row.paciente.apellido_paterno + ' ' + row.paciente.apellido_materno;
		    }},
        	{ data: null,
			   render: function ( data, type, row ) {
			     return row.fecha_cita + ' ' + row.hora_cita;
		    }},
        	{ data: null,
			   render: function ( data, type, row ) {
			     return row.celular_paciente;
		    }},
		    { data: 'servicio.nombre'},
        	{ data: null,
			   render: function ( data, type, row ) {
			     return row.interconsulta ? 'Si' : 'No'; 
		    }},
        	{ data: null,
			   render: function ( data, type, row) {
                    return `<button type="button" onclick="eliminarCita(${row.id_cita})" class="btn btn-sm"><i class="fa fa-trash"></i></button>`;
		    }},
        	{ data: null,
			   render: function ( data, type, row) {
                    return `<button type="button" 
                    		onclick="modalEditarCita(${row.servicio.servicioId}, ${row.id_cita}, ${row.paciente.id_paciente}, '${row.paciente.numero_documento}', '${row.paciente.nombre}', '${row.paciente.apellido_paterno} ${row.paciente.apellido_materno}',
                    		'${row.fecha_cita}', '${row.hora_cita}', '${row.interconsulta}', ${row.celular_paciente})" 
                    		class="btn btn-sm"><i class="fa fa-edit"></i></button>`;
		    }}
        ],
        language
    });
}

function modalEditarCita(id_servicio, id_cita, id_paciente, dni, nombre, apellidos, fecha, hora, interconsulta, celular){
	var form_editar_cita = document.getElementById('form_editar_cita');
	$('#dni_editar_cita').val(dni);
	$('#nombre_editar_cita').val(nombre + " " + apellidos);
	form_editar_cita.servicio_cita.value = id_servicio;
	form_editar_cita.id_cita.value = id_cita;
	form_editar_cita.id_paciente.value = id_paciente;
	form_editar_cita.fecha_cita.value = fecha;
	form_editar_cita.hora_cita.value = hora;
	form_editar_cita.celular_cita.value = celular;
	form_editar_cita.interconsulta.value = interconsulta;
	
	$('#modal_editar_cita').modal('show');
}

function obtenerPaciente(){
	obj_paci = null;
	$('#contenedor_button_agregar_cita').addClass('hidden');
	if($("#busqueda_input_dni").val().trim() == ""){
		mostrarMensajeTemporal("contenedor_mensaje", "amarillo", "Ning\u{FA}n valor de DNI ingresado.", 4);
		return;
    }else{
		$("#contenedor_mensaje").fadeOut('medium');
	}
	dni_paciente_seleccionado = $("#busqueda_input_dni").val().trim();
	posta.get(
			'consultas/obtener_paciente_dni', 
			{ dni_paciente : dni_paciente_seleccionado },
			function(response) {
				if(response+""==""){
					$("#input_dni_paciente").val("");
					$("#input_nombre_paciente").val("");
					$("#input_celular_paciente").val("");
					dni_paciente_seleccionado = '';
					$("#contenedor_cita_consulta").fadeOut('medium');
					mostrarMensajeTemporal("contenedor_mensaje", "amarillo", "El paciente no existe, por favor registre uno en la Gestión de Pacientes. Mostrando la totalidad de consultas registradas en el sistema:", 6);
				}else{
					$('#contenedor_button_agregar_cita').removeClass('hidden');
					document.getElementById('form_agregar_cita').id_paciente.value = response.id_paciente;
					$('#dni_agregar_cita').val(response.numero_documento);
					$('#nombre_agregar_cita').val(response.nombre + " " + response.apellido_paterno + " " + response.apellido_materno);
					$('#celular_agregar_cita').val(response.celular);
					obj_paci = response;
					$("#input_dni_paciente").val(response.numero_documento);
					$("#input_nombre_paciente").val(response.nombre + " " + response.apellido_paterno + " " + response.apellido_materno);
					$("#input_celular_paciente").val(response.celular);
					
					$("#contenedor_registro_consulta").fadeIn('medium');
					mostrarMensajeTemporal("contenedor_mensaje", "verde", "Paciente encontrado. Mostrando sus consultas registradas:", 6);
				}
				listarCitas(dni_paciente_seleccionado);
			}
		);
}

function objectValid(object){
	for(var prop in object){
		if(object[prop] == "") return false;
	}
	return true;
}