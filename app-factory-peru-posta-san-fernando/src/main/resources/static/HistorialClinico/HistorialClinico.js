var HistorialClinicoModule = (function () {
	
	console.log("INGRESANDO HistorialClinicoModule");
	var obj = this;
	console.log("obj>");console.log(obj);
	globalFunctions.extendKORequiredString(ko);
    globalFunctions.configTools(ko);
    
    var config = function (prSearchHistorialClinico) {
    	console.log("prSearchHistorialClinico");
    	console.log(prSearchHistorialClinico);
        ko.applyBindings(obj, document.getElementById(prSearchHistorialClinico));
        $("").showDiv({ section: prSearchHistorialClinico, divId: 'HistorialClinicoSearchMain' });     
        listHistorialClinico();
    };
    
    var aHistorialClinicoEntityCollection =  ko.observableArray([]);
    
    var listHistorialClinico = function (){        
    	$("").coserver({
             action: "/historial/historialClinicoList",
             splashscreen: true,
         }).callService();
    	 console.log("DATA HISTORIAL");
    	 console.log();
    }

	return {
		 config: config,	
		 listHistorialClinico: listHistorialClinico,
		 aHistorialClinicoEntityCollection: aHistorialClinicoEntityCollection
	}
})